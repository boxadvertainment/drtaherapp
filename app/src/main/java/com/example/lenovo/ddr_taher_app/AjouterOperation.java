package com.example.lenovo.ddr_taher_app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Calendar;
import java.util.Date;


public class AjouterOperation extends ActionBarActivity {
    final private static int DIALOG_DATE_NAISSANCE = 1;
    final private static int DIALOG_DATE_OPERATION = 2;
    SharedPreferences prefs;
    Toolbar toolbar;
     RadioGroup radioSexGroup;
    RadioButton radioSexButton , radiooperateurtaher,radiooperateueautre,radioequipetaher,radioequipeautre , radiosexmale , radiosexfemme;
    EditText nom , prenom , mail , tel , diagnostic , adresse , autreoperation , visageautre,seinsautre,corpautre , autreoperateur,autreequipe
            ,antecedents , compte_rendu_operatoire;
    TextView dateoperation ,erordateoperation,age,erornom , erorprenom , erormail , erortel , eroradresse, erordiag , erordateNaissance,erorantecedents,erorcompterendu ;
    String dateNaissance, anciennom , ancienprenom , ancienmail , ancientel , ancienage , anciensexe="homme" , anciendiag , anciencompterendu, ancienantecedent , anciendateoperation,ancienoperateur,ancienequipe,ancienadresse , dateOperation ;
    Button  editdateoperation;
    CheckBox visage, Lifting_cervico_facial ,Peeling,Blepharoplastie,Lifting_temporal,Lifting_complet ,Genioplastie,Otoplastie,autrevisage,
    seins,Augmentation_mammaire , Lipofilling_des_seins,Reduction_mammaire,Changement_de_protheses,Lifting_des_seins,Gynecomastie,autreseins,
    corp ,Liposuccion,Abdominoplastie,Lifting_des_cuisses,Augmentation_fesses , autrecorp,
    autre ;
    boolean dr;

    Boolean ancienvisage, ancienLifting_cervico_facial ,ancienPeeling,ancienBlepharoplastie,ancienLifting_temporal,ancienLifting_complet ,ancienGenioplastie,ancienOtoplastie,
            ancienseins,ancienAugmentation_mammaire , ancienLipofilling_des_seins,ancienReduction_mammaire,ancienChangement_de_protheses,ancienLifting_des_seins,ancienGynecomastie,
            anciencorp ,ancienLiposuccion,ancienAbdominoplastie,ancienLifting_des_cuisses,ancienAugmentation_fesses
            ;
    String ancienautrevisage,ancienautreseins,ancienautrecorp,ancienautre;

    EditText num_adminission;
    TextView errorNum_admission;
    String ancienNum_admission;
    String ancienId;


    TextView errordetailsoperation ,errordetailsoperationvisage,errordetailsoperationcorp,errordetailsoperationseins,errordetailsoperationautre ;
boolean neew;
    int nombre_brouillant;
   EditText editdatenaissance;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter_operation);
        Bundle extras = getIntent().getExtras();
        prefs = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        String mmail=prefs.getString("mail","jj");
        threadListeoperationBrouillant task2=new threadListeoperationBrouillant(mmail);
        Thread thread2 = new Thread (task2);
        thread2.start();
        try {
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            JSONArray hama=new JSONArray(task2.getResultat());
            nombre_brouillant=hama.length();


        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (extras != null) {
            dr = extras.getBoolean("dr");
            anciennom=extras.getString("nom");
            ancienprenom=extras.getString("prenom");
            ancienmail=extras.getString("mail");
            ancientel=extras.getString("tel");
            ancienage=extras.getString("age");
            anciensexe=extras.getString("sexe");
            anciendiag=extras.getString("diag");
            ancienantecedent=extras.getString("antecedent");
            anciencompterendu=extras.getString("compterendu");
            anciendateoperation=extras.getString("dateoperation");
            ancienoperateur=extras.getString("operateur");
            ancienequipe=extras.getString("equipe");
            ancienadresse=extras.getString("adresse");

            ancienvisage =extras.getBoolean("visage");
            ancienLifting_cervico_facial=extras.getBoolean("lifting_cervico_facial");
            ancienPeeling=extras.getBoolean("peeling");
            ancienBlepharoplastie=extras.getBoolean("blepharoplastie");
            ancienLifting_temporal=extras.getBoolean("lifting_temporal");
            ancienLifting_complet=extras.getBoolean( "lifting_complet");
            ancienGenioplastie=extras.getBoolean( "genioplastie");
            ancienOtoplastie=extras.getBoolean("otoplastie");
            ancienautrevisage=extras.getString("autrevisage");
            ancienseins=extras.getBoolean("seins");
            ancienAugmentation_mammaire=extras.getBoolean("augmentation_mammaire");
            ancienLipofilling_des_seins=extras.getBoolean("lipofilling_des_seins");
            ancienReduction_mammaire=extras.getBoolean("reduction_mammaire(");
            ancienChangement_de_protheses=extras.getBoolean("changement_de_protheses");
            ancienLifting_des_seins=extras.getBoolean("lifting_des_seins");
            ancienGynecomastie=extras.getBoolean("gynecomastie");
            ancienautreseins=extras.getString("autreseins");
            anciencorp=extras.getBoolean("corp");
            ancienLiposuccion=extras.getBoolean("liposuccion");
            ancienAbdominoplastie=extras.getBoolean("abdominoplastie");
            ancienLifting_des_cuisses=extras.getBoolean("lifting_des_cuisses");
            ancienAugmentation_fesses=extras.getBoolean("Augmentation_fesse");
            ancienautrecorp=extras.getString("autrecorp");
            ancienNum_admission=extras.getString("admission");
            ancienId=extras.getString("id");


            ancienautre=extras.getString("autre");
            neew=extras.getBoolean("new");

        }
        errordetailsoperationautre= (TextView) findViewById(R.id.erordetailoperationautre);
        errordetailsoperationseins= (TextView) findViewById(R.id.erordetailoperationseins);
        errordetailsoperationcorp= (TextView) findViewById(R.id.erordetailoperationcorp);
        errordetailsoperationvisage= (TextView) findViewById(R.id.erordetailoperationvisage);
        errordetailsoperation= (TextView) findViewById(R.id.erordetailoperation);
        errorNum_admission= (TextView) findViewById(R.id.erorNum_adminission);
        num_adminission= (EditText) findViewById(R.id.Num_adminission);

        editdateoperation= (Button) findViewById(R.id.editDateOperation);
        erordateoperation= (TextView) findViewById(R.id.erorDateOperation);
        dateoperation= (TextView) findViewById(R.id.textdateoperation);
        if(!anciendateoperation.equals("")) {
            dateoperation.setText("Date operation  :(" + anciendateoperation + ")");
            dateOperation=anciendateoperation;
        }else{
            dateoperation.setText("Date operation :");
            dateOperation="";
        }
        age= (TextView) findViewById(R.id.Ageajout);
        if(!ancienage.equals("")) {
            age.setText("Age  :(" + ancienage + ")");
            dateNaissance=ancienage;
        }else{
            age.setText("Age :");
            dateNaissance="";
        }
        num_adminission.setText(ancienNum_admission);
        nom= (EditText) findViewById(R.id.nom);
        nom.setText(anciennom);
        prenom= (EditText) findViewById(R.id.Prenom);
        prenom.setText(ancienprenom);
        mail= (EditText) findViewById(R.id.mail);
        mail.setText(ancienmail);
        tel= (EditText) findViewById(R.id.tel);
        tel.setText(ancientel);
        diagnostic =(EditText)findViewById(R.id.diagnostique);
        diagnostic.setText(anciendiag);
        adresse=(EditText)findViewById(R.id.adresse);
        adresse.setText(ancienadresse);
        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        toolbar=(Toolbar)findViewById(R.id.app_bar);
        erornom=(TextView)findViewById(R.id.erornom);
        erorprenom=(TextView)findViewById(R.id.erorprenom);
        erormail=(TextView)findViewById(R.id.erormail);
        erortel=(TextView)findViewById(R.id.erortel);
        eroradresse=(TextView)findViewById(R.id.eroradresse);
        erordiag=(TextView)findViewById(R.id.erordiag);
        erordateNaissance=(TextView)findViewById(R.id.erordateNaissance);
        editdatenaissance= (EditText) findViewById(R.id.editdatenaissance);
        editdatenaissance.setText(ancienage);
        visage=(CheckBox)findViewById(R.id.visage);
                Lifting_cervico_facial=(CheckBox)findViewById(R.id.visagelifting);
                Peeling=(CheckBox)findViewById(R.id.visagepeeling);
                Blepharoplastie=(CheckBox)findViewById(R.id.visageBlepharoplastie);
                Lifting_temporal=(CheckBox)findViewById(R.id.visageLiftingtemporal);
                Lifting_complet =(CheckBox)findViewById(R.id.visageLiftingcomplet);
                Genioplastie=(CheckBox)findViewById(R.id.visageGenioplastie);
                Otoplastie=(CheckBox)findViewById(R.id.visageOtoplastie);
                autrevisage=(CheckBox)findViewById(R.id.visageautre);
        seins=(CheckBox)findViewById(R.id.seins);
                Augmentation_mammaire =(CheckBox)findViewById(R.id.seinsAugmentationmammaire);
                Lipofilling_des_seins=(CheckBox)findViewById(R.id.seinsLipofillingdesseins);
                Reduction_mammaire=(CheckBox)findViewById(R.id.seinsReductionmammaire);
                Changement_de_protheses=(CheckBox)findViewById(R.id.seinsChangementdeprotheses);
                Lifting_des_seins=(CheckBox)findViewById(R.id.seinsLiftingdesseins);
                Gynecomastie=(CheckBox)findViewById(R.id.seinsGynecomastie);
                autreseins=(CheckBox)findViewById(R.id.autreseins);
        corp=(CheckBox)findViewById(R.id.corp);
                Liposuccion=(CheckBox)findViewById(R.id.CorpLiposuccion);
                Abdominoplastie=(CheckBox)findViewById(R.id.CorpAbdominoplastie);
                Lifting_des_cuisses=(CheckBox)findViewById(R.id.CorpLiftingdescuisses);
                Augmentation_fesses =(CheckBox)findViewById(R.id.CorpAugmentationfesses);
                autrecorp=(CheckBox)findViewById(R.id.corpautre);
        autre=(CheckBox)findViewById(R.id.autre0);

        autreoperation=(EditText)findViewById(R.id.autreOperation);
        visageautre=(EditText)findViewById(R.id.visageautreOperation);
        corpautre=(EditText)findViewById(R.id.autreCorp);
        seinsautre=(EditText)findViewById(R.id.seinsautreOperation);

                radiooperateurtaher= (RadioButton) findViewById(R.id.radiooperateurtaher);
                radiooperateueautre= (RadioButton) findViewById(R.id.radiooperateurautre);
                autreoperateur= (EditText) findViewById(R.id.autreOperateur);

        radioequipetaher= (RadioButton) findViewById(R.id.radioequipetaher);
        radioequipeautre= (RadioButton) findViewById(R.id.radioequipeautre);
        autreequipe= (EditText) findViewById(R.id.autreequipe);
        antecedents= (EditText) findViewById(R.id.antecedents);
        antecedents.setText(ancienantecedent);
        compte_rendu_operatoire= (EditText) findViewById(R.id.compterenduoperatoire);
        compte_rendu_operatoire.setText(anciencompterendu);
        erorantecedents= (TextView) findViewById(R.id.erorantecedents);
        erorcompterendu= (TextView) findViewById(R.id.erorcompterenduoperatoire);
        radiosexfemme= (RadioButton) findViewById(R.id.radioFemale);
        radiosexmale= (RadioButton) findViewById(R.id.radioMale);

        visageautre.setVisibility(View.GONE);
        seinsautre.setVisibility(View.GONE);
        corpautre.setVisibility(View.GONE);
        autreoperation.setVisibility(View.GONE);
        autreequipe.setVisibility(View.GONE);
        autreoperateur.setVisibility(View.GONE);
        if(!ancienvisage) {
            Lifting_cervico_facial.setVisibility(View.GONE);
            Peeling.setVisibility(View.GONE);
            Blepharoplastie.setVisibility(View.GONE);
            Lifting_temporal.setVisibility(View.GONE);
            Lifting_complet.setVisibility(View.GONE);
            Genioplastie.setVisibility(View.GONE);
            Otoplastie.setVisibility(View.GONE);
            autrevisage.setVisibility(View.GONE);
            visageautre.setVisibility(View.GONE);
        }else{
            Lifting_cervico_facial.setVisibility(View.VISIBLE
            );
            Peeling.setVisibility(View.VISIBLE);
            Blepharoplastie.setVisibility(View.VISIBLE);
            Lifting_temporal.setVisibility(View.VISIBLE);
            Lifting_complet.setVisibility(View.VISIBLE);
            Genioplastie.setVisibility(View.VISIBLE);
            Otoplastie.setVisibility(View.VISIBLE);
            autrevisage.setVisibility(View.VISIBLE);
            visageautre.setVisibility(View.VISIBLE);

        }
        if(!ancienseins) {
            Augmentation_mammaire.setVisibility(View.GONE);
            Lipofilling_des_seins.setVisibility(View.GONE);
            Reduction_mammaire.setVisibility(View.GONE);
            Changement_de_protheses.setVisibility(View.GONE);
            Lifting_des_seins.setVisibility(View.GONE);
            Gynecomastie.setVisibility(View.GONE);
            autreseins.setVisibility(View.GONE);
            seinsautre.setVisibility(View.GONE);

        }else{
            Augmentation_mammaire.setVisibility(View.VISIBLE);
            Lipofilling_des_seins.setVisibility(View.VISIBLE);
            Reduction_mammaire.setVisibility(View.VISIBLE);
            Changement_de_protheses.setVisibility(View.VISIBLE);
            Lifting_des_seins.setVisibility(View.VISIBLE);
            Gynecomastie.setVisibility(View.VISIBLE);
            autreseins.setVisibility(View.VISIBLE);
            seinsautre.setVisibility(View.VISIBLE);

        }

        if(!anciencorp) {
            Liposuccion.setVisibility(View.GONE);
            Abdominoplastie.setVisibility(View.GONE);
            Lifting_des_cuisses.setVisibility(View.GONE);
            Augmentation_fesses.setVisibility(View.GONE);
            autrecorp.setVisibility(View.GONE);
            corpautre.setVisibility(View.GONE);
        }else{
            Liposuccion.setVisibility(View.VISIBLE);
            Abdominoplastie.setVisibility(View.VISIBLE);
            Lifting_des_cuisses.setVisibility(View.VISIBLE);
            Augmentation_fesses.setVisibility(View.VISIBLE);
            autrecorp.setVisibility(View.VISIBLE);
            corpautre.setVisibility(View.VISIBLE);

        }

        if(!ancienvisage){
            visage.setChecked(false);
            Lifting_cervico_facial.setChecked(false);
            Peeling.setChecked(false);
            Blepharoplastie.setChecked(false);
            Lifting_temporal.setChecked(false);
            Lifting_complet.setChecked(false);
            Genioplastie.setChecked(false);
            Otoplastie.setChecked(false);
            autrevisage.setChecked(false);
            visageautre.setText("");
        }else{
            visage.setChecked(true);
            if(ancienLifting_cervico_facial){
                Lifting_cervico_facial.setChecked(true);
            }else{
                Lifting_cervico_facial.setChecked(false);

            }
            if(ancienPeeling){
                Peeling.setChecked(true);
            }else{
                Peeling.setChecked(false);

            }
            if(ancienBlepharoplastie){
                Blepharoplastie.setChecked(true);
            }else {
                Blepharoplastie.setChecked(false);

            }
            if(ancienLifting_temporal){
                Lifting_temporal.setChecked(true);
            }else{
                Lifting_temporal.setChecked(false);
            }
            if(ancienLifting_complet){
                Lifting_complet.setChecked(true);
            }else{
                Lifting_complet.setChecked(false);
            }
            if(ancienGenioplastie){
                Genioplastie.setChecked(true);
            }else{
                Genioplastie.setChecked(false);

            }
            if(ancienOtoplastie){
                Otoplastie.setChecked(true);
            }else{
                Otoplastie.setChecked(false);
            }
            if(!ancienautrevisage.equals("")){
                autrevisage.setChecked(true);
                visageautre.setText(ancienautrevisage);
                visageautre.setVisibility(View.VISIBLE);

            }else{

                autrevisage.setChecked(false);
                visageautre.setVisibility(View.GONE);

            }

        }
        if(!ancienseins){

            seins.setChecked(false);
            Augmentation_mammaire .setChecked(false);
            Lipofilling_des_seins.setChecked(false);
            Reduction_mammaire.setChecked(false);
            Changement_de_protheses.setChecked(false);
            Lifting_des_seins.setChecked(false);
            Gynecomastie.setChecked(false);
            autreseins.setChecked(false);
            seinsautre.setText("");


        }else{
            seins.setChecked(true);
            if(ancienAugmentation_mammaire){
                Augmentation_mammaire.setChecked(true);
            }else{
                Augmentation_mammaire.setChecked(false);
            }
            if(ancienLipofilling_des_seins){
                Lipofilling_des_seins.setChecked(true);
            }else{
                Lipofilling_des_seins.setChecked(false);
            }
            if(ancienReduction_mammaire){
                Reduction_mammaire.setChecked(true);
            }else{
                Reduction_mammaire.setChecked(false);
            }
            if(ancienChangement_de_protheses){
                Changement_de_protheses.setChecked(true);
            }else{
                Changement_de_protheses.setChecked(false);
            }
            if(ancienLifting_des_seins){
                Lifting_des_seins.setChecked(true);
            }else{
                Lifting_des_seins.setChecked(false);
            }
            if(ancienGynecomastie){
                Gynecomastie.setChecked(true);
            }else{
                Gynecomastie.setChecked(false);
            }
            if(!ancienautreseins.equals("")){
                autreseins.setChecked(true);
                seinsautre.setText(ancienautreseins);
                seinsautre.setVisibility(View.VISIBLE);

            }else{

                autreseins.setChecked(false);
                seinsautre.setVisibility(View.GONE);

            }
        }
        if(!anciencorp){

            corp.setChecked(false);
            Liposuccion.setChecked(false);
            Abdominoplastie.setChecked(false);
            Lifting_des_cuisses.setChecked(false);
            Augmentation_fesses.setChecked(false);
            autrecorp.setChecked(false);
            corpautre.setText("");

        }else{
            corp.setChecked(true);
            if(ancienLiposuccion){
                Liposuccion.setChecked(true);
            }else{
                Liposuccion.setChecked(false);
            }
            if(ancienAbdominoplastie){
                Abdominoplastie.setChecked(true);
            }else{
                Abdominoplastie.setChecked(false);
            }
            if(ancienLifting_des_cuisses){
                Lifting_des_cuisses.setChecked(true);
            }else{
                Lifting_des_cuisses.setChecked(false);
            }
            if(ancienAugmentation_fesses){
                Augmentation_fesses.setChecked(true);
            }else{
                Augmentation_fesses.setChecked(false);
            }
            if(!ancienautrecorp.equals("")){
                autrecorp.setChecked(true);
                corpautre.setText(ancienautrecorp);
                corpautre.setVisibility(View.VISIBLE);

            }else{

                autrecorp.setChecked(false);
                corpautre.setVisibility(View.GONE);

            }


        }
        if(!ancienautre.equals("")){
            autre.setChecked(true);
            autreoperation.setText(ancienautre);
            autreoperation.setVisibility(View.VISIBLE);
        }else{
            autre.setChecked(false);
            autreoperation.setVisibility(View.GONE);

        }




        erordateNaissance.setText("");
        erornom.setText("");
        erorprenom.setText("");
        erormail.setText("");
        eroradresse.setText("");
        erordiag.setText("");
        erortel.setText("");
        dateNaissance="";
        erorcompterendu.setText("");
        erorantecedents.setText("");
        erordateoperation.setText("");
        errorNum_admission.setText("");
        errordetailsoperation.setText("");
        errordetailsoperationcorp.setText("");
        errordetailsoperationseins.setText("");
        errordetailsoperationvisage.setText("");
        errordetailsoperationautre.setText("");

        errordetailsoperation.setVisibility(View.GONE);
        errordetailsoperationcorp.setVisibility(View.GONE);
        errordetailsoperationseins.setVisibility(View.GONE);
        errordetailsoperationvisage.setVisibility(View.GONE);
        errordetailsoperationautre.setVisibility(View.GONE);



        editdateoperation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DIALOG_DATE_OPERATION);
            }
        });


        setSupportActionBar(toolbar);

        visage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (visage.isChecked()) {
                    Lifting_cervico_facial.setVisibility(View.VISIBLE);
                    Peeling.setVisibility(View.VISIBLE);
                    Blepharoplastie.setVisibility(View.VISIBLE);
                    Lifting_temporal.setVisibility(View.VISIBLE);
                    Lifting_complet.setVisibility(View.VISIBLE);
                    Genioplastie.setVisibility(View.VISIBLE);
                    Otoplastie.setVisibility(View.VISIBLE);
                    autrevisage.setVisibility(View.VISIBLE);
                    if(autrevisage.isChecked()) {
                        visageautre.setVisibility(View.VISIBLE);
                    }else{
                        visageautre.setVisibility(View.GONE);
                    }



                } else {
                    Lifting_cervico_facial.setVisibility(View.GONE);
                    Peeling.setVisibility(View.GONE);
                    Blepharoplastie.setVisibility(View.GONE);
                    Lifting_temporal.setVisibility(View.GONE);
                    Lifting_complet.setVisibility(View.GONE);
                    Genioplastie.setVisibility(View.GONE);
                    Otoplastie.setVisibility(View.GONE);
                    autrevisage.setVisibility(View.GONE);
                    visageautre.setVisibility(View.GONE);

                }

            }
        });
        if(anciensexe.equals("homme")){
            radiosexmale.setChecked(true);
            radiosexfemme.setChecked(false);

        }else{
            radiosexmale.setChecked(false);
            radiosexfemme.setChecked(true);
        }

        seins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (seins.isChecked()) {
                    Augmentation_mammaire.setVisibility(View.VISIBLE);
                    Lipofilling_des_seins.setVisibility(View.VISIBLE);
                    Reduction_mammaire.setVisibility(View.VISIBLE);
                    Changement_de_protheses.setVisibility(View.VISIBLE);
                    Lifting_des_seins.setVisibility(View.VISIBLE);
                    Gynecomastie.setVisibility(View.VISIBLE);
                    autreseins.setVisibility(View.VISIBLE);
                    if(autreseins.isChecked()) {
                        seinsautre.setVisibility(View.VISIBLE);
                    }else{
                        seinsautre.setVisibility(View.GONE);
                    }


                } else {
                    Augmentation_mammaire.setVisibility(View.GONE);
                    Lipofilling_des_seins.setVisibility(View.GONE);
                    Reduction_mammaire.setVisibility(View.GONE);
                    Changement_de_protheses.setVisibility(View.GONE);
                    Lifting_des_seins.setVisibility(View.GONE);
                    Gynecomastie.setVisibility(View.GONE);
                    autreseins.setVisibility(View.GONE);
                    seinsautre.setVisibility(View.GONE);

                }

            }
        });

        corp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (corp.isChecked()) {

                    Liposuccion.setVisibility(View.VISIBLE);
                    Abdominoplastie.setVisibility(View.VISIBLE);
                    Lifting_des_cuisses.setVisibility(View.VISIBLE);
                    Augmentation_fesses .setVisibility(View.VISIBLE);
                    autrecorp.setVisibility(View.VISIBLE);
                    if(autrecorp.isChecked()) {
                        corpautre.setVisibility(View.VISIBLE);
                    }else{
                        corpautre.setVisibility(View.GONE);
                    }


                } else {

                    Liposuccion.setVisibility(View.GONE);
                    Abdominoplastie.setVisibility(View.GONE);
                    Lifting_des_cuisses.setVisibility(View.GONE);
                    Augmentation_fesses .setVisibility(View.GONE);
                    autrecorp.setVisibility(View.GONE);
                    corpautre.setVisibility(View.GONE);


                }

            }
        });

       autre.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if (autre.isChecked()) {
                   autreoperation.setVisibility(View.VISIBLE);

               } else {
                   autreoperation.setText("");
                   autreoperation.setVisibility(View.GONE);
               }

           }
       });

        autrevisage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(autrevisage.isChecked()) {
                    visageautre.setVisibility(View.VISIBLE);

                }else{
                    visageautre.setText("");
                    visageautre.setVisibility(View.GONE);
                }

            }
        });

        autrecorp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(autrecorp.isChecked()) {
                    corpautre.setVisibility(View.VISIBLE);

                }else{
                    corpautre.setText("");
                    corpautre.setVisibility(View.GONE);
                }

            }
        });

        autreseins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(autreseins.isChecked()) {
                    seinsautre.setVisibility(View.VISIBLE);

                }else{
                    seinsautre.setText("");
                    seinsautre.setVisibility(View.GONE);
                }

            }
        });
        radiooperateueautre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(radiooperateueautre.isChecked()) {
                    autreoperateur.setVisibility(View.VISIBLE);

                }else{
                    autreoperateur.setText("");
                    autreoperateur.setVisibility(View.GONE);
                }




            }
        });
        radioequipeautre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    autreequipe.setVisibility(View.VISIBLE);


            }
        });
        radiooperateueautre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    autreoperateur.setVisibility(View.VISIBLE);


            }
        });
        radioequipetaher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                autreequipe.setVisibility(View.GONE);


            }
        });
        radiooperateurtaher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                autreoperateur.setVisibility(View.GONE);


            }
        });
        if(ancienoperateur.equals("drtaher")){
            radiooperateurtaher.setChecked(true);
            radiooperateueautre.setChecked(false);
            autreoperateur.setVisibility(View.GONE);
        }else{
            radiooperateurtaher.setChecked(false);
            radiooperateueautre.setChecked(true);
            autreoperateur.setVisibility(View.VISIBLE);
            autreoperateur.setText(ancienoperateur);

        }
        if(ancienequipe.equals("drtaher")){
            radioequipetaher.setChecked(true);
            radioequipeautre.setChecked(false);
            autreequipe.setVisibility(View.GONE);
        }else{
            radioequipetaher.setChecked(false);
            radioequipeautre.setChecked(true);
            autreequipe.setVisibility(View.VISIBLE);
            autreequipe.setText(ancienequipe);

        }




    }


    protected Dialog onCreateDialog(int id) {



        AlertDialog dialogDetails = null;



        switch (id) {

            case DIALOG_DATE_NAISSANCE:

                LayoutInflater inflater = LayoutInflater.from(this);

                View dialogview = inflater.inflate(R.layout.dialogdatenaissance, null);



                AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);

                dialogbuilder.setTitle("Date de naissance");



                dialogbuilder.setView(dialogview);

                dialogDetails = dialogbuilder.create();



                break;
            case DIALOG_DATE_OPERATION:

                LayoutInflater inflater1 = LayoutInflater.from(this);

                View dialogview1 = inflater1.inflate(R.layout.dialogdatenaissance, null);



                AlertDialog.Builder dialogbuilder1 = new AlertDialog.Builder(this);

                dialogbuilder1.setTitle("Date de l'operation");



                dialogbuilder1.setView(dialogview1);

                dialogDetails = dialogbuilder1.create();



                break;

        }



        return dialogDetails;

    }



    @Override

    protected void onPrepareDialog(int id, Dialog dialog) {



        switch (id) {

            case DIALOG_DATE_NAISSANCE:

                final AlertDialog alertDialog = (AlertDialog) dialog;

                Button ok = (Button) alertDialog

                        .findViewById(R.id.ok);

                Button nn = (Button) alertDialog

                        .findViewById(R.id.non);

                final DatePicker date = (DatePicker) alertDialog

                        .findViewById(R.id.datenaissance);
                Date a =new Date(1950,0,1);
                Date b =new Date(2016,0,1);
                Calendar c = Calendar.getInstance();
                c.set(2016, 0, 1);

                date.setMaxDate(c.getTimeInMillis());
                c.set(1950, 0, 1);

                date.setMinDate(c.getTimeInMillis());







                ok.setOnClickListener(new View.OnClickListener() {



                    @Override

                    public void onClick(View v) {

                        alertDialog.dismiss();

                        Toast.makeText(

                                AjouterOperation.this,

                                "date de naissance est  : "+(date.getDayOfMonth())+"/" + (date.getMonth()+1)+"/"+(date.getYear())

                                        ,

                                Toast.LENGTH_LONG).show();
                        dateNaissance= ""+(date.getDayOfMonth())+"/" + (date.getMonth()+1)+"/"+(date.getYear());
                        age.setText("Age :("+dateNaissance+")");
                        ancienage=dateNaissance;


                    }

                });



                nn.setOnClickListener(new View.OnClickListener() {



                    @Override

                    public void onClick(View v) {

                        alertDialog.dismiss();

                    }

                });

                break;
            case DIALOG_DATE_OPERATION:

                final AlertDialog alertDialog1 = (AlertDialog) dialog;

                Button ok1 = (Button) alertDialog1

                        .findViewById(R.id.ok);

                Button nn1 = (Button) alertDialog1

                        .findViewById(R.id.non);

                final DatePicker date1 = (DatePicker) alertDialog1

                        .findViewById(R.id.datenaissance);
                Date a1 =new Date(2015,0,1);
                Date b1 =new Date(2050,0,1);
                Calendar c1 = Calendar.getInstance();
                c1.set(2050, 0, 1);

                date1.setMaxDate(c1.getTimeInMillis());
                c1.set(2015, 0, 1);

                date1.setMinDate(c1.getTimeInMillis());







                ok1.setOnClickListener(new View.OnClickListener() {



                    @Override

                    public void onClick(View v) {

                        alertDialog1.dismiss();



                        Toast.makeText(

                                AjouterOperation.this,

                                "date de l'operation est  : "+(date1.getDayOfMonth())+"/" + (date1.getMonth()+1)+"/"+(date1.getYear())

                                ,

                                Toast.LENGTH_LONG).show();
                        dateOperation= ""+(date1.getDayOfMonth())+"/" + (date1.getMonth()+1)+"/"+(date1.getYear());
                        dateoperation.setText("Date operation ("+dateOperation+")");
                        anciendateoperation=dateOperation;


                    }

                });



                nn1.setOnClickListener(new View.OnClickListener() {



                    @Override

                    public void onClick(View v) {

                        alertDialog1.dismiss();

                    }

                });

                break;


        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(dr) {
            getMenuInflater().inflate(R.menu.menu_ajouter_operation, menu);
            MenuItem a = menu.findItem(R.id.listebrouillant);
            a.setTitle("liste Brouillant ("+nombre_brouillant+")");

            return true;
        }else{
            getMenuInflater().inflate(R.menu.menu_ajouter_operation_secretaire, menu);
            MenuItem a = menu.findItem(R.id.listebrouillant);
            a.setTitle("liste Brouillant (" + nombre_brouillant + ")");
            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.deconnexion || id == R.id.deconnexion1 ) {
            Intent intent = new Intent(AjouterOperation.this,
                    Authentification.class);

            startActivity(intent);

        }
        if (id == R.id.quitter) {
            Intent intent = new Intent(AjouterOperation.this,
                    Liste_operation.class);

            startActivity(intent);

        }
        if(id == R.id.confirmerbrouillant || id == R.id.confirmerbrouillant1){

            boolean ok=true;
            int lengtvisages=0;
            String []visages= new String[10];;
            String autrevisages = "";
            int lengtseins=0;
            String[]seinss= new String[10];;
            String autreseinss="";
            int lengtcorps=0;
            String[]corps= new String[10];;
            String autrecorps="";
            int lengtinterventions=0;
            String[]interventions = new String[10];
            String autreoperations="";
            if(visage.isChecked()){

                interventions[lengtinterventions]="visage";
                if(Lifting_cervico_facial.isChecked()){

                    visages[lengtvisages]="lifting cervico facial";
                    lengtvisages++;
                }
                if(Peeling.isChecked()){

                    visages[lengtvisages]="peeling";
                    lengtvisages++;
                }
                if(Blepharoplastie.isChecked()){

                    visages[lengtvisages]="blepharoplastie";
                    lengtvisages++;
                }
                if(Lifting_temporal.isChecked()){

                    visages[lengtvisages]="lifting temporal";
                    lengtvisages++;
                }
                if(Lifting_complet.isChecked()){

                    visages[lengtvisages]="lifting complet";
                    lengtvisages++;
                }
                if(Genioplastie.isChecked()){

                    visages[lengtvisages]="genioplastie";
                    lengtvisages++;
                }
                if(Otoplastie.isChecked()){

                    visages[lengtvisages]="otoplastie";
                    lengtvisages++;
                }
                if(autrevisage.isChecked()){

                    visages[lengtvisages]="autre";
                    autrevisages=visageautre.getText().toString();
                    lengtvisages++;
                }

                lengtinterventions++;

            }
            if(corp.isChecked()){

                interventions[lengtinterventions]="corp";
                if(Liposuccion.isChecked()){

                    corps[lengtcorps]="liposuccion";
                    lengtcorps++;
                }
                if(Abdominoplastie.isChecked()){

                    corps[lengtcorps]="abdominoplastie";
                    lengtcorps++;
                }
                if(Lifting_des_cuisses.isChecked()){

                    corps[lengtcorps]="lifting des cuisses";
                    lengtcorps++;
                }
                if(Augmentation_fesses.isChecked()){

                    corps[lengtcorps]="augmentation fesses";
                    lengtcorps++;
                }
                if(autrecorp.isChecked()){

                    corps[lengtcorps]="autre";
                    autrecorps=corpautre.getText().toString();
                    lengtcorps++;
                }

                lengtinterventions++;
            }
            if(seins.isChecked()){

                interventions[lengtinterventions]="seins";
                if(Augmentation_mammaire.isChecked()){

                    seinss[lengtseins]="augmentation mammaire";
                    lengtseins++;
                }
                if(Lipofilling_des_seins.isChecked()){

                    seinss[lengtseins]="lipofilling des seins";
                    lengtseins++;
                }
                if(Reduction_mammaire.isChecked()){

                    seinss[lengtseins]="reduction mammaire";
                    lengtseins++;
                }
                if(Changement_de_protheses.isChecked()){

                    seinss[lengtseins]="changement de protheses";
                    lengtseins++;
                }
                if(Lifting_des_seins.isChecked()){

                    seinss[lengtseins]="lifting des seins";
                    lengtseins++;
                }
                if(Gynecomastie.isChecked()){

                    seinss[lengtseins]="gynecomastie";
                    lengtseins++;
                }
                if(autreseins.isChecked()){

                    seinss[lengtseins]="autre";
                    autreseinss=seinsautre.getText().toString();
                    lengtseins++;
                }
                lengtinterventions++;

            }
            if(autre.isChecked()){
                lengtinterventions++;
                interventions[lengtinterventions]="autre";
                autreoperations=autreoperation.getText().toString();

            }


            String token=prefs.getString("token","h");
            String mailauth = prefs.getString("mail", "d");

            String ajousex="";
            if(radiosexfemme.isChecked()){
                ajousex="Femme";
            }else{
                ajousex="Homme";
            }
            String operateur;
            String autroperateur="";
            if(radiooperateurtaher.isChecked()){
                operateur="dr taher";
                autroperateur="";

            }else{
                operateur="autre";
                autroperateur=autreoperateur.getText().toString();
            }
            String equipes;
            String autroequipes="";
            if(radioequipetaher.isChecked()){
                equipes="dr taher";
                autroequipes="";

            }else{
                equipes="autre";
                autroequipes=autreequipe.getText().toString();
            }

            if(neew) {
                threadAjoutOperationBrouillant task = new threadAjoutOperationBrouillant(num_adminission.getText().toString(), token, mailauth, nom.getText().toString(), prenom.getText().toString(), mail.getText().toString(), tel.getText().toString(), editdatenaissance.getText().toString(), ajousex, diagnostic.getText().toString(), antecedents.getText().toString(), compte_rendu_operatoire.getText().toString(), operateur, autroperateur, equipes, autroequipes, adresse.getText().toString(), visages, autrevisages, seinss, autreseinss, corps, autrecorps, interventions, autreoperations, anciendateoperation);
                Thread thread = new Thread (task);
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(task.getAjouter()) {
                    Toast.makeText(AjouterOperation.this,
                            "l'operation a ete ajouter dans le brouillant avec succes ", Toast.LENGTH_SHORT).show();
                }else{
                    ok=false;

                    AlertDialog.Builder adb = new AlertDialog.Builder(AjouterOperation.this);

                    adb.setTitle("erreur");

                    adb.setMessage(task.getError());

                    adb.setPositiveButton("Ok", null);

                    adb.show();
                    num_adminission.setFocusable(true);

                }
                if(ok) {
                    Toast.makeText(AjouterOperation.this,
                            "l'operation est enregistrer dans le brouillant", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(AjouterOperation.this,
                            Liste_operation_brouillant.class);

                    startActivity(intent);
                }

            }else{
                threadmodification task = new threadmodification(ancienId,num_adminission.getText().toString(), token, mailauth, nom.getText().toString(), prenom.getText().toString(), mail.getText().toString(), tel.getText().toString(), editdatenaissance.getText().toString(), ajousex, diagnostic.getText().toString(), antecedents.getText().toString(), compte_rendu_operatoire.getText().toString(), operateur, autroperateur, equipes, autroequipes, adresse.getText().toString(), visages, autrevisages, seinss, autreseinss, corps, autrecorps, interventions, autreoperations, anciendateoperation);
                Thread thread = new Thread (task);
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(task.getAjouter()) {
                    Toast.makeText(AjouterOperation.this,
                            "l'operation a ete modifier dans le brouillant avec succes ", Toast.LENGTH_SHORT).show();
                }else{
                    ok=false;

                    AlertDialog.Builder adb = new AlertDialog.Builder(AjouterOperation.this);

                    adb.setTitle("erreur");

                    adb.setMessage(task.getError());

                    adb.setPositiveButton("Ok", null);

                    adb.show();
                    num_adminission.setFocusable(true);

                }
                if(ok) {
                    Toast.makeText(AjouterOperation.this,
                            "la modification est enregistrer dans le brouillant", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(AjouterOperation.this,
                            Liste_operation_brouillant.class);

                    startActivity(intent);
                }

            }




        }
        if(id == R.id.listebrouillant || id == R.id.listebrouillant1){


            Toast.makeText(AjouterOperation.this,
                    "liste de brouillant", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(AjouterOperation.this,
                    Liste_operation_brouillant.class);

            startActivity(intent);

        }
        if (id == R.id.confirmer || id == R.id.confirmer1 ) {


            if(dateNaissance.equals("")){
                erordateNaissance.setText("date de naissance n'est pas edite");
            }else{
                erordateNaissance.setText("");
            }

            int selectedId = radioSexGroup.getCheckedRadioButtonId();
            radioSexButton = (RadioButton) findViewById(selectedId);

            Toast.makeText(AjouterOperation.this,
                    radioSexButton.getText(), Toast.LENGTH_SHORT).show();

            if(nom.getText().toString().equals(""))
            {
                erornom.setText("le nom est vide ");
            }else{
                erornom.setText("");
            }

            if(prenom.getText().toString().equals(""))
            {
                erorprenom.setText("le prenom est vide ");
            }else{
                erorprenom.setText("");
            }

            if(mail.getText().toString().equals(""))
            {
                erormail.setText("le mail est vide ");
            }else{
                erormail.setText("");
            }

            if(tel.getText().toString().equals(""))
            {
                erortel.setText("le telephone est vide ");
            }else{
                erortel.setText("");
            }
            if(diagnostic.getText().toString().equals(""))
            {
                erordiag.setText("le diagnostic est vide ");
            }else{
                erordiag.setText("");
            }
            if(adresse.getText().toString().equals(""))
            {
                eroradresse.setText("l'adresse est vide ");
            }else{
                eroradresse.setText("");
            }
            if(antecedents.getText().toString().equals("")){
                erorantecedents.setText("l'antecedents est vide");

            }else{

                erorantecedents.setText("");
            }
            if(compte_rendu_operatoire.getText().toString().equals("")){
                erorcompterendu.setText("l'antecedents est vide");

            }else{

                erorcompterendu.setText("");
            }
            if(dateOperation.equals("")){
                erordateoperation.setText("la date de l'operation n'est pas saisie");
            }else{
                erordateoperation.setText("");
            }
            if(num_adminission.getText().toString().equals("")){
                errorNum_admission.setText("le numero d'admission n'est pas saisie");
            }else{
                errorNum_admission.setText("");
            }
            if(editdatenaissance.getText().toString().equals("")){
                erordateNaissance.setText("vous na'avez pas saisie l'age");
            }else{
                erordateNaissance.setText("");
            }
            if(!visage.isChecked()&&!corp.isChecked()&&!seins.isChecked()&&!autre.isChecked()){
                errordetailsoperationvisage.setVisibility(View.GONE);
                errordetailsoperationvisage.setText("");
                errordetailsoperationseins.setVisibility(View.GONE);
                errordetailsoperationseins.setText("");
                errordetailsoperationcorp.setVisibility(View.GONE);
                errordetailsoperationcorp.setText("");
                errordetailsoperationautre.setVisibility(View.GONE);
                errordetailsoperationautre.setText("");
                errordetailsoperation.setVisibility(View.VISIBLE);
                errordetailsoperation.setText("le detail operation n'est pas specifier");
            }else{
                errordetailsoperation.setVisibility(View.GONE);
                errordetailsoperation.setText("");
                errordetailsoperationvisage.setVisibility(View.GONE);
                errordetailsoperationvisage.setText("");
                errordetailsoperationseins.setVisibility(View.GONE);
                errordetailsoperationseins.setText("");
                errordetailsoperationcorp.setVisibility(View.GONE);
                errordetailsoperationcorp.setText("");
                errordetailsoperationautre.setVisibility(View.GONE);
                errordetailsoperationautre.setText("");
                if(visage.isChecked()){
                    if(!Lifting_cervico_facial.isChecked()&&!Peeling.isChecked()&&!Blepharoplastie.isChecked()&&!Lifting_temporal.isChecked()&&!Lifting_complet.isChecked()&&!Genioplastie.isChecked()&&!Otoplastie.isChecked()&&!autrevisage.isChecked()){
                        errordetailsoperationvisage.setText("le detail operation sur le visage n'est pas specifier");
                        errordetailsoperationvisage.setVisibility(View.VISIBLE);
                    }else{
                        errordetailsoperationvisage.setVisibility(View.GONE);
                        errordetailsoperationvisage.setText("");
                        if(autrevisage.isChecked()&&visageautre.getText().toString().equals("")){
                            errordetailsoperationvisage.setText("l'autre operation sur le visage n'est pas specifier");
                            errordetailsoperationvisage.setVisibility(View.VISIBLE);
                        }else{
                            errordetailsoperationvisage.setText("");
                            errordetailsoperationvisage.setVisibility(View.GONE);
                        }
                    }
                }else{
                    errordetailsoperationvisage.setText("");
                    errordetailsoperationvisage.setVisibility(View.GONE);
                }
                if(seins.isChecked()){
                    if(!Augmentation_mammaire.isChecked()&&!Lipofilling_des_seins.isChecked()&&!Reduction_mammaire.isChecked()&&!Changement_de_protheses.isChecked()&&!Lifting_des_seins.isChecked()&&!Gynecomastie.isChecked()&&!autreseins.isChecked()){
                        errordetailsoperationseins.setText("le detail operation sur les seins n'est pas specifier");
                        errordetailsoperationseins.setVisibility(View.VISIBLE);
                    }else{
                        errordetailsoperationseins.setText("");
                        errordetailsoperationseins.setVisibility(View.GONE);
                        if(autreseins.isChecked()&&seinsautre.getText().toString().equals("")){
                            errordetailsoperationseins.setText("l'autre operation sur les seins n'est pas specifier");
                            errordetailsoperationseins.setVisibility(View.VISIBLE);
                        }else{
                            errordetailsoperationseins.setText("");
                            errordetailsoperationseins.setVisibility(View.GONE);
                        }
                    }
                }else{
                    errordetailsoperationseins.setText("");
                    errordetailsoperationseins.setVisibility(View.GONE);
                }
                if(corp.isChecked()){
                    if(!Liposuccion.isChecked()&&!Abdominoplastie.isChecked()&&!Lifting_des_cuisses.isChecked()&&!Augmentation_fesses.isChecked()&&!autrecorp.isChecked()){
                        errordetailsoperationcorp.setText("le detail operation sur le corp n'est pas specifier");
                        errordetailsoperationcorp.setVisibility(View.VISIBLE);
                    }else{
                        errordetailsoperationcorp.setText("");
                        errordetailsoperationcorp.setVisibility(View.GONE);
                        if(autrecorp.isChecked()&&corpautre.getText().toString().equals("")){
                            errordetailsoperationcorp.setText("l'autre operation sur le corp n'est pas specifier");
                            errordetailsoperationcorp.setVisibility(View.VISIBLE);
                        }else{
                            errordetailsoperationcorp.setVisibility(View.GONE);
                            errordetailsoperationcorp.setText("");
                        }
                    }
                }else{
                    errordetailsoperationcorp.setVisibility(View.GONE);
                    errordetailsoperationcorp.setText("");
                }


            }
            if(autre.isChecked()&&autreoperation.getText().toString().equals("")){
                errordetailsoperationautre.setText("l'autre operation  n'est pas specifier");
                errordetailsoperationautre.setVisibility(View.VISIBLE);
            }else{
                errordetailsoperationautre.setText("");
                errordetailsoperationautre.setVisibility(View.GONE);
            }


            if(!erorcompterendu.getText().toString().equals("")||!erorantecedents.getText().toString().equals("")||!eroradresse.getText().toString().equals("")||!erordiag.getText().toString().equals("")||!erortel.getText().toString().equals("")||!erormail.getText().toString().equals("")||!erorprenom.getText().toString().equals("")||!erornom.getText().toString().equals("")|| !erordateNaissance.getText().toString().equals("")||!erordateoperation.getText().toString().equals("")||!errorNum_admission.getText().toString().equals("")||!errordetailsoperation.getText().toString().equals("")||!errordetailsoperationcorp.getText().toString().equals("")||!errordetailsoperationvisage.getText().toString().equals("")||!errordetailsoperationseins.getText().toString().equals("")||!errordetailsoperationautre.getText().toString().equals("")){

                Toast.makeText(AjouterOperation.this,
                        "il ya des champs qui ne sont pas remplie ", Toast.LENGTH_SHORT).show();
            }else{
                boolean ok=true;
                int lengtvisages=-1;
                String []visages= new String[10];;
                String autrevisages = "";
                int lengtseins=-1;
                String[]seinss= new String[10];;
                String autreseinss="";
                int lengtcorps=-1;
                String[]corps= new String[10];;
                String autrecorps="";
                int lengtinterventions=-1;
                String[]interventions = new String[10];
                String autreoperations="";
                if(visage.isChecked()){
                    lengtinterventions++;
                    interventions[lengtinterventions]="visages";
                    if(Lifting_cervico_facial.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Lifting cervico facial";
                    }
                    if(Peeling.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Peeling";
                    }
                    if(Blepharoplastie.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Blepharoplastie";
                    }
                    if(Lifting_temporal.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Lifting temporal";
                    }
                    if(Lifting_complet.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Lifting complet";
                    }
                    if(Genioplastie.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Genioplastie";
                    }
                    if(Otoplastie.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Otoplastie";
                    }
                    if(autrevisage.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="autre";
                        autrevisages=visageautre.getText().toString();
                    }



                }
                if(corp.isChecked()){
                    lengtinterventions++;
                    interventions[lengtinterventions]="corps";
                    if(Liposuccion.isChecked()){
                        lengtcorps++;
                        corps[lengtcorps]="Liposuccion";
                    }
                    if(Abdominoplastie.isChecked()){
                        lengtcorps++;
                        corps[lengtcorps]="Abdominoplastie";
                    }
                    if(Lifting_des_cuisses.isChecked()){
                        lengtcorps++;
                        corps[lengtcorps]="Lifting des cuisses";
                    }
                    if(Augmentation_fesses.isChecked()){
                        lengtcorps++;
                        corps[lengtcorps]="Augmentation fesses";
                    }
                    if(autrecorp.isChecked()){
                        lengtcorps++;
                        corps[lengtcorps]="autre";
                        autrecorps=corpautre.getText().toString();
                    }


                }
                if(seins.isChecked()){
                    lengtinterventions++;
                    interventions[lengtinterventions]="seins";
                    if(Augmentation_mammaire.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="Augmentation mammaire";
                    }
                    if(Lipofilling_des_seins.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="Lipofilling des seins";
                    }
                    if(Reduction_mammaire.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="Reduction mammaire";
                    }
                    if(Changement_de_protheses.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="Changement de protheses";
                    }
                    if(Lifting_des_seins.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="lifting des seins";
                    }
                    if(Gynecomastie.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="Gynecomastie";
                    }
                    if(autreseins.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="autre";
                        autreseinss=seinsautre.getText().toString();
                    }

                }
                if(autre.isChecked()){
                    lengtinterventions++;
                    interventions[lengtinterventions]="autre";
                    autreoperations=autreoperation.getText().toString();

                }


                String token=prefs.getString("token","h");
                String mailauth = prefs.getString("mail", "d");

                String ajousex="";
                if(radiosexfemme.isChecked()){
                    ajousex="Femme";
                }else{
                    ajousex="Homme";
                }
                String operateur;
                String autroperateur="";
                if(radiooperateurtaher.isChecked()){
                    operateur="dr taher";
                    autroperateur="";

                }else{
                    operateur="autre";
                    autroperateur=autreoperateur.getText().toString();
                }
                String equipes;
                String autroequipes="";
                if(radioequipetaher.isChecked()){
                    equipes="dr taher";
                    autroequipes="";

                }else{
                    equipes="autre";
                    autroequipes=autreequipe.getText().toString();
                }

                if(!neew){
                    threadmodification task=new threadmodification(ancienId,num_adminission.getText().toString(),token,mailauth,nom.getText().toString(),prenom.getText().toString(),mail.getText().toString(),tel.getText().toString(),ancienage,ajousex,diagnostic.getText().toString(),antecedents.getText().toString(),compte_rendu_operatoire.getText().toString(),operateur,autroperateur,equipes,autroequipes,adresse.getText().toString(),visages,autrevisages,seinss,autreseinss,corps,autrecorps,interventions,autreoperations,anciendateoperation);
                    Thread thread = new Thread (task);
                    thread.start();
                    try {
                        thread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(task.getAjouter()) {
                        ok=true;
                        Toast.makeText(AjouterOperation.this,
                                "hhhhhhhhhhhhhhhh l'operation a ete modifier avec succes ", Toast.LENGTH_SHORT).show();
                        threadvalideroperation task1=new threadvalideroperation(ancienId);
                        Thread thread1 = new Thread (task1);
                        thread1.start();
                        try {
                            thread1.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if(task1.getAjouter()){
                            Toast.makeText(AjouterOperation.this,
                                    "l'operation a ete valider ", Toast.LENGTH_SHORT).show();
                             if(dr) {

                                 Intent i = new Intent(AjouterOperation.this, Liste_operation.class);
                                 startActivity(i);
                             }else{
                                 Intent i = new Intent(AjouterOperation.this, Liste_operation_brouillant.class);
                                 startActivity(i);
                             }

                        }

                    }else{
                        ok=false;

                        AlertDialog.Builder adb = new AlertDialog.Builder(AjouterOperation.this);

                        adb.setTitle("erreur");

                        adb.setMessage(task.getError());

                        adb.setPositiveButton("Ok", null);

                        adb.show();
                        num_adminission.setFocusable(true);

                    }




                }else {
                    threadAjoutOperation task = new threadAjoutOperation(num_adminission.getText().toString(), token, mailauth, nom.getText().toString(), prenom.getText().toString(), mail.getText().toString(), tel.getText().toString(), dateOperation, ajousex, diagnostic.getText().toString(), antecedents.getText().toString(), compte_rendu_operatoire.getText().toString(), operateur, autroperateur, equipes, autroequipes, adresse.getText().toString(), visages, autrevisages, seinss, autreseinss, corps, autrecorps, interventions, autreoperations, dateOperation);
                    Thread thread = new Thread(task);
                    thread.start();
                    try {
                        thread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (task.getAjouter()) {
                        Toast.makeText(AjouterOperation.this,
                                "l'operation a ete ajouter avec succes ", Toast.LENGTH_SHORT).show();
                    } else {
                        ok = false;

                        AlertDialog.Builder adb = new AlertDialog.Builder(AjouterOperation.this);

                        adb.setTitle("erreur");

                        adb.setMessage(task.getError());

                        adb.setPositiveButton("Ok", null);

                        adb.show();
                        num_adminission.setFocusable(true);

                    }
                    if (ok) {
                        if (dr) {
                            Intent intent = new Intent(AjouterOperation.this,
                                    Liste_operation.class);

                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(AjouterOperation.this,
                                    AjouterOperation.class);
                            intent.putExtra("dr", false);
                            intent.putExtra("nom", "");
                            intent.putExtra("prenom", "");
                            intent.putExtra("mail", "");
                            intent.putExtra("tel", "");
                            intent.putExtra("age", "");
                            intent.putExtra("sexe", "");
                            intent.putExtra("diag", "");
                            intent.putExtra("antecedent", "");
                            intent.putExtra("compterendu", "");
                            intent.putExtra("dateoperation", "");
                            intent.putExtra("operateur", "");
                            intent.putExtra("equipe", "");
                            intent.putExtra("adresse", "");


                            intent.putExtra("visage", false);
                            intent.putExtra("lifting_cervico_facial", false);
                            intent.putExtra("peeling", false);
                            intent.putExtra("blepharoplastie", false);
                            intent.putExtra("lifting_temporal", false);
                            intent.putExtra("lifting_complet", false);
                            intent.putExtra("genioplastie", false);
                            intent.putExtra("otoplastie", false);
                            intent.putExtra("autrevisage", "");

                            intent.putExtra("seins", false);
                            intent.putExtra("augmentation_mammaire", false);
                            intent.putExtra("lipofilling_des_seins", false);
                            intent.putExtra("reduction_mammaire(", false);
                            intent.putExtra("changement_de_protheses", false);
                            intent.putExtra("lifting_des_seins", false);
                            intent.putExtra("gynecomastie", false);
                            intent.putExtra("autreseins", "");

                            intent.putExtra("corp", false);
                            intent.putExtra("liposuccion", false);
                            intent.putExtra("abdominoplastie", false);
                            intent.putExtra("lifting_des_cuisses", false);
                            intent.putExtra("Augmentation_fesse", false);
                            intent.putExtra("autrecorp", "");

                            intent.putExtra("autre", "");
                            intent.putExtra("id", "");
                            intent.putExtra("new", true);

                            startActivity(intent);


                        }
                    }

                }

            }

        }

        return super.onOptionsItemSelected(item);
    }
}
