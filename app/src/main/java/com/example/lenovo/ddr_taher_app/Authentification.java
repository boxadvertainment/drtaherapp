package com.example.lenovo.ddr_taher_app;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import android.widget.CompoundButton.OnCheckedChangeListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.Buffer;


public class Authentification extends ActionBarActivity implements  AdapterView.OnItemSelectedListener, ViewSwitcher.ViewFactory {
    Toolbar toolbar;
    Thread splashTread;
    EditText userName;
    EditText password;
    TextView errorcnx;
    CheckBox mCbShowPwd;
    String mail;
    String pass;
    String error;
    boolean dr;
    ImageSwitcher mSwitcher;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentification);
        dr=false;
         prefs = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        String token = prefs.getString("token", "d");
        Toast.makeText(Authentification.this,token,Toast.LENGTH_LONG).show();


        toolbar=(Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        mSwitcher = (ImageSwitcher) findViewById(R.id.imageSwitcher1);
        mSwitcher.setFactory(this);
        mSwitcher.setInAnimation(AnimationUtils.loadAnimation(this,
                android.R.anim.fade_in));
        mSwitcher.setOutAnimation(AnimationUtils.loadAnimation(this,
                android.R.anim.fade_out));

        Gallery g = (Gallery) findViewById(R.id.gallery1);
        g.setAdapter(new imageAdaptater(this));
        g.setOnItemSelectedListener(this);



        Button loginbutton = (Button)findViewById(R.id.btn_login);

        Button cancelbutton = (Button) findViewById(R.id.btn_cancel);

        userName = (EditText) findViewById(R.id.txt_name);

         password = (EditText) findViewById(R.id.password);

         errorcnx = (TextView) findViewById(R.id.errorcnx);
        errorcnx.setText("");

         mCbShowPwd=(CheckBox) findViewById(R.id.cbShowPwd);
        // when user clicks on this checkbox, this is the handler.
        mCbShowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    // show password
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

                }


            }
        });




        loginbutton.setOnClickListener(new View.OnClickListener() {





            public void onClick(View v) {
                mail=userName.getText().toString();
                pass=password.getText().toString();

                threadAuthentification task=new threadAuthentification(pass,mail);
                Thread thread = new Thread (task);
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                errorcnx.setText(task.getEror());
                if(errorcnx.getText().toString().equals("")){
                    if(task.getIsadmin()){
                        prefs.edit().putString("mail", mail).apply();
                        Intent intent=new Intent(Authentification.this,Liste_operation.class);
                        intent.putExtra("qr","");
                        startActivity(intent);
                    }else{
                        prefs.edit().putString("mail", mail).apply();
                        Intent intent = new Intent(Authentification.this,
                                AjouterOperation.class);
                        intent.putExtra("dr",false);
                        intent.putExtra("nom", "");
                        intent.putExtra("prenom", "");
                        intent.putExtra("mail", "");
                        intent.putExtra("tel", "");
                        intent.putExtra("age", "");
                        intent.putExtra("sexe", "");
                        intent.putExtra("diag", "");
                        intent.putExtra("antecedent", "");
                        intent.putExtra("compterendu", "");
                        intent.putExtra("dateoperation", "");
                        intent.putExtra("operateur", "");
                        intent.putExtra("equipe", "");
                        intent.putExtra("adresse","");


                        intent.putExtra("visage",false);
                        intent.putExtra("lifting_cervico_facial",false);
                        intent.putExtra("peeling",false);
                        intent.putExtra("blepharoplastie",false);
                        intent.putExtra("lifting_temporal",false);
                        intent.putExtra("lifting_complet",false);
                        intent.putExtra("genioplastie",false);
                        intent.putExtra("otoplastie",false);
                        intent.putExtra("autrevisage","");

                        intent.putExtra("seins",false);
                        intent.putExtra("augmentation_mammaire",false);
                        intent.putExtra("lipofilling_des_seins",false);
                        intent.putExtra("reduction_mammaire(",false);
                        intent.putExtra("changement_de_protheses",false);
                        intent.putExtra("lifting_des_seins",false);
                        intent.putExtra("gynecomastie",false);
                        intent.putExtra("autreseins","");

                        intent.putExtra("corp",false);
                        intent.putExtra("liposuccion",false);
                        intent.putExtra("abdominoplastie",false);
                        intent.putExtra("lifting_des_cuisses",false);
                        intent.putExtra("Augmentation_fesse",false);
                        intent.putExtra("autrecorp",false);

                        intent.putExtra("autre","");
                        intent.putExtra("admission","");
                        intent.putExtra("id","");
                        intent.putExtra("new",true);
                        startActivity(intent);
                    }
                }















            }





        });



        cancelbutton.setOnClickListener(new View.OnClickListener() {


            @Override

            public void onClick(View v) {

                userName.setText("");
                password.setText("");

            }

        });




    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_authentification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mSwitcher.setImageResource(imageAdaptater.mImageIds[position]);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public View makeView() {
        ImageView i = new ImageView(this);
        i.setBackgroundColor(0xFF000000);
        i.setScaleType(ImageView.ScaleType.FIT_CENTER);
        i.setLayoutParams(new ImageSwitcher.LayoutParams(ImageSwitcher.LayoutParams.MATCH_PARENT,
                ImageSwitcher.LayoutParams.MATCH_PARENT));
        return i;
    }
}


