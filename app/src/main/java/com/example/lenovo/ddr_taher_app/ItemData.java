package com.example.lenovo.ddr_taher_app;

import android.graphics.Bitmap;

/**
 * Created by lenovo on 14/06/2015.
 */
public class ItemData {
    private String id;
    private String num_admission;

    private String title;
    private int imageUrl;
    private String nom;
    private String prenom;
    private String mail;
    private String tel;
    private String age;
    private String sexe;
    private String diagnostic;
    private String antecedent;
    private String compterendu;
    private String operateur;
    private String equipe;
    private String adresse;

    private boolean visage;
    private boolean Lifting_cervico_facial;
    private boolean Peeling;
    private boolean Blepharoplastie;
    private boolean Lifting_temporal;
    private boolean Lifting_complet ;
    private boolean Genioplastie;
    private boolean Otoplastie;
    private String autrevisage;

    private boolean seins;
    private boolean Augmentation_mammaire ;
    private boolean Lipofilling_des_seins;
    private boolean Reduction_mammaire;
    private boolean  Changement_de_protheses;
    private boolean  Lifting_des_seins;
    private boolean Gynecomastie;
    private String autreseins;

    private boolean corp ;
    private boolean Liposuccion;
    private boolean Abdominoplastie;
    private boolean  Lifting_des_cuisses;
    private boolean Augmentation_fesses ;
    private String autrecorp;

    private String autre;


    private String nom_operation;
    private String date_operation;

    private String avant1;
    private String avant2;
    private String avant3;
    private String avant4;
    private String avant5;

    private String apres1;
    private String apres2;
    private String apres3;
    private String apres4;
    private String apres5;

    private String descavant1;
    private String descavant2;
    private String descavant3;
    private String descavant4;
    private String descavant5;

    private String descapres1;
    private String descapres2;
    private String descapres3;
    private String descapres4;
    private String descapres5;

    private boolean estavant1;
    private boolean estavant2;
    private boolean estavant3;
    private boolean estavant4;
    private boolean estavant5;

    private boolean estapres1;
    private boolean estapres2;
    private boolean estapres3;
    private boolean estapres4;
    private boolean estapres5;




    public ItemData(String id,String num_admission,  String nom, String prenom, String mail, String tel, String age, String sexe,int imageUrl,String nom_operation, String date_operation , String diagnostic, String antecedent, String compterendu, String operateur, String equipe, String adresse, boolean visage, boolean lifting_cervico_facial, boolean peeling, boolean blepharoplastie, boolean lifting_temporal, boolean lifting_complet, boolean genioplastie, boolean otoplastie, String autrevisage, boolean seins, boolean augmentation_mammaire, boolean lipofilling_des_seins, boolean reduction_mammaire, boolean changement_de_protheses, boolean lifting_des_seins, boolean gynecomastie, String autreseins, boolean corp, boolean liposuccion, boolean abdominoplastie, boolean lifting_des_cuisses, boolean augmentation_fesses, String autrecorp, String autre) {
       this.id=id;
        this.num_admission=num_admission;
        this.imageUrl = imageUrl;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.tel = tel;
        this.age = age;
        this.sexe = sexe;
        this.diagnostic = diagnostic;
        this.antecedent = antecedent;
        this.compterendu = compterendu;
        this.operateur = operateur;
        this.equipe = equipe;
        this.adresse = adresse;
        this.visage = visage;
        Lifting_cervico_facial = lifting_cervico_facial;
        Peeling = peeling;
        Blepharoplastie = blepharoplastie;
        Lifting_temporal = lifting_temporal;
        Lifting_complet = lifting_complet;
        Genioplastie = genioplastie;
        Otoplastie = otoplastie;
        this.autrevisage = autrevisage;
        this.seins = seins;
        Augmentation_mammaire = augmentation_mammaire;
        Lipofilling_des_seins = lipofilling_des_seins;
        Reduction_mammaire = reduction_mammaire;
        Changement_de_protheses = changement_de_protheses;
        Lifting_des_seins = lifting_des_seins;
        Gynecomastie = gynecomastie;
        this.autreseins = autreseins;
        this.corp = corp;
        Liposuccion = liposuccion;
        Abdominoplastie = abdominoplastie;
        Lifting_des_cuisses = lifting_des_cuisses;
        Augmentation_fesses = augmentation_fesses;
        this.autrecorp = autrecorp;
        this.autre = autre;
        this.nom_operation = nom_operation;
        this.date_operation = date_operation;
    }

    public ItemData(String id,String num_admission,  String nom, String prenom, String mail, String tel, String age, String sexe,int imageUrl,String nom_operation, String date_operation , String diagnostic, String antecedent, String compterendu, String operateur, String equipe, String adresse, boolean visage, boolean lifting_cervico_facial, boolean peeling, boolean blepharoplastie, boolean lifting_temporal, boolean lifting_complet, boolean genioplastie, boolean otoplastie, String autrevisage, boolean seins, boolean augmentation_mammaire, boolean lipofilling_des_seins, boolean reduction_mammaire, boolean changement_de_protheses, boolean lifting_des_seins, boolean gynecomastie, String autreseins, boolean corp, boolean liposuccion, boolean abdominoplastie, boolean lifting_des_cuisses, boolean augmentation_fesses, String autrecorp, String autre ,boolean estavant1,String avant1,String descavant1,boolean estavant2,String avant2,String descavant2,boolean estavant3,String avant3,String descavant3,boolean estavant4,String avant4,String descavant4,boolean estavant5,String avant5,String descavant5,boolean estapres1,String apres1,String descapres1,boolean estapres2,String apres2,String descapres2,boolean estapres3,String apres3,String descapres3,boolean estapres4,String apres4,String descapres4,boolean estapres5,String apres5,String descapres5) {
        this.id=id;
        this.num_admission=num_admission;
        this.imageUrl = imageUrl;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.tel = tel;
        this.age = age;
        this.sexe = sexe;
        this.diagnostic = diagnostic;
        this.antecedent = antecedent;
        this.compterendu = compterendu;
        this.operateur = operateur;
        this.equipe = equipe;
        this.adresse = adresse;
        this.visage = visage;
        Lifting_cervico_facial = lifting_cervico_facial;
        Peeling = peeling;
        Blepharoplastie = blepharoplastie;
        Lifting_temporal = lifting_temporal;
        Lifting_complet = lifting_complet;
        Genioplastie = genioplastie;
        Otoplastie = otoplastie;
        this.autrevisage = autrevisage;
        this.seins = seins;
        Augmentation_mammaire = augmentation_mammaire;
        Lipofilling_des_seins = lipofilling_des_seins;
        Reduction_mammaire = reduction_mammaire;
        Changement_de_protheses = changement_de_protheses;
        Lifting_des_seins = lifting_des_seins;
        Gynecomastie = gynecomastie;
        this.autreseins = autreseins;
        this.corp = corp;
        Liposuccion = liposuccion;
        Abdominoplastie = abdominoplastie;
        Lifting_des_cuisses = lifting_des_cuisses;
        Augmentation_fesses = augmentation_fesses;
        this.autrecorp = autrecorp;
        this.autre = autre;
        this.nom_operation = nom_operation;
        this.date_operation = date_operation;

        this.avant1=avant1;
        this.descavant1=descavant1;
        this.avant2=avant2;
        this.descavant2=descavant2;
        this.avant3=avant3;
        this.descavant3=descavant3;
        this.avant4=avant4;
        this.descavant4=descavant4;
        this.avant5=avant5;
        this.descavant5=descavant5;


        this.apres1=apres1;
        this.descapres1=descapres1;
        this.apres2=apres2;
        this.descapres2=descapres2;
        this.apres3=apres3;
        this.descapres3=descapres3;
        this.apres4=apres4;
        this.descapres4=descapres4;
        this.apres5=apres5;
        this.descapres5=descapres5;

        this.estavant1=estavant1;
        this.estavant2=estavant2;
        this.estavant3=estavant3;
        this.estavant4=estavant4;
        this.estavant5=estavant5;

        this.estapres1=estapres1;
        this.estapres2=estapres2;
        this.estapres3=estapres3;
        this.estapres4=estapres4;
        this.estapres5=estapres5;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNum_admission() {
        return num_admission;
    }

    public void setNum_admission(String num_admission) {
        this.num_admission = num_admission;
    }

    public String getTitle() {
        return nom+" "+prenom;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDateOperation() {
        return date_operation;
    }

    public void setDateOperation(String date_operation) {
        this.date_operation=date_operation;
    }

    public String getNomOperation() {
        return nom_operation;
    }

    public void setNomOperation(String nom_operation) {
        this.nom_operation=nom_operation;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(String diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getAntecedent() {
        return antecedent;
    }

    public void setAntecedent(String antecedent) {
        this.antecedent = antecedent;
    }

    public String getCompterendu() {
        return compterendu;
    }

    public void setCompterendu(String compterendu) {
        this.compterendu = compterendu;
    }
    public String getOperateur() {
        return operateur;
    }

    public void setOperateur(String operateur) {
        this.operateur = operateur;
    }

    public String getEquipe() {
        return equipe;
    }

    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public boolean isVisage() {
        return visage;
    }

    public void setVisage(boolean visage) {
        this.visage = visage;
    }

    public boolean isLifting_cervico_facial() {
        return Lifting_cervico_facial;
    }

    public void setLifting_cervico_facial(boolean lifting_cervico_facial) {
        Lifting_cervico_facial = lifting_cervico_facial;
    }

    public boolean isPeeling() {
        return Peeling;
    }

    public void setPeeling(boolean peeling) {
        Peeling = peeling;
    }

    public boolean isBlepharoplastie() {
        return Blepharoplastie;
    }

    public void setBlepharoplastie(boolean blepharoplastie) {
        Blepharoplastie = blepharoplastie;
    }

    public boolean isLifting_temporal() {
        return Lifting_temporal;
    }

    public void setLifting_temporal(boolean lifting_temporal) {
        Lifting_temporal = lifting_temporal;
    }

    public boolean isLifting_complet() {
        return Lifting_complet;
    }

    public void setLifting_complet(boolean lifting_complet) {
        Lifting_complet = lifting_complet;
    }

    public boolean isGenioplastie() {
        return Genioplastie;
    }

    public void setGenioplastie(boolean genioplastie) {
        Genioplastie = genioplastie;
    }

    public boolean isOtoplastie() {
        return Otoplastie;
    }

    public void setOtoplastie(boolean otoplastie) {
        Otoplastie = otoplastie;
    }

    public String getAutrevisage() {
        return autrevisage;
    }

    public void setAutrevisage(String autrevisage) {
        this.autrevisage = autrevisage;
    }

    public boolean isSeins() {
        return seins;
    }

    public void setSeins(boolean seins) {
        this.seins = seins;
    }

    public boolean isAugmentation_mammaire() {
        return Augmentation_mammaire;
    }

    public void setAugmentation_mammaire(boolean augmentation_mammaire) {
        Augmentation_mammaire = augmentation_mammaire;
    }

    public boolean isLipofilling_des_seins() {
        return Lipofilling_des_seins;
    }

    public void setLipofilling_des_seins(boolean lipofilling_des_seins) {
        Lipofilling_des_seins = lipofilling_des_seins;
    }

    public boolean isReduction_mammaire() {
        return Reduction_mammaire;
    }

    public void setReduction_mammaire(boolean reduction_mammaire) {
        Reduction_mammaire = reduction_mammaire;
    }

    public boolean isChangement_de_protheses() {
        return Changement_de_protheses;
    }

    public void setChangement_de_protheses(boolean changement_de_protheses) {
        Changement_de_protheses = changement_de_protheses;
    }

    public boolean isLifting_des_seins() {
        return Lifting_des_seins;
    }

    public void setLifting_des_seins(boolean lifting_des_seins) {
        Lifting_des_seins = lifting_des_seins;
    }

    public boolean isGynecomastie() {
        return Gynecomastie;
    }

    public void setGynecomastie(boolean gynecomastie) {
        Gynecomastie = gynecomastie;
    }

    public String getAutreseins() {
        return autreseins;
    }

    public void setAutreseins(String autreseins) {
        this.autreseins = autreseins;
    }

    public boolean isCorp() {
        return corp;
    }

    public void setCorp(boolean corp) {
        this.corp = corp;
    }

    public boolean isLiposuccion() {
        return Liposuccion;
    }

    public void setLiposuccion(boolean liposuccion) {
        Liposuccion = liposuccion;
    }

    public boolean isAbdominoplastie() {
        return Abdominoplastie;
    }

    public void setAbdominoplastie(boolean abdominoplastie) {
        Abdominoplastie = abdominoplastie;
    }

    public boolean isLifting_des_cuisses() {
        return Lifting_des_cuisses;
    }

    public void setLifting_des_cuisses(boolean lifting_des_cuisses) {
        Lifting_des_cuisses = lifting_des_cuisses;
    }

    public boolean isAugmentation_fesses() {
        return Augmentation_fesses;
    }

    public void setAugmentation_fesses(boolean augmentation_fesses) {
        Augmentation_fesses = augmentation_fesses;
    }

    public String getAutrecorp() {
        return autrecorp;
    }

    public void setAutrecorp(String autrecorp) {
        this.autrecorp = autrecorp;
    }

    public String getAutre() {
        return autre;
    }

    public void setAutre(String autre) {
        this.autre = autre;
    }

    public String getAvant1() {
        return avant1;
    }

    public void setAvant1(String avant1) {
        this.avant1 = avant1;
    }

    public String getAvant2() {
        return avant2;
    }

    public void setAvant2(String avant2) {
        this.avant2 = avant2;
    }

    public String getAvant3() {
        return avant3;
    }

    public void setAvant3(String avant3) {
        this.avant3 = avant3;
    }

    public String getAvant4() {
        return avant4;
    }

    public void setAvant4(String avant4) {
        this.avant4 = avant4;
    }

    public String getAvant5() {
        return avant5;
    }

    public void setAvant5(String avant5) {
        this.avant5 = avant5;
    }

    public String getApres1() {
        return apres1;
    }

    public void setApres1(String apres1) {
        this.apres1 = apres1;
    }

    public String getApres2() {
        return apres2;
    }

    public void setApres2(String apres2) {
        this.apres2 = apres2;
    }

    public String getApres3() {
        return apres3;
    }

    public void setApres3(String apres3) {
        this.apres3 = apres3;
    }

    public String getApres4() {
        return apres4;
    }

    public void setApres4(String apres4) {
        this.apres4 = apres4;
    }

    public String getApres5() {
        return apres5;
    }

    public void setApres5(String apres5) {
        this.apres5 = apres5;
    }

    public String getDescavant1() {
        return descavant1;
    }

    public void setDescavant1(String descavant1) {
        this.descavant1 = descavant1;
    }

    public String getDescavant2() {
        return descavant2;
    }

    public void setDescavant2(String descavant2) {
        this.descavant2 = descavant2;
    }

    public String getDescavant3() {
        return descavant3;
    }

    public void setDescavant3(String descavant3) {
        this.descavant3 = descavant3;
    }

    public String getDescavant4() {
        return descavant4;
    }

    public void setDescavant4(String descavant4) {
        this.descavant4 = descavant4;
    }

    public String getDescavant5() {
        return descavant5;
    }

    public void setDescavant5(String descavant5) {
        this.descavant5 = descavant5;
    }

    public String getDescapres1() {
        return descapres1;
    }

    public void setDescapres1(String descapres1) {
        this.descapres1 = descapres1;
    }

    public String getDescapres2() {
        return descapres2;
    }

    public void setDescapres2(String descapres2) {
        this.descapres2 = descapres2;
    }

    public String getDescapres3() {
        return descapres3;
    }

    public void setDescapres3(String descapres3) {
        this.descapres3 = descapres3;
    }

    public String getDescapres4() {
        return descapres4;
    }

    public void setDescapres4(String descapres4) {
        this.descapres4 = descapres4;
    }

    public String getDescapres5() {
        return descapres5;
    }

    public void setDescapres5(String descapres5) {
        this.descapres5 = descapres5;
    }

    public boolean isEstavant1() {
        return estavant1;
    }

    public void setEstavant1(boolean estavant1) {
        this.estavant1 = estavant1;
    }

    public boolean isEstavant2() {
        return estavant2;
    }

    public void setEstavant2(boolean estavant2) {
        this.estavant2 = estavant2;
    }

    public boolean isEstavant3() {
        return estavant3;
    }

    public void setEstavant3(boolean estavant3) {
        this.estavant3 = estavant3;
    }

    public boolean isEstavant4() {
        return estavant4;
    }

    public void setEstavant4(boolean estavant4) {
        this.estavant4 = estavant4;
    }

    public boolean isEstavant5() {
        return estavant5;
    }

    public void setEstavant5(boolean estavant5) {
        this.estavant5 = estavant5;
    }

    public boolean isEstapres1() {
        return estapres1;
    }

    public void setEstapres1(boolean estapres1) {
        this.estapres1 = estapres1;
    }

    public boolean isEstapres2() {
        return estapres2;
    }

    public void setEstapres2(boolean estapres2) {
        this.estapres2 = estapres2;
    }

    public boolean isEstapres3() {
        return estapres3;
    }

    public void setEstapres3(boolean estapres3) {
        this.estapres3 = estapres3;
    }

    public boolean isEstapres4() {
        return estapres4;
    }

    public void setEstapres4(boolean estapres4) {
        this.estapres4 = estapres4;
    }

    public boolean isEstapres5() {
        return estapres5;
    }

    public void setEstapres5(boolean estapres5) {
        this.estapres5 = estapres5;
    }
}