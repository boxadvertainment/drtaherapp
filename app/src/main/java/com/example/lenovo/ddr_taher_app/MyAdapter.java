package com.example.lenovo.ddr_taher_app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private ItemData[] itemsData;
    private Context context;

    public MyAdapter(ItemData[] itemsData) {
        this.itemsData = itemsData;

    }

    public MyAdapter(ItemData[] itemsData ,Context context) {
        this.itemsData = itemsData;
        this.context=context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemlayoutview, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        viewHolder.txtViewTitle.setText(itemsData[position].getTitle());
        viewHolder.imgViewIcon.setImageResource(itemsData[position].getImageUrl());
        viewHolder.nom_operation.setText(itemsData[position].getNomOperation());
        viewHolder.date_operation.setText(itemsData[position].getDateOperation());

        viewHolder.imgViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,detailOperation.class);
                intent.putExtra("modif",true);
                intent.putExtra("nom",itemsData[position].getNom());
                intent.putExtra("prenom",itemsData[position].getPrenom());
                intent.putExtra("mail",itemsData[position].getMail());
                intent.putExtra("tel",itemsData[position].getTel());
                intent.putExtra("age",itemsData[position].getAge());
                intent.putExtra("sexe",itemsData[position].getSexe());
                intent.putExtra("diag",itemsData[position].getDiagnostic());
                intent.putExtra("antecedent",itemsData[position].getAntecedent());
                intent.putExtra("compterendu",itemsData[position].getCompterendu());
                intent.putExtra("dateoperation",itemsData[position].getDateOperation());
                intent.putExtra("operateur",itemsData[position].getOperateur());
                intent.putExtra("equipe",itemsData[position].getEquipe());
                intent.putExtra("adresse",itemsData[position].getAdresse());

                intent.putExtra("visage",itemsData[position].isVisage());
                intent.putExtra("lifting_cervico_facial",itemsData[position].isLifting_cervico_facial());
                intent.putExtra("peeling",itemsData[position].isPeeling());
                intent.putExtra("blepharoplastie",itemsData[position].isBlepharoplastie());
                intent.putExtra("lifting_temporal",itemsData[position].isLifting_temporal());
                intent.putExtra("lifting_complet",itemsData[position].isLifting_complet());
                intent.putExtra("genioplastie",itemsData[position].isGenioplastie());
                intent.putExtra("otoplastie",itemsData[position].isOtoplastie());
                intent.putExtra("autrevisage",itemsData[position].getAutrevisage());

                intent.putExtra("seins",itemsData[position].isSeins());
                intent.putExtra("augmentation_mammaire",itemsData[position].isAugmentation_mammaire());
                intent.putExtra("lipofilling_des_seins",itemsData[position].isLipofilling_des_seins());
                intent.putExtra("reduction_mammaire(",itemsData[position].isReduction_mammaire());
                intent.putExtra("changement_de_protheses",itemsData[position].isChangement_de_protheses());
                intent.putExtra("lifting_des_seins",itemsData[position].isLifting_des_seins());
                intent.putExtra("gynecomastie",itemsData[position].isGynecomastie());
                intent.putExtra("autreseins", itemsData[position].getAutreseins());

                intent.putExtra("corp",itemsData[position].isCorp());
                intent.putExtra("liposuccion",itemsData[position].isLiposuccion());
                intent.putExtra("abdominoplastie", itemsData[position].isAbdominoplastie());
                intent.putExtra("lifting_des_cuisses",itemsData[position].isLifting_des_cuisses());
                intent.putExtra("Augmentation_fesse",itemsData[position].isAugmentation_fesses());
                intent.putExtra("autrecorp", itemsData[position].getAutrecorp());

                intent.putExtra("autre",itemsData[position].getAutre());
                intent.putExtra("admission",itemsData[position].getNum_admission());
                intent.putExtra("id", itemsData[position].getId());


                intent.putExtra("avant1",itemsData[position].getAvant1());
                intent.putExtra("descavant1", itemsData[position].getDescavant1());


                intent.putExtra("avant2",itemsData[position].getAvant2());
                intent.putExtra("descavant2",itemsData[position].getDescavant2());


                intent.putExtra("avant3",itemsData[position].getAvant3());
                intent.putExtra("descavant3", itemsData[position].getDescavant3());


                intent.putExtra("avant4",itemsData[position].getAvant4());
                intent.putExtra("descavant4",itemsData[position].getDescavant4());


                intent.putExtra("avant5",itemsData[position].getAvant5());
                intent.putExtra("descavant5",itemsData[position].getDescavant5());



                intent.putExtra("apres1",itemsData[position].getApres1());
                intent.putExtra("descapres1", itemsData[position].getDescapres1());


                intent.putExtra("apres2",itemsData[position].getApres2());
                intent.putExtra("descapres2",itemsData[position].getDescapres2());


                intent.putExtra("apres3",itemsData[position].getApres3());
                intent.putExtra("descapres3",itemsData[position].getDescapres3());


                intent.putExtra("apres4",itemsData[position].getApres4());
                intent.putExtra("descapres4", itemsData[position].getDescapres4());


                intent.putExtra("apres5",itemsData[position].getApres5());
                intent.putExtra("descapres5",itemsData[position].getDescapres5());

                intent.putExtra("isavant1",itemsData[position].isEstavant1());
                intent.putExtra("isavant2",itemsData[position].isEstavant2());
                intent.putExtra("isavant3",itemsData[position].isEstavant3());
                intent.putExtra("isavant4",itemsData[position].isEstavant4());
                intent.putExtra("isavant5",itemsData[position].isEstavant5());

                intent.putExtra("isapres1",itemsData[position].isEstapres1());
                intent.putExtra("isapres2",itemsData[position].isEstapres2());
                intent.putExtra("isapres3",itemsData[position].isEstapres3());
                intent.putExtra("isapres4",itemsData[position].isEstapres4());
                intent.putExtra("isapres5",itemsData[position].isEstapres5());

                context.startActivity(intent);

            }
        });

        viewHolder.txtViewTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,detailOperation.class);
                intent.putExtra("modif",true);
                intent.putExtra("nom",itemsData[position].getNom());
                intent.putExtra("prenom",itemsData[position].getPrenom());
                intent.putExtra("mail",itemsData[position].getMail());
                intent.putExtra("tel",itemsData[position].getTel());
                intent.putExtra("age",itemsData[position].getAge());
                intent.putExtra("sexe",itemsData[position].getSexe());
                intent.putExtra("diag",itemsData[position].getDiagnostic());
                intent.putExtra("antecedent",itemsData[position].getAntecedent());
                intent.putExtra("compterendu",itemsData[position].getCompterendu());
                intent.putExtra("dateoperation",itemsData[position].getDateOperation());
                intent.putExtra("operateur",itemsData[position].getOperateur());
                intent.putExtra("equipe",itemsData[position].getEquipe());
                intent.putExtra("adresse",itemsData[position].getAdresse());

                intent.putExtra("visage",itemsData[position].isVisage());
                intent.putExtra("lifting_cervico_facial",itemsData[position].isLifting_cervico_facial());
                intent.putExtra("peeling",itemsData[position].isPeeling());
                intent.putExtra("blepharoplastie",itemsData[position].isBlepharoplastie());
                intent.putExtra("lifting_temporal",itemsData[position].isLifting_temporal());
                intent.putExtra("lifting_complet",itemsData[position].isLifting_complet());
                intent.putExtra("genioplastie",itemsData[position].isGenioplastie());
                intent.putExtra("otoplastie",itemsData[position].isOtoplastie());
                intent.putExtra("autrevisage",itemsData[position].getAutrevisage());

                intent.putExtra("seins",itemsData[position].isSeins());
                intent.putExtra("augmentation_mammaire", itemsData[position].isAugmentation_mammaire());
                intent.putExtra("lipofilling_des_seins",itemsData[position].isLipofilling_des_seins());
                intent.putExtra("reduction_mammaire(",itemsData[position].isReduction_mammaire());
                intent.putExtra("changement_de_protheses",itemsData[position].isChangement_de_protheses());
                intent.putExtra("lifting_des_seins",itemsData[position].isLifting_des_seins());
                intent.putExtra("gynecomastie",itemsData[position].isGynecomastie());
                intent.putExtra("autreseins",itemsData[position].getAutreseins());

                intent.putExtra("corp",itemsData[position].isCorp());
                intent.putExtra("liposuccion", itemsData[position].isLiposuccion());
                intent.putExtra("abdominoplastie",itemsData[position].isAbdominoplastie());
                intent.putExtra("lifting_des_cuisses",itemsData[position].isLifting_des_cuisses());
                intent.putExtra("Augmentation_fesse",itemsData[position].isAugmentation_fesses());
                intent.putExtra("autrecorp", itemsData[position].getAutrecorp());

                intent.putExtra("autre",itemsData[position].getAutre());
                intent.putExtra("admission",itemsData[position].getNum_admission());
                intent.putExtra("id",itemsData[position].getId());
                intent.putExtra("avant1",itemsData[position].getAvant1());
                intent.putExtra("descavant1", itemsData[position].getDescavant1());


                intent.putExtra("avant2",itemsData[position].getAvant2());
                intent.putExtra("descavant2",itemsData[position].getDescavant2());


                intent.putExtra("avant3",itemsData[position].getAvant3());
                intent.putExtra("descavant3", itemsData[position].getDescavant3());


                intent.putExtra("avant4",itemsData[position].getAvant4());
                intent.putExtra("descavant4",itemsData[position].getDescavant4());


                intent.putExtra("avant5",itemsData[position].getAvant5());
                intent.putExtra("descavant5",itemsData[position].getDescavant5());



                intent.putExtra("apres1",itemsData[position].getApres1());
                intent.putExtra("descapres1", itemsData[position].getDescapres1());


                intent.putExtra("apres2",itemsData[position].getApres2());
                intent.putExtra("descapres2",itemsData[position].getDescapres2());


                intent.putExtra("apres3",itemsData[position].getApres3());
                intent.putExtra("descapres3",itemsData[position].getDescapres3());


                intent.putExtra("apres4",itemsData[position].getApres4());
                intent.putExtra("descapres4", itemsData[position].getDescapres4());


                intent.putExtra("apres5",itemsData[position].getApres5());
                intent.putExtra("descapres5",itemsData[position].getDescapres5());

                intent.putExtra("isavant1",itemsData[position].isEstavant1());
                intent.putExtra("isavant2",itemsData[position].isEstavant2());
                intent.putExtra("isavant3",itemsData[position].isEstavant3());
                intent.putExtra("isavant4",itemsData[position].isEstavant4());
                intent.putExtra("isavant5",itemsData[position].isEstavant5());

                intent.putExtra("isapres1",itemsData[position].isEstapres1());
                intent.putExtra("isapres2",itemsData[position].isEstapres2());
                intent.putExtra("isapres3",itemsData[position].isEstapres3());
                intent.putExtra("isapres4",itemsData[position].isEstapres4());
                intent.putExtra("isapres5",itemsData[position].isEstapres5());
                context.startActivity(intent);
            }
        });
        viewHolder.nom_operation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,detailOperation.class);
                intent.putExtra("modif",true);
                intent.putExtra("nom",itemsData[position].getNom());
                intent.putExtra("prenom",itemsData[position].getPrenom());
                intent.putExtra("mail",itemsData[position].getMail());
                intent.putExtra("tel",itemsData[position].getTel());
                intent.putExtra("age",itemsData[position].getAge());
                intent.putExtra("sexe",itemsData[position].getSexe());
                intent.putExtra("diag",itemsData[position].getDiagnostic());
                intent.putExtra("antecedent",itemsData[position].getAntecedent());
                intent.putExtra("compterendu",itemsData[position].getCompterendu());
                intent.putExtra("dateoperation",itemsData[position].getDateOperation());
                intent.putExtra("operateur",itemsData[position].getOperateur());
                intent.putExtra("equipe",itemsData[position].getEquipe());
                intent.putExtra("adresse",itemsData[position].getAdresse());

                intent.putExtra("visage",itemsData[position].isVisage());
                intent.putExtra("lifting_cervico_facial",itemsData[position].isLifting_cervico_facial());
                intent.putExtra("peeling",itemsData[position].isPeeling());
                intent.putExtra("blepharoplastie",itemsData[position].isBlepharoplastie());
                intent.putExtra("lifting_temporal",itemsData[position].isLifting_temporal());
                intent.putExtra("lifting_complet",itemsData[position].isLifting_complet());
                intent.putExtra("genioplastie",itemsData[position].isGenioplastie());
                intent.putExtra("otoplastie",itemsData[position].isOtoplastie());
                intent.putExtra("autrevisage",itemsData[position].getAutrevisage());

                intent.putExtra("seins",itemsData[position].isSeins());
                intent.putExtra("augmentation_mammaire", itemsData[position].isAugmentation_mammaire());
                intent.putExtra("lipofilling_des_seins",itemsData[position].isLipofilling_des_seins());
                intent.putExtra("reduction_mammaire(",itemsData[position].isReduction_mammaire());
                intent.putExtra("changement_de_protheses",itemsData[position].isChangement_de_protheses());
                intent.putExtra("lifting_des_seins",itemsData[position].isLifting_des_seins());
                intent.putExtra("gynecomastie",itemsData[position].isGynecomastie());
                intent.putExtra("autreseins",itemsData[position].getAutreseins());

                intent.putExtra("corp",itemsData[position].isCorp());
                intent.putExtra("liposuccion", itemsData[position].isLiposuccion());
                intent.putExtra("abdominoplastie",itemsData[position].isAbdominoplastie());
                intent.putExtra("lifting_des_cuisses",itemsData[position].isLifting_des_cuisses());
                intent.putExtra("Augmentation_fesse",itemsData[position].isAugmentation_fesses());
                intent.putExtra("autrecorp", itemsData[position].getAutrecorp());

                intent.putExtra("autre",itemsData[position].getAutre());
                intent.putExtra("admission",itemsData[position].getNum_admission());
                intent.putExtra("id",itemsData[position].getId());

                intent.putExtra("avant1",itemsData[position].getAvant1());
                intent.putExtra("descavant1", itemsData[position].getDescavant1());


                intent.putExtra("avant2",itemsData[position].getAvant2());
                intent.putExtra("descavant2",itemsData[position].getDescavant2());


                intent.putExtra("avant3",itemsData[position].getAvant3());
                intent.putExtra("descavant3", itemsData[position].getDescavant3());


                intent.putExtra("avant4",itemsData[position].getAvant4());
                intent.putExtra("descavant4",itemsData[position].getDescavant4());


                intent.putExtra("avant5",itemsData[position].getAvant5());
                intent.putExtra("descavant5",itemsData[position].getDescavant5());



                intent.putExtra("apres1",itemsData[position].getApres1());
                intent.putExtra("descapres1", itemsData[position].getDescapres1());


                intent.putExtra("apres2",itemsData[position].getApres2());
                intent.putExtra("descapres2",itemsData[position].getDescapres2());


                intent.putExtra("apres3",itemsData[position].getApres3());
                intent.putExtra("descapres3",itemsData[position].getDescapres3());


                intent.putExtra("apres4",itemsData[position].getApres4());
                intent.putExtra("descapres4", itemsData[position].getDescapres4());


                intent.putExtra("apres5",itemsData[position].getApres5());
                intent.putExtra("descapres5",itemsData[position].getDescapres5());

                intent.putExtra("isavant1",itemsData[position].isEstavant1());
                intent.putExtra("isavant2",itemsData[position].isEstavant2());
                intent.putExtra("isavant3",itemsData[position].isEstavant3());
                intent.putExtra("isavant4",itemsData[position].isEstavant4());
                intent.putExtra("isavant5",itemsData[position].isEstavant5());

                intent.putExtra("isapres1",itemsData[position].isEstapres1());
                intent.putExtra("isapres2",itemsData[position].isEstapres2());
                intent.putExtra("isapres3",itemsData[position].isEstapres3());
                intent.putExtra("isapres4",itemsData[position].isEstapres4());
                intent.putExtra("isapres5",itemsData[position].isEstapres5());
                context.startActivity(intent);
            }
        });
        viewHolder.date_operation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,detailOperation.class);
                intent.putExtra("modif",true);
                intent.putExtra("nom",itemsData[position].getNom());
                intent.putExtra("prenom",itemsData[position].getPrenom());
                intent.putExtra("mail",itemsData[position].getMail());
                intent.putExtra("tel",itemsData[position].getTel());
                intent.putExtra("age",itemsData[position].getAge());
                intent.putExtra("sexe",itemsData[position].getSexe());
                intent.putExtra("diag",itemsData[position].getDiagnostic());
                intent.putExtra("antecedent",itemsData[position].getAntecedent());
                intent.putExtra("compterendu",itemsData[position].getCompterendu());
                intent.putExtra("dateoperation",itemsData[position].getDateOperation());
                intent.putExtra("operateur",itemsData[position].getOperateur());
                intent.putExtra("equipe",itemsData[position].getEquipe());
                intent.putExtra("adresse",itemsData[position].getAdresse());

                intent.putExtra("visage",itemsData[position].isVisage());
                intent.putExtra("lifting_cervico_facial",itemsData[position].isLifting_cervico_facial());
                intent.putExtra("peeling",itemsData[position].isPeeling());
                intent.putExtra("blepharoplastie",itemsData[position].isBlepharoplastie());
                intent.putExtra("lifting_temporal",itemsData[position].isLifting_temporal());
                intent.putExtra("lifting_complet",itemsData[position].isLifting_complet());
                intent.putExtra("genioplastie",itemsData[position].isGenioplastie());
                intent.putExtra("otoplastie",itemsData[position].isOtoplastie());
                intent.putExtra("autrevisage",itemsData[position].getAutrevisage());

                intent.putExtra("seins",itemsData[position].isSeins());
                intent.putExtra("augmentation_mammaire",itemsData[position].isAugmentation_mammaire());
                intent.putExtra("lipofilling_des_seins",itemsData[position].isLipofilling_des_seins());
                intent.putExtra("reduction_mammaire(",itemsData[position].isReduction_mammaire());
                intent.putExtra("changement_de_protheses",itemsData[position].isChangement_de_protheses());
                intent.putExtra("lifting_des_seins",itemsData[position].isLifting_des_seins());
                intent.putExtra("gynecomastie",itemsData[position].isGynecomastie());
                intent.putExtra("autreseins",itemsData[position].getAutreseins());

                intent.putExtra("corp",itemsData[position].isCorp());
                intent.putExtra("liposuccion",itemsData[position].isLiposuccion());
                intent.putExtra("abdominoplastie",itemsData[position].isAbdominoplastie());
                intent.putExtra("lifting_des_cuisses", itemsData[position].isLifting_des_cuisses());
                intent.putExtra("Augmentation_fesse",itemsData[position].isAugmentation_fesses());
                intent.putExtra("autrecorp",itemsData[position].getAutrecorp());

                intent.putExtra("autre", itemsData[position].getAutre());
                intent.putExtra("admission", itemsData[position].getNum_admission());
                intent.putExtra("id",itemsData[position].getId());

                intent.putExtra("avant1",itemsData[position].getAvant1());
                intent.putExtra("descavant1", itemsData[position].getDescavant1());


                intent.putExtra("avant2",itemsData[position].getAvant2());
                intent.putExtra("descavant2",itemsData[position].getDescavant2());


                intent.putExtra("avant3",itemsData[position].getAvant3());
                intent.putExtra("descavant3", itemsData[position].getDescavant3());


                intent.putExtra("avant4",itemsData[position].getAvant4());
                intent.putExtra("descavant4",itemsData[position].getDescavant4());


                intent.putExtra("avant5",itemsData[position].getAvant5());
                intent.putExtra("descavant5",itemsData[position].getDescavant5());



                intent.putExtra("apres1",itemsData[position].getApres1());
                intent.putExtra("descapres1", itemsData[position].getDescapres1());


                intent.putExtra("apres2",itemsData[position].getApres2());
                intent.putExtra("descapres2",itemsData[position].getDescapres2());


                intent.putExtra("apres3",itemsData[position].getApres3());
                intent.putExtra("descapres3",itemsData[position].getDescapres3());


                intent.putExtra("apres4",itemsData[position].getApres4());
                intent.putExtra("descapres4", itemsData[position].getDescapres4());


                intent.putExtra("apres5",itemsData[position].getApres5());
                intent.putExtra("descapres5",itemsData[position].getDescapres5());

                intent.putExtra("isavant1",itemsData[position].isEstavant1());
                intent.putExtra("isavant2",itemsData[position].isEstavant2());
                intent.putExtra("isavant3",itemsData[position].isEstavant3());
                intent.putExtra("isavant4",itemsData[position].isEstavant4());
                intent.putExtra("isavant5",itemsData[position].isEstavant5());

                intent.putExtra("isapres1",itemsData[position].isEstapres1());
                intent.putExtra("isapres2",itemsData[position].isEstapres2());
                intent.putExtra("isapres3",itemsData[position].isEstapres3());
                intent.putExtra("isapres4",itemsData[position].isEstapres4());
                intent.putExtra("isapres5",itemsData[position].isEstapres5());

                context.startActivity(intent);
            }
        });







    }


    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txtViewTitle;
        public ImageView imgViewIcon;
        public TextView nom_operation;
        public TextView date_operation;
        private Context contxt;



        public ViewHolder(View itemLayoutView ) {
            super(itemLayoutView);
            itemLayoutView.setClickable(true);
            itemLayoutView.setOnClickListener(this);
            txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.item_title);
            imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.item_icon);
            nom_operation= (TextView) itemLayoutView.findViewById(R.id.item_operation);
            date_operation= (TextView) itemLayoutView.findViewById(R.id.item_date_operation);

        }

        @Override
        public void onClick(View v) {


        }
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.length;
    }
}