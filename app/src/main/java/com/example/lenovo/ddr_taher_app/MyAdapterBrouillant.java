package com.example.lenovo.ddr_taher_app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by lenovo on 17/06/2015.
 */
public class MyAdapterBrouillant extends RecyclerView.Adapter<MyAdapterBrouillant.ViewHolder> {
    private ItemData[] itemsData;
    private Context context;
    public MyAdapterBrouillant(ItemData[] itemsData) {
        this.itemsData = itemsData;
    }
    public MyAdapterBrouillant(ItemData[] itemsData ,Context context) {
        this.itemsData = itemsData;
        this.context=context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemlayoutview, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        viewHolder.txtViewTitle.setText(itemsData[position].getTitle());
        viewHolder.imgViewIcon.setImageResource(itemsData[position].getImageUrl());
        viewHolder.nom_operation.setText(itemsData[position].getNomOperation());
        viewHolder.date_operation.setText(itemsData[position].getDateOperation());

        viewHolder.imgViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, AjouterOperation.class);
                intent.putExtra("dr",false);

                intent.putExtra("nom",itemsData[position].getNom());
                intent.putExtra("prenom",itemsData[position].getPrenom());
                intent.putExtra("mail",itemsData[position].getMail());
                intent.putExtra("tel",itemsData[position].getTel());
                intent.putExtra("age",itemsData[position].getAge());
                intent.putExtra("sexe",itemsData[position].getSexe());
                intent.putExtra("diag",itemsData[position].getDiagnostic());
                intent.putExtra("antecedent",itemsData[position].getAntecedent());
                intent.putExtra("compterendu",itemsData[position].getCompterendu());
                intent.putExtra("dateoperation",itemsData[position].getDateOperation());
                intent.putExtra("operateur",itemsData[position].getOperateur());
                intent.putExtra("equipe",itemsData[position].getEquipe());
                intent.putExtra("adresse",itemsData[position].getAdresse());

                intent.putExtra("visage",itemsData[position].isVisage());
                intent.putExtra("lifting_cervico_facial",itemsData[position].isLifting_cervico_facial());
                intent.putExtra("peeling",itemsData[position].isPeeling());
                intent.putExtra("blepharoplastie",itemsData[position].isBlepharoplastie());
                intent.putExtra("lifting_temporal",itemsData[position].isLifting_temporal());
                intent.putExtra("lifting_complet",itemsData[position].isLifting_complet());
                intent.putExtra("genioplastie",itemsData[position].isGenioplastie());
                intent.putExtra("otoplastie",itemsData[position].isOtoplastie());
                intent.putExtra("autrevisage",itemsData[position].getAutrevisage());

                intent.putExtra("seins",itemsData[position].isSeins());
                intent.putExtra("augmentation_mammaire",itemsData[position].isAugmentation_mammaire());
                intent.putExtra("lipofilling_des_seins",itemsData[position].isLipofilling_des_seins());
                intent.putExtra("reduction_mammaire(",itemsData[position].isReduction_mammaire());
                intent.putExtra("changement_de_protheses",itemsData[position].isChangement_de_protheses());
                intent.putExtra("lifting_des_seins",itemsData[position].isLifting_des_seins());
                intent.putExtra("gynecomastie",itemsData[position].isGynecomastie());
                intent.putExtra("autreseins",itemsData[position].getAutreseins());

                intent.putExtra("corp",itemsData[position].isCorp());
                intent.putExtra("liposuccion",itemsData[position].isLiposuccion());
                intent.putExtra("abdominoplastie",itemsData[position].isAbdominoplastie());
                intent.putExtra("lifting_des_cuisses",itemsData[position].isLifting_des_cuisses());
                intent.putExtra("Augmentation_fesse",itemsData[position].isAugmentation_fesses());
                intent.putExtra("autrecorp",itemsData[position].getAutrecorp());

                intent.putExtra("autre",itemsData[position].getAutre());
                intent.putExtra("admission",itemsData[position].getNum_admission());
                intent.putExtra("id",itemsData[position].getId());
                intent.putExtra("new",false);
                context.startActivity(intent);

            }
        });

        viewHolder.txtViewTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, AjouterOperation.class);

                intent.putExtra("dr", false);
                intent.putExtra("nom",itemsData[position].getNom());
                intent.putExtra("prenom",itemsData[position].getPrenom());
                intent.putExtra("mail",itemsData[position].getMail());
                intent.putExtra("tel",itemsData[position].getTel());
                intent.putExtra("age",itemsData[position].getAge());
                intent.putExtra("sexe",itemsData[position].getSexe());
                intent.putExtra("diag",itemsData[position].getDiagnostic());
                intent.putExtra("antecedent",itemsData[position].getAntecedent());
                intent.putExtra("compterendu",itemsData[position].getCompterendu());
                intent.putExtra("dateoperation",itemsData[position].getDateOperation());
                intent.putExtra("operateur",itemsData[position].getOperateur());
                intent.putExtra("equipe",itemsData[position].getEquipe());
                intent.putExtra("adresse",itemsData[position].getAdresse());

                intent.putExtra("visage",itemsData[position].isVisage());
                intent.putExtra("lifting_cervico_facial",itemsData[position].isLifting_cervico_facial());
                intent.putExtra("peeling",itemsData[position].isPeeling());
                intent.putExtra("blepharoplastie",itemsData[position].isBlepharoplastie());
                intent.putExtra("lifting_temporal",itemsData[position].isLifting_temporal());
                intent.putExtra("lifting_complet",itemsData[position].isLifting_complet());
                intent.putExtra("genioplastie",itemsData[position].isGenioplastie());
                intent.putExtra("otoplastie",itemsData[position].isOtoplastie());
                intent.putExtra("autrevisage",itemsData[position].getAutrevisage());

                intent.putExtra("seins",itemsData[position].isSeins());
                intent.putExtra("augmentation_mammaire",itemsData[position].isAugmentation_mammaire());
                intent.putExtra("lipofilling_des_seins",itemsData[position].isLipofilling_des_seins());
                intent.putExtra("reduction_mammaire(",itemsData[position].isReduction_mammaire());
                intent.putExtra("changement_de_protheses",itemsData[position].isChangement_de_protheses());
                intent.putExtra("lifting_des_seins",itemsData[position].isLifting_des_seins());
                intent.putExtra("gynecomastie",itemsData[position].isGynecomastie());
                intent.putExtra("autreseins",itemsData[position].getAutreseins());

                intent.putExtra("corp",itemsData[position].isCorp());
                intent.putExtra("liposuccion",itemsData[position].isLiposuccion());
                intent.putExtra("abdominoplastie",itemsData[position].isAbdominoplastie());
                intent.putExtra("lifting_des_cuisses",itemsData[position].isLifting_des_cuisses());
                intent.putExtra("Augmentation_fesse",itemsData[position].isAugmentation_fesses());
                intent.putExtra("autrecorp",itemsData[position].getAutrecorp());
                intent.putExtra("autre",itemsData[position].getAutre());
                intent.putExtra("admission",itemsData[position].getNum_admission());
                intent.putExtra("id",itemsData[position].getId());
                intent.putExtra("new",false);
                context.startActivity(intent);
            }
        });
        viewHolder.nom_operation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, AjouterOperation.class);
                intent.putExtra("dr", false);
                intent.putExtra("nom",itemsData[position].getNom());
                intent.putExtra("prenom",itemsData[position].getPrenom());
                intent.putExtra("mail",itemsData[position].getMail());
                intent.putExtra("tel",itemsData[position].getTel());
                intent.putExtra("age",itemsData[position].getAge());
                intent.putExtra("sexe",itemsData[position].getSexe());
                intent.putExtra("diag",itemsData[position].getDiagnostic());
                intent.putExtra("antecedent",itemsData[position].getAntecedent());
                intent.putExtra("compterendu",itemsData[position].getCompterendu());
                intent.putExtra("dateoperation",itemsData[position].getDateOperation());
                intent.putExtra("operateur",itemsData[position].getOperateur());
                intent.putExtra("equipe",itemsData[position].getEquipe());
                intent.putExtra("adresse",itemsData[position].getAdresse());

                intent.putExtra("visage",itemsData[position].isVisage());
                intent.putExtra("lifting_cervico_facial",itemsData[position].isLifting_cervico_facial());
                intent.putExtra("peeling",itemsData[position].isPeeling());
                intent.putExtra("blepharoplastie",itemsData[position].isBlepharoplastie());
                intent.putExtra("lifting_temporal",itemsData[position].isLifting_temporal());
                intent.putExtra("lifting_complet",itemsData[position].isLifting_complet());
                intent.putExtra("genioplastie",itemsData[position].isGenioplastie());
                intent.putExtra("otoplastie",itemsData[position].isOtoplastie());
                intent.putExtra("autrevisage",itemsData[position].getAutrevisage());

                intent.putExtra("seins",itemsData[position].isSeins());
                intent.putExtra("augmentation_mammaire",itemsData[position].isAugmentation_mammaire());
                intent.putExtra("lipofilling_des_seins",itemsData[position].isLipofilling_des_seins());
                intent.putExtra("reduction_mammaire(",itemsData[position].isReduction_mammaire());
                intent.putExtra("changement_de_protheses",itemsData[position].isChangement_de_protheses());
                intent.putExtra("lifting_des_seins",itemsData[position].isLifting_des_seins());
                intent.putExtra("gynecomastie",itemsData[position].isGynecomastie());
                intent.putExtra("autreseins",itemsData[position].getAutreseins());

                intent.putExtra("corp",itemsData[position].isCorp());
                intent.putExtra("liposuccion",itemsData[position].isLiposuccion());
                intent.putExtra("abdominoplastie",itemsData[position].isAbdominoplastie());
                intent.putExtra("lifting_des_cuisses",itemsData[position].isLifting_des_cuisses());
                intent.putExtra("Augmentation_fesse",itemsData[position].isAugmentation_fesses());
                intent.putExtra("autrecorp",itemsData[position].getAutrecorp());

                intent.putExtra("autre",itemsData[position].getAutre());
                intent.putExtra("admission",itemsData[position].getNum_admission());
                intent.putExtra("id",itemsData[position].getId());
                intent.putExtra("new",false);
                context.startActivity(intent);
            }
        });
        viewHolder.date_operation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, AjouterOperation.class);
                intent.putExtra("dr", false);
                intent.putExtra("nom",itemsData[position].getNom());
                intent.putExtra("prenom",itemsData[position].getPrenom());
                intent.putExtra("mail",itemsData[position].getMail());
                intent.putExtra("tel",itemsData[position].getTel());
                intent.putExtra("age",itemsData[position].getAge());
                intent.putExtra("sexe",itemsData[position].getSexe());
                intent.putExtra("diag",itemsData[position].getDiagnostic());
                intent.putExtra("antecedent",itemsData[position].getAntecedent());
                intent.putExtra("compterendu",itemsData[position].getCompterendu());
                intent.putExtra("dateoperation",itemsData[position].getDateOperation());
                intent.putExtra("operateur",itemsData[position].getOperateur());
                intent.putExtra("equipe",itemsData[position].getEquipe());
                intent.putExtra("adresse",itemsData[position].getAdresse());

                intent.putExtra("visage",itemsData[position].isVisage());
                intent.putExtra("lifting_cervico_facial",itemsData[position].isLifting_cervico_facial());
                intent.putExtra("peeling",itemsData[position].isPeeling());
                intent.putExtra("blepharoplastie",itemsData[position].isBlepharoplastie());
                intent.putExtra("lifting_temporal",itemsData[position].isLifting_temporal());
                intent.putExtra("lifting_complet",itemsData[position].isLifting_complet());
                intent.putExtra("genioplastie",itemsData[position].isGenioplastie());
                intent.putExtra("otoplastie",itemsData[position].isOtoplastie());
                intent.putExtra("autrevisage",itemsData[position].getAutrevisage());

                intent.putExtra("seins",itemsData[position].isSeins());
                intent.putExtra("augmentation_mammaire",itemsData[position].isAugmentation_mammaire());
                intent.putExtra("lipofilling_des_seins",itemsData[position].isLipofilling_des_seins());
                intent.putExtra("reduction_mammaire(",itemsData[position].isReduction_mammaire());
                intent.putExtra("changement_de_protheses",itemsData[position].isChangement_de_protheses());
                intent.putExtra("lifting_des_seins",itemsData[position].isLifting_des_seins());
                intent.putExtra("gynecomastie",itemsData[position].isGynecomastie());
                intent.putExtra("autreseins",itemsData[position].getAutreseins());

                intent.putExtra("corp",itemsData[position].isCorp());
                intent.putExtra("liposuccion",itemsData[position].isLiposuccion());
                intent.putExtra("abdominoplastie",itemsData[position].isAbdominoplastie());
                intent.putExtra("lifting_des_cuisses",itemsData[position].isLifting_des_cuisses());
                intent.putExtra("Augmentation_fesse",itemsData[position].isAugmentation_fesses());
                intent.putExtra("autrecorp",itemsData[position].getAutrecorp());

                intent.putExtra("autre",itemsData[position].getAutre());
                intent.putExtra("admission",itemsData[position].getNum_admission());
                intent.putExtra("id",itemsData[position].getId());
                intent.putExtra("new",false);

                context.startActivity(intent);
            }
        });






    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtViewTitle;
        public ImageView imgViewIcon;
        public TextView nom_operation;
        public TextView date_operation;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.item_title);
            imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.item_icon);
            nom_operation= (TextView) itemLayoutView.findViewById(R.id.item_operation);
            date_operation= (TextView) itemLayoutView.findViewById(R.id.item_date_operation);
        }


    }

    @Override
    public int getItemCount() {
        return itemsData.length;
    }
}
