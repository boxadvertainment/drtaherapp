package com.example.lenovo.ddr_taher_app;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import android.graphics.Bitmap;
import android.support.v7.widget.Toolbar;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Base64;
import android.widget.Toast;

public class Personne {



    private String email;
    private String password;
    private String remember;
    //private String token;
    private String nom ;
    private String prenom;

    public Personne( String nom , String prenom ) {
        this.nom=nom;
        this.prenom=prenom;

    }



    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    private static String convertStreamToString(InputStream is) {
        String k = null;
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(is,"UTF-8"));
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            k= sb.toString();
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return k;



    }
   public static String gettoken(){
       String a ="";

       HttpClient httpclient = new DefaultHttpClient();
       HttpGet httppost;
       ArrayList<NameValuePair> nameValuePairs;
       HttpResponse response;
       HttpEntity entity;
       boolean sortie = false;
       httppost = new HttpGet("http://apps.smart-robox.com/drtaher/api/getToken");



       try {


           response = httpclient.execute(httppost);


           if (response.getStatusLine().getStatusCode() == 200) {


               entity = response.getEntity();

               if (entity != null) {
                   InputStream in = entity.getContent();
                   a = Personne.convertStreamToString(in);


               }


           }
       }catch(HttpHostConnectException e ){

           return "";

       } catch (ClientProtocolException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
           return "";
       } catch (IOException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
           return "";
       }






return a ;


   }
    public static String cnx( String mail , String pass ){

        HttpClient httpclient = new DefaultHttpClient();
        ArrayList<NameValuePair> params;
        HttpResponse response;
        HttpEntity entity;
        JSONObject f =null;

       String a="" ;



        try {
            params = new ArrayList<NameValuePair>();

            //params.add(new BasicNameValuePair("_token", token));
            params.add(new BasicNameValuePair("email", mail));
            params.add(new BasicNameValuePair("password", pass));
            params.add(new BasicNameValuePair("remember", "on"));


            StringBuilder requestUrl = new StringBuilder("http://apps.smart-robox.com/drtaher/api/login");

            String querystring = URLEncodedUtils.format(params, "utf-8");
            requestUrl.append("?");
            requestUrl.append(querystring);
            HttpGet get = new HttpGet(requestUrl.toString());
            response = httpclient.execute(get);

            if (response.getStatusLine().getStatusCode() == 200) {
                System.out.println("houni");

                entity = response.getEntity();

                if (entity != null) {
                    InputStream in = entity.getContent();
                    a = Personne.convertStreamToString(in);





                }


            }

        }catch(HttpHostConnectException e){
                a="abc";

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }




        return a;



    }
    public static String add(String mailthou ,String token, String num_admission,  String nom, String prenom, String mail, String tel, String age, String sexe,int imageUrl,String nom_operation, String date_operation , String diagnostic, String antecedent, String compterendu, String dr,String autredr, String equipe,String autreequipe, String adresse, String[] visage,  String autrevisage, String[] seins,  String autreseins, String[] corp, String autrecorp,String [] intervention, String autre) {


        HttpClient httpclient = new DefaultHttpClient();
        ArrayList<NameValuePair> params;
        HttpResponse response;
        HttpEntity entity;
        JSONObject f =null;

        String a="" ;



        try {
            params = new ArrayList<NameValuePair>();

            //params.add(new BasicNameValuePair("_token", token));
            params.add(new BasicNameValuePair("admission_num", num_admission));
            params.add(new BasicNameValuePair("firstname", nom));
            params.add(new BasicNameValuePair("lastname", prenom));
            params.add(new BasicNameValuePair("sexe", sexe));
            params.add(new BasicNameValuePair("age", age));
            params.add(new BasicNameValuePair("address", adresse));
            params.add(new BasicNameValuePair("email", mail));
            params.add(new BasicNameValuePair("tel", tel));
            params.add(new BasicNameValuePair("diag", diagnostic));
            for(int i=0 ;i<visage.length;i++) {
                params.add(new BasicNameValuePair("visages"+"["+i+"]", visage[i]));
            }
            params.add(new BasicNameValuePair("autre_visages", autrevisage));
            for(int i=0 ;i<seins.length;i++) {
                params.add(new BasicNameValuePair("seins"+"["+i+"]", seins[i]));
            }
            params.add(new BasicNameValuePair("autre_seins", autreseins));
            for(int i=0 ;i<corp.length;i++) {
                params.add(new BasicNameValuePair("corps"+"["+i+"]", corp[i]));
            }
            params.add(new BasicNameValuePair("autre_corps", autrecorp));
            for(int i=0 ;i<intervention.length;i++) {
                params.add(new BasicNameValuePair("intervention"+"["+i+"]", intervention[i]));

            }

            params.add(new BasicNameValuePair("autre_intervention", autre));
            params.add(new BasicNameValuePair("date", date_operation));
            params.add(new BasicNameValuePair("dr", dr));
            params.add(new BasicNameValuePair("autre_dr", autredr));
            params.add(new BasicNameValuePair("equipe", equipe));
            params.add(new BasicNameValuePair("autre_equipe", autreequipe));
            params.add(new BasicNameValuePair("antecedents", antecedent));
            params.add(new BasicNameValuePair("resum_clinic", compterendu));
            params.add(new BasicNameValuePair("authEmail", mailthou));





            StringBuilder requestUrl = new StringBuilder("http://apps.smart-robox.com/drtaher/api/add");

            String querystring = URLEncodedUtils.format(params, "utf-8");
            requestUrl.append("?");
            requestUrl.append(querystring);
            HttpGet get = new HttpGet(requestUrl.toString());
            response = httpclient.execute(get);
            System.out.println(response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() == 200) {
                System.out.println("houni");

                entity = response.getEntity();

                if (entity != null) {
                    InputStream in = entity.getContent();
                    a = Personne.convertStreamToString(in);





                }


            }

        }catch(HttpHostConnectException e){
            a="abc";

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }




        return a;



    }

    public static String addbrouillant(String mailthou ,String token, String num_admission,  String nom, String prenom, String mail, String tel, String age, String sexe,int imageUrl,String nom_operation, String date_operation , String diagnostic, String antecedent, String compterendu, String dr,String autredr, String equipe,String autreequipe, String adresse, String[] visage,  String autrevisage, String[] seins,  String autreseins, String[] corp, String autrecorp,String [] intervention, String autre) {


        HttpClient httpclient = new DefaultHttpClient();
        ArrayList<NameValuePair> params;
        HttpResponse response;
        HttpEntity entity;
        JSONObject f =null;

        String a="" ;



        try {
            params = new ArrayList<NameValuePair>();

            //params.add(new BasicNameValuePair("_token", token));
            params.add(new BasicNameValuePair("admission_num", num_admission));
            params.add(new BasicNameValuePair("firstname", nom));
            params.add(new BasicNameValuePair("lastname", prenom));
            params.add(new BasicNameValuePair("sexe", sexe));
            params.add(new BasicNameValuePair("age", age));
            params.add(new BasicNameValuePair("address", adresse));
            params.add(new BasicNameValuePair("email", mail));
            params.add(new BasicNameValuePair("tel", tel));
            params.add(new BasicNameValuePair("diag", diagnostic));
            if(visage.length!=0){
                for(int i=0 ;i<visage.length;i++) {
                    params.add(new BasicNameValuePair("visages"+"["+i+"]", visage[i]));
                }

            }else{
                params.add(new BasicNameValuePair("visages", null));
            }

            params.add(new BasicNameValuePair("autre_visages", autrevisage));

            if(seins.length!=0) {
                for (int i = 0; i < seins.length; i++) {
                    params.add(new BasicNameValuePair("seins" + "[" + i + "]", seins[i]));
                }
            }else{
                params.add(new BasicNameValuePair("seins", null));
            }
            params.add(new BasicNameValuePair("autre_seins", autreseins));
            if(corp.length!=0) {

                for (int i = 0; i < corp.length; i++) {
                    params.add(new BasicNameValuePair("corps" + "[" + i + "]", corp[i]));
                }
            }else{
                params.add(new BasicNameValuePair("corps", null));
            }
            params.add(new BasicNameValuePair("autre_corps", autrecorp));
            if(intervention.length!=0) {
                for (int i = 0; i < intervention.length; i++) {
                    params.add(new BasicNameValuePair("intervention" + "[" + i + "]", intervention[i]));

                }
            }else{
                params.add(new BasicNameValuePair("intervention", null));
            }

            params.add(new BasicNameValuePair("autre_intervention", autre));
            params.add(new BasicNameValuePair("date", date_operation));
            params.add(new BasicNameValuePair("dr", dr));
            params.add(new BasicNameValuePair("autre_dr", autredr));
            params.add(new BasicNameValuePair("equipe", equipe));
            params.add(new BasicNameValuePair("autre_equipe", autreequipe));
            params.add(new BasicNameValuePair("antecedents", antecedent));
            params.add(new BasicNameValuePair("resum_clinic", compterendu));
            params.add(new BasicNameValuePair("authEmail", mailthou));





            StringBuilder requestUrl = new StringBuilder("http://apps.smart-robox.com/drtaher/api/add/draft");

            String querystring = URLEncodedUtils.format(params, "utf-8");
            requestUrl.append("?");
            requestUrl.append(querystring);
            HttpGet get = new HttpGet(requestUrl.toString());
            response = httpclient.execute(get);
            System.out.println(response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() == 200) {
                System.out.println("houni");

                entity = response.getEntity();

                if (entity != null) {
                    InputStream in = entity.getContent();
                    a = Personne.convertStreamToString(in);





                }


            }

        }catch(HttpHostConnectException e){
            a="abc";

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }




        return a;



    }


    public static String getlisteoperation( String mail ){

        HttpClient httpclient = new DefaultHttpClient();
        ArrayList<NameValuePair> params;
        HttpResponse response;
        HttpEntity entity;
        JSONObject f =null;

        String a="" ;



        try {
            params = new ArrayList<NameValuePair>();

            //params.add(new BasicNameValuePair("_token", token));
            params.add(new BasicNameValuePair("authEmail", mail));



            StringBuilder requestUrl = new StringBuilder("http://apps.smart-robox.com/drtaher/api/operationsList");

            String querystring = URLEncodedUtils.format(params, "utf-8");
            requestUrl.append("?");
            requestUrl.append(querystring);
            HttpGet get = new HttpGet(requestUrl.toString());
            response = httpclient.execute(get);

            if (response.getStatusLine().getStatusCode() == 200) {
                System.out.println("houni");

                entity = response.getEntity();

                if (entity != null) {
                    InputStream in = entity.getContent();
                    a = Personne.convertStreamToString(in);





                }


            }

        }catch(HttpHostConnectException e){
            a="abc";

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }




        return a;



    }
    public static String getlisteoperationbrouillant( String mail ){

        HttpClient httpclient = new DefaultHttpClient();
        ArrayList<NameValuePair> params;
        HttpResponse response;
        HttpEntity entity;
        JSONObject f =null;

        String a="" ;



        try {
            params = new ArrayList<NameValuePair>();

            //params.add(new BasicNameValuePair("_token", token));
            params.add(new BasicNameValuePair("authEmail", mail));



            StringBuilder requestUrl = new StringBuilder("http://apps.smart-robox.com/drtaher/api/operationsList/draft");

            String querystring = URLEncodedUtils.format(params, "utf-8");
            requestUrl.append("?");
            requestUrl.append(querystring);
            HttpGet get = new HttpGet(requestUrl.toString());
            response = httpclient.execute(get);

            if (response.getStatusLine().getStatusCode() == 200) {
                System.out.println("houni");

                entity = response.getEntity();

                if (entity != null) {
                    InputStream in = entity.getContent();
                    a = Personne.convertStreamToString(in);





                }


            }

        }catch(HttpHostConnectException e){
            a="abc";

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }




        return a;



    }
    public static String modifier(String id,String mailthou ,String token, String num_admission,  String nom, String prenom, String mail, String tel, String age, String sexe,int imageUrl,String nom_operation, String date_operation , String diagnostic, String antecedent, String compterendu, String dr,String autredr, String equipe,String autreequipe, String adresse, String[] visage,  String autrevisage, String[] seins,  String autreseins, String[] corp, String autrecorp,String [] intervention, String autre) {


        HttpClient httpclient = new DefaultHttpClient();
        ArrayList<NameValuePair> params;
        HttpResponse response;
        HttpEntity entity;
        JSONObject f =null;

        String a="" ;



        try {
            params = new ArrayList<NameValuePair>();

            //params.add(new BasicNameValuePair("_token", token));
            params.add(new BasicNameValuePair("admission_num", num_admission));
            params.add(new BasicNameValuePair("firstname", nom));
            params.add(new BasicNameValuePair("lastname", prenom));
            params.add(new BasicNameValuePair("sexe", sexe));
            params.add(new BasicNameValuePair("age", age));
            params.add(new BasicNameValuePair("address", adresse));
            params.add(new BasicNameValuePair("email", mail));
            params.add(new BasicNameValuePair("tel", tel));
            params.add(new BasicNameValuePair("diag", diagnostic));
            for(int i=0 ;i<visage.length;i++) {
                params.add(new BasicNameValuePair("visages"+"["+i+"]", visage[i]));
            }
            params.add(new BasicNameValuePair("autre_visages", autrevisage));
            for(int i=0 ;i<seins.length;i++) {
                params.add(new BasicNameValuePair("seins"+"["+i+"]", seins[i]));
            }
            params.add(new BasicNameValuePair("autre_seins", autreseins));
            for(int i=0 ;i<corp.length;i++) {
                params.add(new BasicNameValuePair("corps"+"["+i+"]", corp[i]));
            }
            params.add(new BasicNameValuePair("autre_corps", autrecorp));
            for(int i=0 ;i<intervention.length;i++) {
                params.add(new BasicNameValuePair("intervention"+"["+i+"]", intervention[i]));

            }

            params.add(new BasicNameValuePair("autre_intervention", autre));
            params.add(new BasicNameValuePair("date", date_operation));
            params.add(new BasicNameValuePair("dr", dr));
            params.add(new BasicNameValuePair("autre_dr", autredr));
            params.add(new BasicNameValuePair("equipe", equipe));
            params.add(new BasicNameValuePair("autre_equipe", autreequipe));
            params.add(new BasicNameValuePair("antecedents", antecedent));
            params.add(new BasicNameValuePair("resum_clinic", compterendu));
            params.add(new BasicNameValuePair("authEmail", mailthou));





            StringBuilder requestUrl = new StringBuilder("http://apps.smart-robox.com/drtaher/api/edit/"+id);

            String querystring = URLEncodedUtils.format(params, "utf-8");
            requestUrl.append("?");
            requestUrl.append(querystring);
            HttpGet get = new HttpGet(requestUrl.toString());
            response = httpclient.execute(get);
            System.out.println(response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() == 200) {
                System.out.println("houni");

                entity = response.getEntity();

                if (entity != null) {
                    InputStream in = entity.getContent();
                    a = Personne.convertStreamToString(in);





                }


            }

        }catch(HttpHostConnectException e){
            a="abc";

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }




        return a;



    }

    public static String modifvalide(String id ) {


        HttpClient httpclient = new DefaultHttpClient();
        ArrayList<NameValuePair> params;
        HttpResponse response;
        HttpEntity entity;
        JSONObject f =null;

        String a="" ;



        try {
            params = new ArrayList<NameValuePair>();

            //params.add(new BasicNameValuePair("_token", token));


           // params.add(new BasicNameValuePair("authEmail", mailthou));





            StringBuilder requestUrl = new StringBuilder("http://apps.smart-robox.com/drtaher/api/setDraft/"+id+"/false");

            String querystring = URLEncodedUtils.format(params, "utf-8");
            requestUrl.append("?");
            requestUrl.append(querystring);
            HttpGet get = new HttpGet(requestUrl.toString());
            response = httpclient.execute(get);
            System.out.println(response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() == 200) {
                System.out.println("houni");

                entity = response.getEntity();

                if (entity != null) {
                    InputStream in = entity.getContent();
                    a = Personne.convertStreamToString(in);





                }


            }

        }catch(HttpHostConnectException e){
            a="abc";

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }




        return a;



    }
    public static String getImage(String id ) {


        HttpClient httpclient = new DefaultHttpClient();
        ArrayList<NameValuePair> params;
        HttpResponse response;
        HttpEntity entity;
        JSONObject f =null;

        String a="" ;



        try {
            params = new ArrayList<NameValuePair>();

            //params.add(new BasicNameValuePair("_token", token));


            // params.add(new BasicNameValuePair("authEmail", mailthou));





            StringBuilder requestUrl = new StringBuilder("http://apps.smart-robox.com/drtaher/api/getPhotos/"+id);

            String querystring = URLEncodedUtils.format(params, "utf-8");
            requestUrl.append("?");
            requestUrl.append(querystring);
            HttpGet get = new HttpGet(requestUrl.toString());
            response = httpclient.execute(get);
            System.out.println(response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() == 200) {
                System.out.println("houni");

                entity = response.getEntity();

                if (entity != null) {
                    InputStream in = entity.getContent();
                    a = Personne.convertStreamToString(in);





                }


            }

        }catch(HttpHostConnectException e){
            a="abc";

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }




        return a;



    }


    public String ajouter( String id , String type,String desc ,Bitmap image1 ){
        HttpClient httpclient;
        HttpPost httppost;
        ArrayList<NameValuePair> nameValuePairs;
        HttpResponse response;
        HttpEntity entity;
       String a="" ;
        String token = Personne.gettoken();
        httpclient=new DefaultHttpClient();
        httppost=new HttpPost("http://apps.smart-robox.com/drtaher/api/addPhoto/"+id);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image1.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] image = stream.toByteArray();

        String ba1 = Base64.encodeToString(image, Base64.DEFAULT);


        try {
            nameValuePairs=new ArrayList<NameValuePair>();
           // nameValuePairs.add(new BasicNameValuePair("email", "admin@box.agency"));
           // nameValuePairs.add(new BasicNameValuePair("_token", token));
            nameValuePairs.add(new BasicNameValuePair("type",type));
            nameValuePairs.add(new BasicNameValuePair("description",desc));
            nameValuePairs.add(new BasicNameValuePair("photo", ba1));


            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            response=httpclient.execute(httppost);
            System.out.print("hamahamahama" + response.getStatusLine().getStatusCode());
            if(response.getStatusLine().getStatusCode()==200){

                entity=response.getEntity();

                if(entity != null){
                    InputStream in =entity.getContent();
                    a= Personne.convertStreamToString(in);
                    System.out.print(a);








                }

            }
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }






        return a;

    }







    public static void main(String[] args) {
        Personne p = new Personne ("hama","grine");
       p.gettoken();
       // boolean f =Personne.cnx("admin@boxagency","Q26zq6B8","");
     //   System.out.print(f);


    }
    public static String delettephoto(String id){
        String a ="";

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httppost;
        ArrayList<NameValuePair> nameValuePairs;
        HttpResponse response;
        HttpEntity entity;
        boolean sortie = false;
        httppost = new HttpGet("http://apps.smart-robox.com/drtaher/api/deletePhoto/"+id);



        try {


            response = httpclient.execute(httppost);


            if (response.getStatusLine().getStatusCode() == 200) {


                entity = response.getEntity();

                if (entity != null) {
                    InputStream in = entity.getContent();
                    a = Personne.convertStreamToString(in);


                }


            }
        }catch(HttpHostConnectException e ){

            return "";

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }






        return a ;


    }

}
