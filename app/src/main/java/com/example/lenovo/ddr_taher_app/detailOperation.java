package com.example.lenovo.ddr_taher_app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class detailOperation extends ActionBarActivity implements   ViewSwitcher.ViewFactory {

    private static final int cam_request1=1313;
    private static final int cam_request2=1314;
    private static final int cam_request3=1315;
    private static final int cam_request4=1316;
    private static final int cam_request5=1317;

    private static final int gallery_request1=1318;
    private static final int gallery_request2=1319;
    private static final int gallery_request3=1320;
    private static final int gallery_request4=1321;
    private static final int gallery_request5=1322;


    private static final int cam_request11=1323;
    private static final int cam_request21=1324;
    private static final int cam_request31=1325;
    private static final int cam_request41=1326;
    private static final int cam_request51=1327;

    private static final int gallery_request11=1328;
    private static final int gallery_request21=1329;
    private static final int gallery_request31=1330;
    private static final int gallery_request41=1331;
    private static final int gallery_request51=1332;

    final private static int DIALOG_DATE_NAISSANCE = 1;
    final private static int DIALOG_DATE_OPERATION = 2;
    Toolbar toolbar;
    TabHost tabHost;
    EditText editnom , editprenom , editmail , edittel, visageautre,seinsautre,corpautre,autreoperateur,autreequipe,autreoperation,antecedent,compterendu ,editadresse ;
    TextView errornom , errorprenom , errormail , errortel , errorage , dateoperation , errorcompterendu,errorantecedent , erordateoperation , eroroperateur , erorequipe ,eroradresse;
    RadioButton femme , homme ;

    String sexe , dateNaissance , dateOperation,modifnom,modifprenom , modifage ,modifmail , modiftel;
    String anciennom , ancienprenom , ancienmail , ancientel , ancienage , anciensexe="homme" , anciendiag , anciencompterendu, ancienantecedent , anciendateoperation,ancienoperateur,ancienequipe,ancienadresse ;
    boolean modif;

    CheckBox visage, Lifting_cervico_facial ,Peeling,Blepharoplastie,Lifting_temporal,Lifting_complet ,Genioplastie,Otoplastie,autrevisage,
            seins,Augmentation_mammaire , Lipofilling_des_seins,Reduction_mammaire,Changement_de_protheses,Lifting_des_seins,Gynecomastie,autreseins,
            corp ,Liposuccion,Abdominoplastie,Lifting_des_cuisses,Augmentation_fesses , autrecorp,
            autre ;

    RadioButton radioSexButton , radiooperateurtaher,radiooperateueautre,radioequipetaher,radioequipeautre;

    Button editerdateoperation;

   Boolean ancienvisage, ancienLifting_cervico_facial ,ancienPeeling,ancienBlepharoplastie,ancienLifting_temporal,ancienLifting_complet ,ancienGenioplastie,ancienOtoplastie,
           ancienseins,ancienAugmentation_mammaire , ancienLipofilling_des_seins,ancienReduction_mammaire,ancienChangement_de_protheses,ancienLifting_des_seins,ancienGynecomastie,
           anciencorp ,ancienLiposuccion,ancienAbdominoplastie,ancienLifting_des_cuisses,ancienAugmentation_fesses
     ;
   String ancienautrevisage,ancienautreseins,ancienautrecorp,ancienautre , ancienId;

    EditText num_adminission;
    TextView errorNum_admission;
    String ancienNum_admission;




    EditText editdiagnostic;
    TextView errordiagnostic;

    TextView errordetailsoperation ,errordetailsoperationvisage,errordetailsoperationcorp,errordetailsoperationseins,errordetailsoperationautre ;


    EditText age;

    ImageView avant1,avant2,avant3,avant4,avant5;
    TextView errortofavant , Note1 ,  Note2, Note4, Note3, Note5;
    EditText editnote1,editnote2,editnote3,editnote4,editnote5;
    Button ajoutertofavant;
    int nombrefotoavant ;



    ImageView avant11,avant21,avant31,avant41,avant51;
    TextView errortofavant1 , Note11 ,  Note21, Note41, Note31, Note51;
    EditText editnote11,editnote21,editnote31,editnote41,editnote51;
    Button ajoutertofavant1;
    int nombrefotoavant1 ;

    Bitmap a1 , a2, a3,a4,a5,ap1,ap2,ap3,ap4,ap5;
    String anciendescavant1,anciendescavant2,anciendescavant3,anciendescavant4,anciendescavant5,anciendescapres1,anciendescapres2,anciendescapres3,anciendescapres4,anciendescapres5;
    boolean isavant1,isavant2,isavant3,isavant4,isavant5,isapres1,isapres2,isapres3,isapres4,isapres5;
    String ancienavant1,ancienavant2,ancienavant3,ancienavant4,ancienavant5,ancienapres1,ancienapres2,ancienapres3,ancienapres4,ancienapres5;

    LinearLayout mayout_liposuccion , mayout_plastie_abdominal , layout_injection_graisse,layout_prothese_mammaire , layout_plastie_mamaire , layout_lifting_bras, layout_lifting_cuisse1,layout_lifting_cuisse2, layout_Blepharoplastie , layout_lifting_cervico_facial; ;
    EditText edit1 , edit2,edit3,edit4,edit5,edit7,edit8,edit9,edit10,edit11,edit12,edit13,edit14,edit15,edit16,edit17;
    CheckBox edit18,edit19,edit20,edit21,edit22,edit23,edit24,edit25,edit26,edit27,edit28,edit29,edit30,edit31,edit32,edit33,edit34,edit35;
    RadioButton hama1,hama2,hama3,hama4,hama5, hama6, hama7;
    TextView cash1, cash2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_operation);
        Bundle extras = getIntent().getExtras();
        modif=true;
        if (extras != null) {
            modif = extras.getBoolean("modif");
            anciennom=extras.getString("nom");
            ancienprenom=extras.getString("prenom");
            ancienmail=extras.getString("mail");
            ancientel=extras.getString("tel");
            ancienage=extras.getString("age");
            anciensexe=extras.getString("sexe");
            anciendiag=extras.getString("diag");
            ancienantecedent=extras.getString("antecedent");
            anciencompterendu=extras.getString("compterendu");
            anciendateoperation=extras.getString("dateoperation");
            ancienoperateur=extras.getString("operateur");
            ancienequipe=extras.getString("equipe");
            ancienadresse=extras.getString("adresse");

            ancienvisage =extras.getBoolean("visage");
            ancienLifting_cervico_facial=extras.getBoolean("lifting_cervico_facial");
            ancienPeeling=extras.getBoolean("peeling");
            ancienBlepharoplastie=extras.getBoolean("blepharoplastie");
            ancienLifting_temporal=extras.getBoolean("lifting_temporal");
            ancienLifting_complet=extras.getBoolean("lifting_complet");
            ancienGenioplastie=extras.getBoolean("genioplastie");
            ancienOtoplastie=extras.getBoolean("otoplastie");
            ancienautrevisage=extras.getString("autrevisage");
            ancienseins=extras.getBoolean("seins");
            ancienAugmentation_mammaire=extras.getBoolean("augmentation_mammaire");
            ancienLipofilling_des_seins=extras.getBoolean("lipofilling_des_seins");
            ancienReduction_mammaire=extras.getBoolean("reduction_mammaire(");
            ancienChangement_de_protheses=extras.getBoolean("changement_de_protheses");
            ancienLifting_des_seins=extras.getBoolean("lifting_des_seins");
            ancienGynecomastie=extras.getBoolean("gynecomastie");
            ancienautreseins=extras.getString("autreseins");
            anciencorp=extras.getBoolean("corp");
            ancienLiposuccion=extras.getBoolean("liposuccion");
            ancienAbdominoplastie=extras.getBoolean("abdominoplastie");
            ancienLifting_des_cuisses=extras.getBoolean("lifting_des_cuisses");
            ancienAugmentation_fesses=extras.getBoolean("Augmentation_fesse");
            ancienautrecorp=extras.getString("autrecorp");
            ancienNum_admission=extras.getString("admission");

            ancienautre=extras.getString("autre");
            ancienId=extras.getString("id");

            ancienavant1=extras.getString("avant1");
            ancienavant2=extras.getString("avant2");
            ancienavant3=extras.getString("avant3");
            ancienavant4=extras.getString("avant4");
            ancienavant5=extras.getString("avant5");

            ancienapres1=extras.getString("apres1");
            ancienapres2=extras.getString("apres2");
            ancienapres3=extras.getString("apres3");
            ancienapres4=extras.getString("apres4");
            ancienapres5=extras.getString("apres5");


            anciendescavant1=extras.getString("descavant1");
             anciendescavant2=extras.getString("descavant2");
             anciendescavant3=extras.getString("descavant3");
             anciendescavant4=extras.getString("descavant4");
             anciendescavant5=extras.getString("descavant5");

             anciendescapres1=extras.getString("descapres1");
             anciendescapres2=extras.getString("descapres2");
             anciendescapres3=extras.getString("descapres3");
             anciendescapres4=extras.getString("descapres4");
             anciendescapres5=extras.getString("descapres5");

            isavant1=extras.getBoolean("isavant1");
            isavant2=extras.getBoolean("isavant2");
            isavant3=extras.getBoolean("isavant3");
            isavant4=extras.getBoolean("isavant4");
            isavant5=extras.getBoolean("isavant5");

            isapres1=extras.getBoolean("isapres1");
            isapres2=extras.getBoolean("isapres2");
            isapres3=extras.getBoolean("isapres3");
            isapres4=extras.getBoolean("isapres4");
            isapres5=extras.getBoolean("isapres5");



        }
        cash1= (TextView) findViewById(R.id.cash1);
        cash2= (TextView) findViewById(R.id.cash2);
        edit4= (EditText) findViewById(R.id.Edit4);
        hama6= (RadioButton) findViewById(R.id.hama6);
        hama7= (RadioButton) findViewById(R.id.hama7);
        if(hama7.isChecked()){
            cash1.setVisibility(View.VISIBLE);
            cash2.setVisibility(View.VISIBLE);
            edit4.setVisibility(View.VISIBLE);

        }else{
            cash1.setVisibility(View.GONE);
            cash2.setVisibility(View.GONE);
            edit4.setVisibility(View.GONE);
        }
        hama7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(hama7.isChecked()){
                    cash1.setVisibility(View.VISIBLE);
                    cash2.setVisibility(View.VISIBLE);
                    edit4.setVisibility(View.VISIBLE);
                }
            }
        });
        hama6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cash1.setVisibility(View.GONE);
                cash2.setVisibility(View.GONE);
                edit4.setVisibility(View.GONE);
            }
        });



        mayout_liposuccion= (LinearLayout) findViewById(R.id.tab5_Liposuccion);
        mayout_plastie_abdominal=(LinearLayout) findViewById(R.id.tab5_Abdominale);
        layout_injection_graisse=(LinearLayout) findViewById(R.id.tab5_ijectionGraisse);
        layout_prothese_mammaire=(LinearLayout) findViewById(R.id.tab5_Prothese_mammaire);
        layout_plastie_mamaire=(LinearLayout) findViewById(R.id.tab5_Plastie_mammaire);
        layout_lifting_bras=(LinearLayout) findViewById(R.id.tab5_Lifting_des_bras);
        layout_lifting_cuisse1=(LinearLayout) findViewById(R.id.tab5_Lifting_des_cuisses_1);
        layout_lifting_cuisse2=(LinearLayout) findViewById(R.id.tab5_Lifting_des_cuisses_2);
        layout_Blepharoplastie=(LinearLayout) findViewById(R.id.tab5_Blepharoplastie);
        layout_lifting_cervico_facial=(LinearLayout) findViewById(R.id.tab5_Liftingcervicofacial);




        ajoutertofavant= (Button) findViewById(R.id.ajoutertofavant);

        Note1= (TextView) findViewById(R.id.staticavant1);
        Note2= (TextView) findViewById(R.id.staticavant2);
        Note3= (TextView) findViewById(R.id.staticavant3);
        Note4= (TextView) findViewById(R.id.staticavant4);
        Note5= (TextView) findViewById(R.id.staticavant5);

        editnote1= (EditText) findViewById(R.id.editavant1);
        editnote2= (EditText) findViewById(R.id.editavant2);
        editnote3= (EditText) findViewById(R.id.editavant3);
        editnote4= (EditText) findViewById(R.id.editavant4);
        editnote5= (EditText) findViewById(R.id.editavant5);

        avant1= (ImageView) findViewById(R.id.avant1);
        avant2= (ImageView) findViewById(R.id.avant2);
        avant3= (ImageView) findViewById(R.id.avant3);
        avant4= (ImageView) findViewById(R.id.avant4);
        avant5= (ImageView) findViewById(R.id.avant5);

      /*  avant1.setImageBitmap(a1);
        editnote1.setText(anciendescavant1);
        avant2.setImageBitmap(a2);
        editnote2.setText(anciendescavant2);
        avant3.setImageBitmap(a3);
        editnote3.setText(anciendescavant3);
        avant4.setImageBitmap(a4);
        editnote4.setText(anciendescavant4);
        avant5.setImageBitmap(a5);
        editnote5.setText(anciendescavant5);*/





        avant1.setVisibility(View.GONE);
        editnote1.setVisibility(View.GONE);
        Note1.setVisibility(View.GONE);

        avant2.setVisibility(View.GONE);
        editnote2.setVisibility(View.GONE);
        Note2.setVisibility(View.GONE);

        avant3.setVisibility(View.GONE);
        editnote3.setVisibility(View.GONE);
        Note3.setVisibility(View.GONE);

        avant4.setVisibility(View.GONE);
        editnote4.setVisibility(View.GONE);
        Note4.setVisibility(View.GONE);

        avant5.setVisibility(View.GONE);
        editnote5.setVisibility(View.GONE);
        Note5.setVisibility(View.GONE);

        if(!ancienavant1.equals("")) {
            a1 = downloadImage(ancienavant1);
        }
        if(!ancienavant2.equals("")) {
            a2 = downloadImage(ancienavant2);
        }
        if(!ancienavant3.equals("")) {
            a3 = downloadImage(ancienavant3);
        }
        if(!ancienavant4.equals("")) {
            a4 = downloadImage(ancienavant4);
        }
        if(!ancienavant5.equals("")) {
            a5 = downloadImage(ancienavant5);
        }




   boolean[]x={isavant1,isavant2,isavant3,isavant4,isavant5};
        ImageView[]a={avant1,avant2,avant3,avant4,avant5};
        EditText[]e={editnote1,editnote2,editnote3,editnote4,editnote5};
        TextView[]n={ Note1 ,  Note2, Note4, Note3, Note5};
        Bitmap[]b={a1 , a2, a3,a4,a5};
        String[]d={anciendescavant1,anciendescavant2,anciendescavant3,anciendescavant4,anciendescavant5};
        int i =0;
        int j =0;
        nombrefotoavant=0;
        while(i<x.length){
            if(x[i]){

                a[j].setVisibility(View.VISIBLE);
                a[j].setImageBitmap(b[i]);
                e[j].setVisibility(View.VISIBLE);
                e[j].setText(d[i]);
                n[j].setVisibility(View.VISIBLE);
                i++;
                j++;
                nombrefotoavant++;

            }else{
                i++;
            }
        }
        j++;
        while(j<5){
            a[j].setVisibility(View.GONE);

            e[j].setVisibility(View.GONE);
            e[j].setText("");
            n[j].setVisibility(View.GONE);
            j++;
        }



        ajoutertofavant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean[]x={isavant1,isavant2,isavant3,isavant4,isavant5};
                ImageView[]a={avant1,avant2,avant3,avant4,avant5};
                EditText[]e={editnote1,editnote2,editnote3,editnote4,editnote5};
                TextView[]n={ Note1 ,  Note2, Note4, Note3, Note5};
                Bitmap[]b={a1 , a2, a3,a4,a5};
                String[]d={anciendescavant1,anciendescavant2,anciendescavant3,anciendescavant4,anciendescavant5};
                int i =0;
                int j =0;
                nombrefotoavant=0;
                while(i<x.length){
                    if(x[i]){

                        a[j].setVisibility(View.VISIBLE);
                        a[j].setImageBitmap(b[i]);
                        e[j].setVisibility(View.VISIBLE);
                        e[j].setText(d[i]);
                        n[j].setVisibility(View.VISIBLE);
                        i++;
                        j++;
                        nombrefotoavant++;

                    }else{
                        i++;
                    }
                }
                j++;
                while(j<5){
                    a[j].setVisibility(View.GONE);

                    e[j].setVisibility(View.GONE);
                    e[j].setText("");
                    n[j].setVisibility(View.GONE);
                    j++;
                }
                switch (nombrefotoavant){
                    case 0 :
                        final CharSequence[] items = {
                                "prendre une photo", "choisir apartir du gallery", "Annuler l'Ajout"
                        };

                        AlertDialog.Builder builder = new AlertDialog.Builder(detailOperation.this);
                        builder.setIcon(R.mipmap.button_ajout);
                        builder.setTitle("Ajout d'une  photo");
                        builder.setItems(items, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                // Do something with the selection
                                if (item == 0) {
                                    nombrefotoavant++;
                                    isavant1=true;
                                    avant1.setVisibility(View.VISIBLE);
                                    editnote1.setVisibility(View.VISIBLE);
                                    Note1.setVisibility(View.VISIBLE);

                                    avant2.setVisibility(View.GONE);
                                    editnote2.setVisibility(View.GONE);
                                    Note2.setVisibility(View.GONE);

                                    avant3.setVisibility(View.GONE);
                                    editnote3.setVisibility(View.GONE);
                                    Note3.setVisibility(View.GONE);

                                    avant4.setVisibility(View.GONE);
                                    editnote4.setVisibility(View.GONE);
                                    Note4.setVisibility(View.GONE);

                                    avant5.setVisibility(View.GONE);
                                    editnote5.setVisibility(View.GONE);
                                    Note5.setVisibility(View.GONE);
                                    Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(cameraintent, cam_request1);
                                } else {

                                    if (item == 1) {
                                        nombrefotoavant++;
                                        isavant1=true;

                                        avant1.setVisibility(View.VISIBLE);
                                        editnote1.setVisibility(View.VISIBLE);
                                        Note1.setVisibility(View.VISIBLE);

                                        avant2.setVisibility(View.GONE);
                                        editnote2.setVisibility(View.GONE);
                                        Note2.setVisibility(View.GONE);

                                        avant3.setVisibility(View.GONE);
                                        editnote3.setVisibility(View.GONE);
                                        Note3.setVisibility(View.GONE);

                                        avant4.setVisibility(View.GONE);
                                        editnote4.setVisibility(View.GONE);
                                        Note4.setVisibility(View.GONE);

                                        avant5.setVisibility(View.GONE);
                                        editnote5.setVisibility(View.GONE);
                                        Note5.setVisibility(View.GONE);
                                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(intent, gallery_request1);
                                        //traitement de choisir apartir du gallery
                                    } else {
                                        if (item == 2) {
                                            //traitement de annuler
                                        }
                                    }
                                }

                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();

                        break;
                    case 1 :
                        final CharSequence[] items1 = {
                                "prendre une photo", "choisir apartir du gallery", "Annuler l'Ajout"
                        };

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(detailOperation.this);
                        builder1.setIcon(R.mipmap.button_ajout);
                        builder1.setTitle("Ajout d'une  photo");
                        builder1.setItems(items1, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                // Do something with the selection
                                if (item == 0) {
                                    nombrefotoavant++;
                                    isavant2=true;

                                    avant1.setVisibility(View.VISIBLE);
                                    editnote1.setVisibility(View.VISIBLE);
                                    Note1.setVisibility(View.VISIBLE);

                                    avant2.setVisibility(View.VISIBLE);
                                    editnote2.setVisibility(View.VISIBLE);
                                    Note2.setVisibility(View.VISIBLE);

                                    avant3.setVisibility(View.GONE);
                                    editnote3.setVisibility(View.GONE);
                                    Note3.setVisibility(View.GONE);

                                    avant4.setVisibility(View.GONE);
                                    editnote4.setVisibility(View.GONE);
                                    Note4.setVisibility(View.GONE);

                                    avant5.setVisibility(View.GONE);
                                    editnote5.setVisibility(View.GONE);
                                    Note5.setVisibility(View.GONE);
                                    Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(cameraintent, cam_request2);
                                } else {
                                    if (item == 1) {
                                        nombrefotoavant++;
                                        isavant2=true;

                                        avant1.setVisibility(View.VISIBLE);
                                        editnote1.setVisibility(View.VISIBLE);
                                        Note1.setVisibility(View.VISIBLE);

                                        avant2.setVisibility(View.VISIBLE);
                                        editnote2.setVisibility(View.VISIBLE);
                                        Note2.setVisibility(View.VISIBLE);

                                        avant3.setVisibility(View.GONE);
                                        editnote3.setVisibility(View.GONE);
                                        Note3.setVisibility(View.GONE);

                                        avant4.setVisibility(View.GONE);
                                        editnote4.setVisibility(View.GONE);
                                        Note4.setVisibility(View.GONE);

                                        avant5.setVisibility(View.GONE);
                                        editnote5.setVisibility(View.GONE);
                                        Note5.setVisibility(View.GONE);
                                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(intent, gallery_request2);
                                        //traitement de choisir apartir du gallery
                                    } else {
                                        if (item == 2) {
                                            //traitement de annuler
                                        }
                                    }
                                }

                            }
                        });
                        AlertDialog alert1 = builder1.create();
                        alert1.show();
                        break;

                    case 2 :
                        final CharSequence[] items2 = {
                                "prendre une photo", "choisir apartir du gallery", "Annuler l'Ajout"
                        };

                        AlertDialog.Builder builder2 = new AlertDialog.Builder(detailOperation.this);
                        builder2.setIcon(R.mipmap.button_ajout);
                        builder2.setTitle("Ajout d'une  photo");
                        builder2.setItems(items2, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                // Do something with the selection
                                if (item == 0) {
                                    nombrefotoavant++;
                                    isavant3=true;

                                    avant1.setVisibility(View.VISIBLE);
                                    editnote1.setVisibility(View.VISIBLE);
                                    Note1.setVisibility(View.VISIBLE);

                                    avant2.setVisibility(View.VISIBLE);
                                    editnote2.setVisibility(View.VISIBLE);
                                    Note2.setVisibility(View.VISIBLE);

                                    avant3.setVisibility(View.VISIBLE);
                                    editnote3.setVisibility(View.VISIBLE);
                                    Note3.setVisibility(View.VISIBLE);

                                    avant4.setVisibility(View.GONE);
                                    editnote4.setVisibility(View.GONE);
                                    Note4.setVisibility(View.GONE);

                                    avant5.setVisibility(View.GONE);
                                    editnote5.setVisibility(View.GONE);
                                    Note5.setVisibility(View.GONE);
                                    Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(cameraintent, cam_request3);
                                } else {
                                    if (item == 1) {
                                        nombrefotoavant++;
                                        isavant3=true;

                                        //traitement de choisir apartir du gallery
                                        avant1.setVisibility(View.VISIBLE);
                                        editnote1.setVisibility(View.VISIBLE);
                                        Note1.setVisibility(View.VISIBLE);

                                        avant2.setVisibility(View.VISIBLE);
                                        editnote2.setVisibility(View.VISIBLE);
                                        Note2.setVisibility(View.VISIBLE);

                                        avant3.setVisibility(View.VISIBLE);
                                        editnote3.setVisibility(View.VISIBLE);
                                        Note3.setVisibility(View.VISIBLE);

                                        avant4.setVisibility(View.GONE);
                                        editnote4.setVisibility(View.GONE);
                                        Note4.setVisibility(View.GONE);

                                        avant5.setVisibility(View.GONE);
                                        editnote5.setVisibility(View.GONE);
                                        Note5.setVisibility(View.GONE);
                                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(intent, gallery_request3);
                                    } else {
                                        if (item == 2) {

                                            //traitement de annuler
                                        }
                                    }
                                }

                            }
                        });
                        AlertDialog alert2 = builder2.create();
                        alert2.show();

                        break;
                    case 3 :
                        final CharSequence[] items3 = {
                                "prendre une photo", "choisir apartir du gallery", "Annuler l'Ajout"
                        };

                        AlertDialog.Builder builder3 = new AlertDialog.Builder(detailOperation.this);
                        builder3.setIcon(R.mipmap.button_ajout);
                        builder3.setTitle("Ajout d'une  photo");
                        builder3.setItems(items3, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                // Do something with the selection
                                if (item == 0) {
                                    nombrefotoavant++;
                                    isavant4=true;

                                    avant1.setVisibility(View.VISIBLE);
                                    editnote1.setVisibility(View.VISIBLE);
                                    Note1.setVisibility(View.VISIBLE);

                                    avant2.setVisibility(View.VISIBLE);
                                    editnote2.setVisibility(View.VISIBLE);
                                    Note2.setVisibility(View.VISIBLE);

                                    avant3.setVisibility(View.VISIBLE);
                                    editnote3.setVisibility(View.VISIBLE);
                                    Note3.setVisibility(View.VISIBLE);

                                    avant4.setVisibility(View.VISIBLE);
                                    editnote4.setVisibility(View.VISIBLE);
                                    Note4.setVisibility(View.VISIBLE);

                                    avant5.setVisibility(View.GONE);
                                    editnote5.setVisibility(View.GONE);
                                    Note5.setVisibility(View.GONE);
                                    Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(cameraintent, cam_request4);
                                } else {
                                    if (item == 1) {
                                        nombrefotoavant++;
                                        isavant4=true;
                                        avant1.setVisibility(View.VISIBLE);
                                        editnote1.setVisibility(View.VISIBLE);
                                        Note1.setVisibility(View.VISIBLE);

                                        avant2.setVisibility(View.VISIBLE);
                                        editnote2.setVisibility(View.VISIBLE);
                                        Note2.setVisibility(View.VISIBLE);

                                        avant3.setVisibility(View.VISIBLE);
                                        editnote3.setVisibility(View.VISIBLE);
                                        Note3.setVisibility(View.VISIBLE);

                                        avant4.setVisibility(View.VISIBLE);
                                        editnote4.setVisibility(View.VISIBLE);
                                        Note4.setVisibility(View.VISIBLE);

                                        avant5.setVisibility(View.GONE);
                                        editnote5.setVisibility(View.GONE);
                                        Note5.setVisibility(View.GONE);
                                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(intent, gallery_request4);
                                        //traitement de choisir apartir du gallery
                                    } else {
                                        if (item == 2) {
                                            //traitement de annuler
                                        }
                                    }
                                }

                            }
                        });
                        AlertDialog alert3 = builder3.create();
                        alert3.show();

                        break;
                    case 4 :
                        final CharSequence[] items4 = {
                                "prendre une photo", "choisir apartir du gallery", "Annuler l'Ajout"
                        };

                        AlertDialog.Builder builder4 = new AlertDialog.Builder(detailOperation.this);
                        builder4.setIcon(R.mipmap.button_ajout);
                        builder4.setTitle("Ajout d'une  photo");
                        builder4.setItems(items4, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                // Do something with the selection
                                if (item == 0) {
                                    nombrefotoavant++;
                                    isavant5=true;

                                    avant1.setVisibility(View.VISIBLE);
                                    editnote1.setVisibility(View.VISIBLE);
                                    Note1.setVisibility(View.VISIBLE);

                                    avant2.setVisibility(View.VISIBLE);
                                    editnote2.setVisibility(View.VISIBLE);
                                    Note2.setVisibility(View.VISIBLE);

                                    avant3.setVisibility(View.VISIBLE);
                                    editnote3.setVisibility(View.VISIBLE);
                                    Note3.setVisibility(View.VISIBLE);

                                    avant4.setVisibility(View.VISIBLE);
                                    editnote4.setVisibility(View.VISIBLE);
                                    Note4.setVisibility(View.VISIBLE);

                                    avant5.setVisibility(View.VISIBLE);
                                    editnote5.setVisibility(View.VISIBLE);
                                    Note5.setVisibility(View.VISIBLE);
                                    Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(cameraintent, cam_request5);
                                } else {
                                    if (item == 1) {
                                        nombrefotoavant++;
                                        isavant5=true;

                                        avant1.setVisibility(View.VISIBLE);
                                        editnote1.setVisibility(View.VISIBLE);
                                        Note1.setVisibility(View.VISIBLE);

                                        avant2.setVisibility(View.VISIBLE);
                                        editnote2.setVisibility(View.VISIBLE);
                                        Note2.setVisibility(View.VISIBLE);

                                        avant3.setVisibility(View.VISIBLE);
                                        editnote3.setVisibility(View.VISIBLE);
                                        Note3.setVisibility(View.VISIBLE);

                                        avant4.setVisibility(View.VISIBLE);
                                        editnote4.setVisibility(View.VISIBLE);
                                        Note4.setVisibility(View.VISIBLE);

                                        avant5.setVisibility(View.VISIBLE);
                                        editnote5.setVisibility(View.VISIBLE);
                                        Note5.setVisibility(View.VISIBLE);
                                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(intent, gallery_request5);
                                        //traitement de choisir apartir du gallery
                                    } else {
                                        if (item == 2) {
                                            //traitement de annuler
                                        }
                                    }
                                }

                            }
                        });
                        AlertDialog alert4 = builder4.create();
                        alert4.show();

                        break;

                    default:
                        AlertDialog.Builder adb = new AlertDialog.Builder(detailOperation.this);

                        adb.setTitle("erreur");

                        adb.setMessage("vous ne pouvez ajouter pas plus que 5 photos ");

                        adb.setPositiveButton("Ok", null);

                        adb.show();

                }

            }
        });




        avant1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {
                        "prendre une photo", "choisir apartir du gallery","supprimer photo", "Annuler le modification"
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(detailOperation.this);
                builder.setIcon(avant1.getDrawable());
                builder.setTitle("modifier  photo");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        if (item == 0) {
                            isavant1=true;
                            Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraintent, cam_request1);
                        } else {
                            if (item == 1) {
                                isavant1=true;
                                Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, gallery_request1);
                                //traitement de choisir apartir du gallery
                            } else {
                                if (item == 2) {
                                    avant1.setImageResource(R.mipmap.button_ajout);
                                    editnote1.setText("");
                                    isavant1=false;

                                    avant1.setVisibility(View.GONE);
                                    editnote1.setVisibility(View.GONE);
                                    Note1.setVisibility(View.GONE);




                                    //traitement de annuler
                                }
                            }
                        }

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        avant2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {
                        "prendre une photo", "choisir apartir du gallery","supprimer photo", "Annuler le modification"
                };


                AlertDialog.Builder builder = new AlertDialog.Builder(detailOperation.this);
                builder.setIcon(R.mipmap.button_ajout);
                builder.setTitle("Ajout d'une  photo");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        if (item == 0) {
                            isavant2=true;
                            Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraintent, cam_request2);
                        } else {
                            if (item == 1) {
                                isavant2=true;
                                Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, gallery_request2);
                                //traitement de choisir apartir du gallery
                            } else {
                                if (item == 2) {
                                    avant2.setImageResource(R.mipmap.button_ajout);
                                    editnote2.setText("");
                                    isavant2=false;

                                    avant2.setVisibility(View.GONE);
                                    editnote2.setVisibility(View.GONE);
                                    Note2.setVisibility(View.GONE);


                                    //traitement de annuler
                                }
                            }
                        }

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        avant3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {
                        "prendre une photo", "choisir apartir du gallery","supprimer photo", "Annuler le modification"
                };


                AlertDialog.Builder builder = new AlertDialog.Builder(detailOperation.this);
                builder.setIcon(R.mipmap.button_ajout);
                builder.setTitle("Ajout d'une  photo");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        if (item == 0) {
                            isavant3=true;
                            Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraintent, cam_request3);
                        } else {
                            if (item == 1) {
                                isavant3=true;
                                Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, gallery_request3);
                                //traitement de choisir apartir du gallery
                            } else {
                                if (item == 2) {
                                    //traitement de annuler
                                    avant3.setImageResource(R.mipmap.button_ajout);
                                    editnote3.setText("");
                                    isavant3=false;
                                    avant3.setVisibility(View.GONE);
                                    editnote3.setVisibility(View.GONE);
                                    Note3.setVisibility(View.GONE);

                                }
                            }
                        }

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        avant4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {
                        "prendre une photo", "choisir apartir du gallery","supprimer photo", "Annuler le modification"
                };


                AlertDialog.Builder builder = new AlertDialog.Builder(detailOperation.this);
                builder.setIcon(R.mipmap.button_ajout);
                builder.setTitle("Ajout d'une  photo");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        if (item == 0) {
                            isavant4=true;
                            Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraintent, cam_request4);
                        } else {
                            if (item == 1) {
                                isavant4=true;
                                Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, gallery_request4);
                                //traitement de choisir apartir du gallery
                            } else {
                                if (item == 2) {
                                    //traitement de annuler
                                    avant4.setImageResource(R.mipmap.button_ajout);
                                    editnote4.setText("");
                                    isavant4=false;
                                    avant4.setVisibility(View.GONE);
                                    editnote4.setVisibility(View.GONE);
                                    Note4.setVisibility(View.GONE);

                                }
                            }
                        }

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        avant5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {
                        "prendre une photo", "choisir apartir du gallery","supprimer photo", "Annuler le modification"
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(detailOperation.this);
                builder.setIcon(R.mipmap.button_ajout);
                builder.setTitle("Ajout d'une  photo");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        if (item == 0) {
                            isavant5=true;
                            Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraintent, cam_request5);
                        } else {
                            if (item == 1) {
                                isavant5=true;
                                Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, gallery_request5);
                                //traitement de choisir apartir du gallery
                            } else {
                                if (item == 2) {
                                    //traitement de annuler
                                    avant5.setImageResource(R.mipmap.button_ajout);
                                    editnote5.setText("");
                                    isavant5=false;
                                    avant5.setVisibility(View.GONE);
                                    editnote5.setVisibility(View.GONE);
                                    Note5.setVisibility(View.GONE);

                                }
                            }
                        }

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });


        ajoutertofavant1= (Button) findViewById(R.id.ajoutertofavant1);

        Note11= (TextView) findViewById(R.id.staticavant11);
        Note21= (TextView) findViewById(R.id.staticavant21);
        Note31= (TextView) findViewById(R.id.staticavant31);
        Note41= (TextView) findViewById(R.id.staticavant41);
        Note51= (TextView) findViewById(R.id.staticavant51);

        editnote11= (EditText) findViewById(R.id.editavant11);
        editnote21= (EditText) findViewById(R.id.editavant21);
        editnote31= (EditText) findViewById(R.id.editavant31);
        editnote41= (EditText) findViewById(R.id.editavant41);
        editnote51= (EditText) findViewById(R.id.editavant51);

        avant11= (ImageView) findViewById(R.id.avant11);
        avant21= (ImageView) findViewById(R.id.avant21);
        avant31= (ImageView) findViewById(R.id.avant31);
        avant41= (ImageView) findViewById(R.id.avant41);
        avant51= (ImageView) findViewById(R.id.avant51);


        avant11.setVisibility(View.GONE);
        editnote11.setVisibility(View.GONE);
        Note11.setVisibility(View.GONE);

        avant21.setVisibility(View.GONE);
        editnote21.setVisibility(View.GONE);
        Note21.setVisibility(View.GONE);

        avant31.setVisibility(View.GONE);
        editnote31.setVisibility(View.GONE);
        Note31.setVisibility(View.GONE);

        avant41.setVisibility(View.GONE);
        editnote41.setVisibility(View.GONE);
        Note41.setVisibility(View.GONE);

        avant51.setVisibility(View.GONE);
        editnote51.setVisibility(View.GONE);
        Note51.setVisibility(View.GONE);
        if(!ancienapres1.equals("")) {
            ap1 = downloadImage(ancienapres1);
        }
        if(!ancienapres2.equals("")) {
            ap2 = downloadImage(ancienapres2);
        }
        if(!ancienapres3.equals("")) {
            ap3 = downloadImage(ancienapres3);
        }
        if(!ancienapres4.equals("")) {
            ap4 = downloadImage(ancienapres4);
        }
        if(!ancienapres5.equals("")) {
            ap5 = downloadImage(ancienapres5);
        }

        boolean[]x1={isapres1,isapres2,isapres3,isapres4,isapres5};
        ImageView[]a1={avant11,avant21,avant31,avant41,avant51};
        EditText[]e1={editnote11,editnote21,editnote31,editnote41,editnote51};
        TextView[]n1={ Note11 ,  Note21, Note41, Note31, Note51};
        Bitmap[]b1={ap1 , ap2, ap3,ap4,ap5};
        String[]d1={anciendescapres1,anciendescapres2,anciendescapres3,anciendescapres4,anciendescapres5};
        int i1 =0;
        int j1 =0;
        nombrefotoavant1=0;
        while(i1<x1.length){
            if(x1[i1]){
                nombrefotoavant1++;
                a1[j1].setVisibility(View.VISIBLE);
                a1[j1].setImageBitmap(b1[i1]);
                e1[j1].setVisibility(View.VISIBLE);
                e1[j1].setText(d1[i1]);
                n1[j1].setVisibility(View.VISIBLE);
                i1++;
                j1++;

            }else{
                i1++;
            }
        }
        j1++;
        while(j1<5){
            a1[j1].setVisibility(View.GONE);

            e1[j1].setVisibility(View.GONE);
            e1[j1].setText("");
            n1[j1].setVisibility(View.GONE);
            j1++;
        }

        ajoutertofavant1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                switch (nombrefotoavant1){
                    case 0 :
                        final CharSequence[] items = {
                                "prendre une photo", "choisir apartir du gallery", "Annuler l'Ajout"
                        };

                        AlertDialog.Builder builder = new AlertDialog.Builder(detailOperation.this);
                        builder.setIcon(R.mipmap.button_ajout);
                        builder.setTitle("Ajout d'une  photo");
                        builder.setItems(items, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                // Do something with the selection
                                if (item == 0) {
                                    nombrefotoavant1++;
                                    avant11.setVisibility(View.VISIBLE);
                                    editnote11.setVisibility(View.VISIBLE);
                                    Note11.setVisibility(View.VISIBLE);

                                    avant21.setVisibility(View.GONE);
                                    editnote21.setVisibility(View.GONE);
                                    Note21.setVisibility(View.GONE);

                                    avant31.setVisibility(View.GONE);
                                    editnote31.setVisibility(View.GONE);
                                    Note31.setVisibility(View.GONE);

                                    avant41.setVisibility(View.GONE);
                                    editnote41.setVisibility(View.GONE);
                                    Note41.setVisibility(View.GONE);

                                    avant51.setVisibility(View.GONE);
                                    editnote51.setVisibility(View.GONE);
                                    Note51.setVisibility(View.GONE);
                                    Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(cameraintent, cam_request11);
                                } else {
                                    if (item == 1) {
                                        nombrefotoavant1++;
                                        avant11.setVisibility(View.VISIBLE);
                                        editnote11.setVisibility(View.VISIBLE);
                                        Note11.setVisibility(View.VISIBLE);

                                        avant21.setVisibility(View.GONE);
                                        editnote21.setVisibility(View.GONE);
                                        Note21.setVisibility(View.GONE);

                                        avant31.setVisibility(View.GONE);
                                        editnote31.setVisibility(View.GONE);
                                        Note31.setVisibility(View.GONE);

                                        avant41.setVisibility(View.GONE);
                                        editnote41.setVisibility(View.GONE);
                                        Note41.setVisibility(View.GONE);

                                        avant51.setVisibility(View.GONE);
                                        editnote51.setVisibility(View.GONE);
                                        Note51.setVisibility(View.GONE);
                                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(intent, gallery_request11);
                                        //traitement de choisir apartir du gallery
                                    } else {
                                        if (item == 2) {
                                            //traitement de annuler
                                        }
                                    }
                                }

                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();

                        break;
                    case 1 :
                        final CharSequence[] items1 = {
                                "prendre une photo", "choisir apartir du gallery", "Annuler l'Ajout"
                        };

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(detailOperation.this);
                        builder1.setIcon(R.mipmap.button_ajout);
                        builder1.setTitle("Ajout d'une  photo");
                        builder1.setItems(items1, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                // Do something with the selection
                                if (item == 0) {
                                    nombrefotoavant1++;
                                    avant11.setVisibility(View.VISIBLE);
                                    editnote11.setVisibility(View.VISIBLE);
                                    Note11.setVisibility(View.VISIBLE);

                                    avant21.setVisibility(View.VISIBLE);
                                    editnote21.setVisibility(View.VISIBLE);
                                    Note21.setVisibility(View.VISIBLE);

                                    avant31.setVisibility(View.GONE);
                                    editnote31.setVisibility(View.GONE);
                                    Note31.setVisibility(View.GONE);

                                    avant41.setVisibility(View.GONE);
                                    editnote41.setVisibility(View.GONE);
                                    Note41.setVisibility(View.GONE);

                                    avant51.setVisibility(View.GONE);
                                    editnote51.setVisibility(View.GONE);
                                    Note51.setVisibility(View.GONE);
                                    Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(cameraintent, cam_request21);
                                } else {
                                    if (item == 1) {
                                        nombrefotoavant1++;
                                        avant11.setVisibility(View.VISIBLE);
                                        editnote11.setVisibility(View.VISIBLE);
                                        Note11.setVisibility(View.VISIBLE);

                                        avant21.setVisibility(View.VISIBLE);
                                        editnote21.setVisibility(View.VISIBLE);
                                        Note21.setVisibility(View.VISIBLE);

                                        avant31.setVisibility(View.GONE);
                                        editnote31.setVisibility(View.GONE);
                                        Note31.setVisibility(View.GONE);

                                        avant41.setVisibility(View.GONE);
                                        editnote41.setVisibility(View.GONE);
                                        Note41.setVisibility(View.GONE);

                                        avant51.setVisibility(View.GONE);
                                        editnote51.setVisibility(View.GONE);
                                        Note51.setVisibility(View.GONE);
                                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(intent, gallery_request21);
                                        //traitement de choisir apartir du gallery
                                    } else {
                                        if (item == 2) {
                                            //traitement de annuler
                                        }
                                    }
                                }

                            }
                        });
                        AlertDialog alert1 = builder1.create();
                        alert1.show();
                        break;

                    case 2 :
                        final CharSequence[] items2 = {
                                "prendre une photo", "choisir apartir du gallery", "Annuler l'Ajout"
                        };

                        AlertDialog.Builder builder2 = new AlertDialog.Builder(detailOperation.this);
                        builder2.setIcon(R.mipmap.button_ajout);
                        builder2.setTitle("Ajout d'une  photo");
                        builder2.setItems(items2, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                // Do something with the selection
                                if (item == 0) {
                                    nombrefotoavant1++;
                                    avant11.setVisibility(View.VISIBLE);
                                    editnote11.setVisibility(View.VISIBLE);
                                    Note11.setVisibility(View.VISIBLE);

                                    avant21.setVisibility(View.VISIBLE);
                                    editnote21.setVisibility(View.VISIBLE);
                                    Note21.setVisibility(View.VISIBLE);

                                    avant31.setVisibility(View.VISIBLE);
                                    editnote31.setVisibility(View.VISIBLE);
                                    Note31.setVisibility(View.VISIBLE);

                                    avant41.setVisibility(View.GONE);
                                    editnote41.setVisibility(View.GONE);
                                    Note41.setVisibility(View.GONE);

                                    avant51.setVisibility(View.GONE);
                                    editnote51.setVisibility(View.GONE);
                                    Note51.setVisibility(View.GONE);
                                    Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(cameraintent, cam_request31);
                                } else {
                                    if (item == 1) {
                                        nombrefotoavant1++;
                                        //traitement de choisir apartir du gallery
                                        avant11.setVisibility(View.VISIBLE);
                                        editnote11.setVisibility(View.VISIBLE);
                                        Note11.setVisibility(View.VISIBLE);

                                        avant21.setVisibility(View.VISIBLE);
                                        editnote21.setVisibility(View.VISIBLE);
                                        Note21.setVisibility(View.VISIBLE);

                                        avant31.setVisibility(View.VISIBLE);
                                        editnote31.setVisibility(View.VISIBLE);
                                        Note31.setVisibility(View.VISIBLE);

                                        avant41.setVisibility(View.GONE);
                                        editnote41.setVisibility(View.GONE);
                                        Note41.setVisibility(View.GONE);

                                        avant51.setVisibility(View.GONE);
                                        editnote51.setVisibility(View.GONE);
                                        Note51.setVisibility(View.GONE);
                                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(intent, gallery_request31);
                                    } else {
                                        if (item == 2) {

                                            //traitement de annuler
                                        }
                                    }
                                }

                            }
                        });
                        AlertDialog alert2 = builder2.create();
                        alert2.show();

                        break;
                    case 3 :
                        final CharSequence[] items3 = {
                                "prendre une photo", "choisir apartir du gallery", "Annuler l'Ajout"
                        };

                        AlertDialog.Builder builder3 = new AlertDialog.Builder(detailOperation.this);
                        builder3.setIcon(R.mipmap.button_ajout);
                        builder3.setTitle("Ajout d'une  photo");
                        builder3.setItems(items3, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                // Do something with the selection
                                if (item == 0) {
                                    nombrefotoavant1++;
                                    avant11.setVisibility(View.VISIBLE);
                                    editnote11.setVisibility(View.VISIBLE);
                                    Note11.setVisibility(View.VISIBLE);

                                    avant21.setVisibility(View.VISIBLE);
                                    editnote21.setVisibility(View.VISIBLE);
                                    Note21.setVisibility(View.VISIBLE);

                                    avant31.setVisibility(View.VISIBLE);
                                    editnote31.setVisibility(View.VISIBLE);
                                    Note31.setVisibility(View.VISIBLE);

                                    avant41.setVisibility(View.VISIBLE);
                                    editnote41.setVisibility(View.VISIBLE);
                                    Note41.setVisibility(View.VISIBLE);

                                    avant51.setVisibility(View.GONE);
                                    editnote51.setVisibility(View.GONE);
                                    Note51.setVisibility(View.GONE);
                                    Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(cameraintent, cam_request41);
                                } else {
                                    if (item == 1) {
                                        nombrefotoavant1++;
                                        avant11.setVisibility(View.VISIBLE);
                                        editnote11.setVisibility(View.VISIBLE);
                                        Note11.setVisibility(View.VISIBLE);

                                        avant21.setVisibility(View.VISIBLE);
                                        editnote21.setVisibility(View.VISIBLE);
                                        Note21.setVisibility(View.VISIBLE);

                                        avant31.setVisibility(View.VISIBLE);
                                        editnote31.setVisibility(View.VISIBLE);
                                        Note31.setVisibility(View.VISIBLE);

                                        avant41.setVisibility(View.VISIBLE);
                                        editnote41.setVisibility(View.VISIBLE);
                                        Note41.setVisibility(View.VISIBLE);

                                        avant51.setVisibility(View.GONE);
                                        editnote51.setVisibility(View.GONE);
                                        Note51.setVisibility(View.GONE);
                                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(intent, gallery_request41);
                                        //traitement de choisir apartir du gallery
                                    } else {
                                        if (item == 2) {
                                            //traitement de annuler
                                        }
                                    }
                                }

                            }
                        });
                        AlertDialog alert3 = builder3.create();
                        alert3.show();

                        break;
                    case 4 :
                        final CharSequence[] items4 = {
                                "prendre une photo", "choisir apartir du gallery", "Annuler l'Ajout"
                        };

                        AlertDialog.Builder builder4 = new AlertDialog.Builder(detailOperation.this);
                        builder4.setIcon(R.mipmap.button_ajout);
                        builder4.setTitle("Ajout d'une  photo");
                        builder4.setItems(items4, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                // Do something with the selection
                                if (item == 0) {
                                    nombrefotoavant1++;
                                    avant11.setVisibility(View.VISIBLE);
                                    editnote11.setVisibility(View.VISIBLE);
                                    Note11.setVisibility(View.VISIBLE);

                                    avant21.setVisibility(View.VISIBLE);
                                    editnote21.setVisibility(View.VISIBLE);
                                    Note21.setVisibility(View.VISIBLE);

                                    avant31.setVisibility(View.VISIBLE);
                                    editnote31.setVisibility(View.VISIBLE);
                                    Note31.setVisibility(View.VISIBLE);

                                    avant41.setVisibility(View.VISIBLE);
                                    editnote41.setVisibility(View.VISIBLE);
                                    Note41.setVisibility(View.VISIBLE);

                                    avant51.setVisibility(View.VISIBLE);
                                    editnote51.setVisibility(View.VISIBLE);
                                    Note51.setVisibility(View.VISIBLE);
                                    Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(cameraintent, cam_request51);
                                } else {
                                    if (item == 1) {
                                        nombrefotoavant1++;
                                        avant11.setVisibility(View.VISIBLE);
                                        editnote11.setVisibility(View.VISIBLE);
                                        Note11.setVisibility(View.VISIBLE);

                                        avant21.setVisibility(View.VISIBLE);
                                        editnote21.setVisibility(View.VISIBLE);
                                        Note21.setVisibility(View.VISIBLE);

                                        avant31.setVisibility(View.VISIBLE);
                                        editnote31.setVisibility(View.VISIBLE);
                                        Note31.setVisibility(View.VISIBLE);

                                        avant41.setVisibility(View.VISIBLE);
                                        editnote41.setVisibility(View.VISIBLE);
                                        Note41.setVisibility(View.VISIBLE);

                                        avant51.setVisibility(View.VISIBLE);
                                        editnote51.setVisibility(View.VISIBLE);
                                        Note51.setVisibility(View.VISIBLE);
                                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(intent, gallery_request51);
                                        //traitement de choisir apartir du gallery
                                    } else {
                                        if (item == 2) {
                                            //traitement de annuler
                                        }
                                    }
                                }

                            }
                        });
                        AlertDialog alert4 = builder4.create();
                        alert4.show();

                        break;

                    default:
                        AlertDialog.Builder adb = new AlertDialog.Builder(detailOperation.this);

                        adb.setTitle("erreur");

                        adb.setMessage("vous ne pouvez ajouter pas plus que 5 photos ");

                        adb.setPositiveButton("Ok", null);

                        adb.show();

                }

            }
        });

/*
        avant11.setImageBitmap(ap1);
        editnote11.setText(anciendescapres1);
        avant21.setImageBitmap(ap2);
        editnote21.setText(anciendescapres2);
        avant31.setImageBitmap(ap3);
        editnote31.setText(anciendescapres3);
        avant41.setImageBitmap(ap4);
        editnote41.setText(anciendescapres4);
        avant51.setImageBitmap(ap5);
        editnote51.setText(anciendescapres5);
        */

        avant11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {
                        "prendre une photo", "choisir apartir du gallery","supprimer photo", "Annuler le modification"
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(detailOperation.this);
                builder.setIcon(avant11.getDrawable());
                builder.setTitle("modifier  photo");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        if (item == 0) {
                            Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraintent, cam_request11);
                        } else {
                            if (item == 1) {
                                Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, gallery_request11);
                                //traitement de choisir apartir du gallery
                            } else {
                                if (item == 2) {
                                    avant11.setImageResource(R.mipmap.button_ajout);
                                    editnote11.setText("");
                                    isapres1=false;
                                    avant11.setVisibility(View.GONE);
                                    editnote11.setVisibility(View.GONE);
                                    Note11.setVisibility(View.GONE);
                                    //traitement de annuler
                                }
                            }
                        }

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        avant21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {
                        "prendre une photo", "choisir apartir du gallery","supprimer photo", "Annuler le modification"
                };


                AlertDialog.Builder builder = new AlertDialog.Builder(detailOperation.this);
                builder.setIcon(R.mipmap.button_ajout);
                builder.setTitle("Ajout d'une  photo");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        if (item == 0) {
                            Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraintent, cam_request21);
                        } else {
                            if (item == 1) {
                                Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, gallery_request21);
                                //traitement de choisir apartir du gallery
                            } else {
                                if (item == 2) {
                                    avant21.setImageResource(R.mipmap.button_ajout);
                                    editnote21.setText("");
                                    isapres2=false;
                                    avant21.setVisibility(View.GONE);
                                    editnote21.setVisibility(View.GONE);
                                    Note21.setVisibility(View.GONE);
                                    //traitement de annuler
                                }
                            }
                        }

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        avant31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {
                        "prendre une photo", "choisir apartir du gallery","supprimer photo", "Annuler le modification"
                };


                AlertDialog.Builder builder = new AlertDialog.Builder(detailOperation.this);
                builder.setIcon(R.mipmap.button_ajout);
                builder.setTitle("Ajout d'une  photo");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        if (item == 0) {
                            Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraintent, cam_request31);
                        } else {
                            if (item == 1) {
                                Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, gallery_request31);
                                //traitement de choisir apartir du gallery
                            } else {
                                if (item == 2) {
                                    //traitement de annuler
                                    avant31.setImageResource(R.mipmap.button_ajout);
                                    editnote31.setText("");
                                    isapres3=false;
                                    avant31.setVisibility(View.GONE);
                                    editnote11.setVisibility(View.GONE);
                                    Note11.setVisibility(View.GONE);
                                }
                            }
                        }

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        avant41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {
                        "prendre une photo", "choisir apartir du gallery","supprimer photo", "Annuler le modification"
                };


                AlertDialog.Builder builder = new AlertDialog.Builder(detailOperation.this);
                builder.setIcon(R.mipmap.button_ajout);
                builder.setTitle("Ajout d'une  photo");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        if (item == 0) {
                            Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraintent, cam_request41);
                        } else {
                            if (item == 1) {
                                Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, gallery_request41);
                                //traitement de choisir apartir du gallery
                            } else {
                                if (item == 2) {
                                    //traitement de annuler
                                    avant41.setImageResource(R.mipmap.button_ajout);
                                    editnote41.setText("");
                                    isapres4=false;
                                    avant41.setVisibility(View.GONE);
                                    editnote41.setVisibility(View.GONE);
                                    Note41.setVisibility(View.GONE);
                                }
                            }
                        }

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        avant51.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {
                        "prendre une photo", "choisir apartir du gallery","supprimer photo", "Annuler le modification"
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(detailOperation.this);
                builder.setIcon(R.mipmap.button_ajout);
                builder.setTitle("Ajout d'une  photo");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        if (item == 0) {
                            Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraintent, cam_request51);
                        } else {
                            if (item == 1) {
                                Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, gallery_request51);
                                //traitement de choisir apartir du gallery
                            } else {
                                if (item == 2) {
                                    //traitement de annuler
                                    avant51.setImageResource(R.mipmap.button_ajout);
                                    editnote51.setText("");
                                    isapres5=false;
                                    avant51.setVisibility(View.GONE);
                                    editnote51.setVisibility(View.GONE);
                                    Note51.setVisibility(View.GONE);
                                }
                            }
                        }

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

















        errordetailsoperationautre= (TextView) findViewById(R.id.erordetailoperationautre);
        errordetailsoperationseins= (TextView) findViewById(R.id.erordetailoperationseins);
        errordetailsoperationcorp= (TextView) findViewById(R.id.erordetailoperationcorp);
        errordetailsoperationvisage= (TextView) findViewById(R.id.erordetailoperationvisage);
        errordetailsoperation= (TextView) findViewById(R.id.erordetailoperation);
        errorNum_admission= (TextView) findViewById(R.id.erorNum_adminission);
        num_adminission= (EditText) findViewById(R.id.editNum_adminission);


        sexe="";
        dateNaissance="";
        toolbar= (Toolbar) findViewById(R.id.app_bar);
       setSupportActionBar(toolbar);
        tabHost=(TabHost)findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec spec1=tabHost.newTabSpec("Details patient");
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("Details patient");


        TabHost.TabSpec spec2=tabHost.newTabSpec("Details operation");
        spec2.setIndicator("Details operation");
        spec2.setContent(R.id.tab2);


        TabHost.TabSpec spec3=tabHost.newTabSpec("Photos Avant");
        spec3.setContent(R.id.tab3);
        spec3.setIndicator("Photos Avant");

        TabHost.TabSpec spec4=tabHost.newTabSpec("Photos Apres");
        spec4.setContent(R.id.tab4);
        spec4.setIndicator("Photos Apres");


        TabHost.TabSpec spec5=tabHost.newTabSpec("Rapport");
        spec5.setContent(R.id.tab5);
        spec5.setIndicator("Rapport");

        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);
        tabHost.addTab(spec4);
        tabHost.addTab(spec5);
        editadresse= (EditText) findViewById(R.id.editadresse);
        eroradresse= (TextView) findViewById(R.id.eroradresse);

        erorequipe= (TextView) findViewById(R.id.erorequipe);
        eroroperateur= (TextView) findViewById(R.id.eroroperateur);

        erordateoperation= (TextView) findViewById(R.id.erordateoperation);

        editerdateoperation= (Button) findViewById(R.id.buttoneditdateoperateur);
        dateoperation= (TextView) findViewById(R.id.dateoperation);
        antecedent= (EditText) findViewById(R.id.editAntecedent);
        compterendu= (EditText) findViewById(R.id.editcompterendu);
        errorcompterendu= (TextView) findViewById(R.id.erorcompterendu);
        errorantecedent= (TextView) findViewById(R.id.erorantecedent);





        editnom= (EditText) findViewById(R.id.editNom);
        editprenom= (EditText) findViewById(R.id.editPrenom);
        editmail= (EditText) findViewById(R.id.editMail);
        edittel= (EditText) findViewById(R.id.editTel);

        errornom= (TextView) findViewById(R.id.erorNom);
        errorprenom= (TextView) findViewById(R.id.erorPrenom);
        errormail= (TextView) findViewById(R.id.erorMail);
        errortel= (TextView) findViewById(R.id.erorTel);
        errorage= (TextView) findViewById(R.id.erorAge);
        age= (EditText) findViewById(R.id.Age);

        femme= (RadioButton) findViewById(R.id.radioFemale);
        homme= (RadioButton) findViewById(R.id.radioMale);




        editdiagnostic= (EditText) findViewById(R.id.editDiagnostic);
        errordiagnostic=(TextView)findViewById(R.id.erordiagnostic);

        visage=(CheckBox)findViewById(R.id.visage);
        Lifting_cervico_facial=(CheckBox)findViewById(R.id.visagelifting);
        Peeling=(CheckBox)findViewById(R.id.visagepeeling);
        Blepharoplastie=(CheckBox)findViewById(R.id.visageBlepharoplastie);
        Lifting_temporal=(CheckBox)findViewById(R.id.visageLiftingtemporal);
        Lifting_complet =(CheckBox)findViewById(R.id.visageLiftingcomplet);
        Genioplastie=(CheckBox)findViewById(R.id.visageGenioplastie);
        Otoplastie=(CheckBox)findViewById(R.id.visageOtoplastie);
        autrevisage=(CheckBox)findViewById(R.id.visageautre);
        seins=(CheckBox)findViewById(R.id.seins);
        Augmentation_mammaire =(CheckBox)findViewById(R.id.seinsAugmentationmammaire);
        Lipofilling_des_seins=(CheckBox)findViewById(R.id.seinsLipofillingdesseins);
        Reduction_mammaire=(CheckBox)findViewById(R.id.seinsReductionmammaire);
        Changement_de_protheses=(CheckBox)findViewById(R.id.seinsChangementdeprotheses);
        Lifting_des_seins=(CheckBox)findViewById(R.id.seinsLiftingdesseins);
        Gynecomastie=(CheckBox)findViewById(R.id.seinsGynecomastie);
        autreseins=(CheckBox)findViewById(R.id.autreseins);
        corp=(CheckBox)findViewById(R.id.corp);
        Liposuccion=(CheckBox)findViewById(R.id.CorpLiposuccion);
        Abdominoplastie=(CheckBox)findViewById(R.id.CorpAbdominoplastie);
        Lifting_des_cuisses=(CheckBox)findViewById(R.id.CorpLiftingdescuisses);
        Augmentation_fesses =(CheckBox)findViewById(R.id.CorpAugmentationfesses);
        autrecorp=(CheckBox)findViewById(R.id.corpautre);
        autre=(CheckBox)findViewById(R.id.autre0);

        autreoperation=(EditText)findViewById(R.id.autreOperation);
        visageautre=(EditText)findViewById(R.id.visageautreOperation);
        corpautre=(EditText)findViewById(R.id.autreCorp);
        seinsautre=(EditText)findViewById(R.id.seinsautreOperation);

        radiooperateurtaher= (RadioButton) findViewById(R.id.radiooperateurtaher);
        radiooperateueautre= (RadioButton) findViewById(R.id.radiooperateurautre);
        autreoperateur= (EditText) findViewById(R.id.autreOperateur);

                radiooperateurtaher =(RadioButton) findViewById(R.id.radioOperateurDrtaher);
                radiooperateueautre =(RadioButton) findViewById(R.id.radioOperateurAutre);
                        autreoperateur=(EditText) findViewById(R.id.editautreoperateur);


        radiooperateurtaher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radiooperateurtaher.setChecked(true);
                radiooperateueautre.setChecked(false);
                autreoperateur.setVisibility(View.GONE);
            }
        });
        radiooperateueautre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radiooperateurtaher.setChecked(false);
                radiooperateueautre.setChecked(true);
                autreoperateur.setVisibility(View.VISIBLE);
            }
        });


        radioequipetaher= (RadioButton) findViewById(R.id.radioequipeDrtaher);
        radioequipeautre= (RadioButton) findViewById(R.id.radioequipeAutre);
        autreequipe= (EditText) findViewById(R.id.editautreequipe);


    radioequipetaher.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            radioequipetaher.setChecked(true);
            radioequipeautre.setChecked(false);
            autreequipe.setVisibility(View.GONE);
        }
    });
        radioequipeautre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioequipetaher.setChecked(false);
                radioequipeautre.setChecked(true);
                autreequipe.setVisibility(View.VISIBLE);
            }
        });


        if(!ancienvisage){
            visage.setChecked(false);
            Lifting_cervico_facial.setChecked(false);
            Peeling.setChecked(false);
            Blepharoplastie.setChecked(false);
            Lifting_temporal.setChecked(false);
            Lifting_complet.setChecked(false);
            Genioplastie.setChecked(false);
            Otoplastie.setChecked(false);
            autrevisage.setChecked(false);
            visageautre.setText("");
        }else{
            visage.setChecked(true);
            if(ancienLifting_cervico_facial){
                Lifting_cervico_facial.setChecked(true);
            }else{
                Lifting_cervico_facial.setChecked(false);

            }
            if(ancienPeeling){
                Peeling.setChecked(true);
            }else{
                Peeling.setChecked(false);

            }
            if(ancienBlepharoplastie){
                Blepharoplastie.setChecked(true);
            }else {
                Blepharoplastie.setChecked(false);

            }
            if(ancienLifting_temporal){
                Lifting_temporal.setChecked(true);
            }else{
                Lifting_temporal.setChecked(false);
            }
            if(ancienLifting_complet){
                Lifting_complet.setChecked(true);
            }else{
                Lifting_complet.setChecked(false);
            }
            if(ancienGenioplastie){
                Genioplastie.setChecked(true);
            }else{
                Genioplastie.setChecked(false);

            }
            if(ancienOtoplastie){
                Otoplastie.setChecked(true);
            }else{
                Otoplastie.setChecked(false);
            }
            if(!ancienautrevisage.equals("")){
                autrevisage.setChecked(true);
                visageautre.setText(ancienautrevisage);
                visageautre.setVisibility(View.VISIBLE);

            }else{

                autrevisage.setChecked(false);
                visageautre.setVisibility(View.GONE);

            }

        }
        if(!ancienseins){

            seins.setChecked(false);
            Augmentation_mammaire .setChecked(false);
            Lipofilling_des_seins.setChecked(false);
            Reduction_mammaire.setChecked(false);
            Changement_de_protheses.setChecked(false);
            Lifting_des_seins.setChecked(false);
            Gynecomastie.setChecked(false);
            autreseins.setChecked(false);
            seinsautre.setText("");


        }else{
            seins.setChecked(true);
            if(ancienAugmentation_mammaire){
                Augmentation_mammaire.setChecked(true);
            }else{
                Augmentation_mammaire.setChecked(false);
            }
            if(ancienLipofilling_des_seins){
                Lipofilling_des_seins.setChecked(true);
            }else{
                Lipofilling_des_seins.setChecked(false);
            }
            if(ancienReduction_mammaire){
                Reduction_mammaire.setChecked(true);
            }else{
                Reduction_mammaire.setChecked(false);
            }
            if(ancienChangement_de_protheses){
                Changement_de_protheses.setChecked(true);
            }else{
                Changement_de_protheses.setChecked(false);
            }
            if(ancienLifting_des_seins){
                Lifting_des_seins.setChecked(true);
            }else{
                Lifting_des_seins.setChecked(false);
            }
            if(ancienGynecomastie){
                Gynecomastie.setChecked(true);
            }else{
                Gynecomastie.setChecked(false);
            }
            if(!ancienautreseins.equals("")){
                autreseins.setChecked(true);
                seinsautre.setText(ancienautreseins);
                seinsautre.setVisibility(View.VISIBLE);

            }else{

                autreseins.setChecked(false);
                seinsautre.setVisibility(View.GONE);

            }
        }
        if(!anciencorp){

            corp.setChecked(false);
            Liposuccion.setChecked(false);
            Abdominoplastie.setChecked(false);
            Lifting_des_cuisses.setChecked(false);
            Augmentation_fesses.setChecked(false);
            autrecorp.setChecked(false);
            corpautre.setText("");

        }else{
            corp.setChecked(true);
            if(ancienLiposuccion){
                Liposuccion.setChecked(true);
            }else{
                Liposuccion.setChecked(false);
            }
            if(ancienAbdominoplastie){
                Abdominoplastie.setChecked(true);
            }else{
                Abdominoplastie.setChecked(false);
            }
            if(ancienLifting_des_cuisses){
                Lifting_des_cuisses.setChecked(true);
            }else{
                Lifting_des_cuisses.setChecked(false);
            }
            if(ancienAugmentation_fesses){
                Augmentation_fesses.setChecked(true);
            }else{
                Augmentation_fesses.setChecked(false);
            }
            if(!ancienautrecorp.equals("")){
                autrecorp.setChecked(true);
                corpautre.setText(ancienautrecorp);
                corpautre.setVisibility(View.VISIBLE);

            }else{

                autrecorp.setChecked(false);
                corpautre.setVisibility(View.GONE);

            }


        }
        if(!ancienautre.equals("")){
            autre.setChecked(true);
            autreoperation.setText(ancienautre);
            autreoperation.setVisibility(View.VISIBLE);
        }else{
            autre.setChecked(false);
            autreoperation.setVisibility(View.GONE);

        }
        if(!ancienvisage){
            Lifting_cervico_facial.setVisibility(View.GONE);
            Peeling.setVisibility(View.GONE);
            Blepharoplastie.setVisibility(View.GONE);
            Lifting_temporal.setVisibility(View.GONE);
            Lifting_complet.setVisibility(View.GONE);
            Genioplastie.setVisibility(View.GONE);
            Otoplastie.setVisibility(View.GONE);
            autrevisage.setVisibility(View.GONE);
            visageautre.setVisibility(View.GONE);

        }
        if(!ancienseins){
            Augmentation_mammaire.setVisibility(View.GONE);
            Lipofilling_des_seins.setVisibility(View.GONE);
            Reduction_mammaire.setVisibility(View.GONE);
            Changement_de_protheses.setVisibility(View.GONE);
            Lifting_des_seins.setVisibility(View.GONE);
            Gynecomastie.setVisibility(View.GONE);
            autreseins.setVisibility(View.GONE);
            seinsautre.setVisibility(View.GONE);
        }
        if(!anciencorp){
            Liposuccion.setVisibility(View.GONE);
            Abdominoplastie.setVisibility(View.GONE);
            Lifting_des_cuisses.setVisibility(View.GONE);
            Augmentation_fesses .setVisibility(View.GONE);
            autrecorp.setVisibility(View.GONE);
            corpautre.setVisibility(View.GONE);
        }


        if(anciensexe.equals("homme")){
            homme.setChecked(true);
            femme.setChecked(false);
        }else{
            femme.setChecked(true);
            homme.setChecked(false);
        }




        femme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                femme.setChecked(true);
                homme.setChecked(false);
                sexe = "femme";
            }
        });
        homme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homme.setChecked(true);
                femme.setChecked(false);
                sexe = "homme";
            }
        });
        num_adminission.setText(ancienNum_admission);
        age.setText(ancienage);
        editnom.setText(anciennom);
        editprenom.setText(ancienprenom);
        editmail.setText(ancienmail);
        edittel.setText(ancientel);
        editdiagnostic.setText(anciendiag);
        antecedent.setText(ancienantecedent);
        compterendu.setText(anciencompterendu);
        dateoperation.setText(anciendateoperation);
        editadresse.setText(ancienadresse);
        if(ancienoperateur.equals("drtaher")){
            radiooperateurtaher.setChecked(true);
            radiooperateueautre.setChecked(false);
        }else{

            radiooperateurtaher.setChecked(false);
            radiooperateueautre.setChecked(true);
            autreoperateur.setEnabled(false);
            autreoperateur.setText(ancienoperateur);

        }
        if(ancienequipe.equals("drtaher")){
            radioequipetaher.setChecked(true);
            radioequipeautre.setChecked(false);
        }else{

            radioequipetaher.setChecked(false);
            radioequipeautre.setChecked(true);
            autreequipe.setEnabled(false);
            autreequipe.setText(ancienequipe);

        }

        errornom.setText("");
        errorprenom.setText("");
        errorage.setText("");
        errortel.setText("");
        errormail.setText("");
        errordiagnostic.setText("");
        errorcompterendu.setText("");
        errorantecedent.setText("");
        erordateoperation.setText("");
        erorequipe.setText("");
        eroroperateur.setText("");
        eroradresse.setText("");
        errorNum_admission.setText("");
        errordetailsoperation.setText("");
        errordetailsoperationcorp.setText("");
        errordetailsoperationseins.setText("");
        errordetailsoperationvisage.setText("");
        errordetailsoperationautre.setText("");

        errordetailsoperation.setVisibility(View.GONE);
        errordetailsoperationcorp.setVisibility(View.GONE);
        errordetailsoperationseins.setVisibility(View.GONE);
        errordetailsoperationvisage.setVisibility(View.GONE);
        errordetailsoperationautre.setVisibility(View.GONE);

        if(anciensexe.equals("femme")){
            femme.setChecked(true);
            homme.setChecked(false);
        }else{
            homme.setChecked(true);
            femme.setChecked(false);

        }





        editerdateoperation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DIALOG_DATE_OPERATION);
            }
        });




        autreoperateur.setVisibility(View.GONE);





        if(modif){
            avant1.setClickable(false);
            avant2.setClickable(false);
            avant3.setClickable(false);
            avant4.setClickable(false);
            avant5.setClickable(false);

            avant11.setClickable(false);
            avant21.setClickable(false);
            avant31.setClickable(false);
            avant41.setClickable(false);
            avant51.setClickable(false);

            ajoutertofavant.setVisibility(View.GONE);
            ajoutertofavant1.setVisibility(View.GONE);

            editnote1.setEnabled(false);
            editnote2.setEnabled(false);
            editnote3.setEnabled(false);
            editnote4.setEnabled(false);
            editnote5.setEnabled(false);

            editnote11.setEnabled(false);
            editnote21.setEnabled(false);
            editnote31.setEnabled(false);
            editnote41.setEnabled(false);
            editnote51.setEnabled(false);

            this.setTitle("Details operation");
            num_adminission.setEnabled(false);

            editnom.setEnabled(false);
            editprenom.setEnabled(false);
            editmail.setEnabled(false);
            edittel.setEnabled(false);

            femme.setEnabled(false);
            homme.setEnabled(false);

            age.setEnabled(false);
            editdiagnostic.setEnabled(false);
            antecedent.setEnabled(false);
            compterendu.setEnabled(false);
            editerdateoperation.setVisibility(View.GONE);
            radiooperateurtaher.setEnabled(false);
            radiooperateueautre.setEnabled(false);
            radioequipetaher.setEnabled(false);
            radioequipeautre.setEnabled(false);
            autreoperateur.setEnabled(false);
            autreequipe.setEnabled(false);

            editadresse.setEnabled(false);

            visage.setEnabled(false);
            Lifting_cervico_facial.setEnabled(false);
            Peeling.setEnabled(false);
            Blepharoplastie.setEnabled(false);
            Lifting_temporal.setEnabled(false);
            Lifting_complet.setEnabled(false);
            Genioplastie.setEnabled(false);
            Otoplastie.setEnabled(false);
            autrevisage.setEnabled(false);
            visageautre.setEnabled(false);

            seins.setEnabled(false);
            Augmentation_mammaire .setEnabled(false);
            Lipofilling_des_seins.setEnabled(false);
            Reduction_mammaire.setEnabled(false);
            Changement_de_protheses.setEnabled(false);
            Lifting_des_seins.setEnabled(false);
            Gynecomastie.setEnabled(false);
            autreseins.setEnabled(false);
            seinsautre.setEnabled(false);


            corp.setEnabled(false);
            Liposuccion.setEnabled(false);
            Abdominoplastie.setEnabled(false);
            Lifting_des_cuisses.setEnabled(false);
            Augmentation_fesses.setEnabled(false);
            autrecorp.setEnabled(false);
            corpautre.setEnabled(false);

            autre.setEnabled(false);

            autreoperation.setEnabled(false);




        }else{
            this.setTitle("modification operation");

            num_adminission.setEnabled(true);


            avant1.setClickable(true);
            avant2.setClickable(true);
            avant3.setClickable(true);
            avant4.setClickable(true);
            avant5.setClickable(true);

            avant11.setClickable(true);
            avant21.setClickable(true);
            avant31.setClickable(true);
            avant41.setClickable(true);
            avant51.setClickable(true);

            ajoutertofavant.setVisibility(View.VISIBLE);
            ajoutertofavant1.setVisibility(View.VISIBLE);

            editnote1.setEnabled(true);
            editnote2.setEnabled(true);
            editnote3.setEnabled(true);
            editnote4.setEnabled(true);
            editnote5.setEnabled(true);

            editnote11.setEnabled(true);
            editnote21.setEnabled(true);
            editnote31.setEnabled(true);
            editnote41.setEnabled(true);
            editnote51.setEnabled(true);

            editnom.setEnabled(true);
            editprenom.setEnabled(true);
            editmail.setEnabled(true);
            edittel.setEnabled(true);

            femme.setClickable(true);
            homme.setClickable(true);

            age.setEnabled(true);



            editdiagnostic.setEnabled(true);

            antecedent.setEnabled(true);
            compterendu.setEnabled(true);
            editerdateoperation.setVisibility(View.VISIBLE);
            radiooperateurtaher.setEnabled(true);
            radiooperateueautre.setEnabled(true);
            radioequipetaher.setEnabled(true);
            radioequipeautre.setEnabled(true);
            autreoperateur.setEnabled(true);
            autreequipe.setEnabled(true);
            editadresse.setEnabled(true);

            visage.setEnabled(true);
            Lifting_cervico_facial.setEnabled(true);
            Peeling.setEnabled(true);
            Blepharoplastie.setEnabled(true);
            Lifting_temporal.setEnabled(true);
            Lifting_complet.setEnabled(true);
            Genioplastie.setEnabled(true);
            Otoplastie.setEnabled(true);
            autrevisage.setEnabled(true);
            visageautre.setEnabled(true);

            seins.setEnabled(true);
            Augmentation_mammaire .setEnabled(true);
            Lipofilling_des_seins.setEnabled(true);
            Reduction_mammaire.setEnabled(true);
            Changement_de_protheses.setEnabled(true);
            Lifting_des_seins.setEnabled(true);
            Gynecomastie.setEnabled(true);
            autreseins.setEnabled(true);
            seinsautre.setEnabled(true);


            corp.setEnabled(true);
            Liposuccion.setEnabled(true);
            Abdominoplastie.setEnabled(true);
            Lifting_des_cuisses.setEnabled(true);
            Augmentation_fesses.setEnabled(true);
            autrecorp.setEnabled(true);
            corpautre.setEnabled(true);

            autre.setEnabled(true);

            autreoperation.setEnabled(true);
        }

        if(ancienautrevisage.equals("")) {
            visageautre.setVisibility(View.GONE);
        }


        if(ancienautreseins.equals("")) {
            seinsautre.setVisibility(View.GONE);
        }

       if(ancienautrecorp.equals("")) {
           corpautre.setVisibility(View.GONE);
       }
        if(ancienautre.equals("")) {
            autreoperation.setVisibility(View.GONE);
        }

        if(Liposuccion.isChecked()){
            mayout_liposuccion.setVisibility(View.VISIBLE);
        }else{
            mayout_liposuccion.setVisibility(View.GONE);
        }
        Liposuccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Liposuccion.isChecked()){
                    mayout_liposuccion.setVisibility(View.VISIBLE);
                }else{
                    mayout_liposuccion.setVisibility(View.GONE);
                }
            }
        });
        if(Abdominoplastie.isChecked()){
            mayout_plastie_abdominal.setVisibility(View.VISIBLE);
        }else{
            mayout_plastie_abdominal.setVisibility(View.GONE);
        }
        Abdominoplastie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Abdominoplastie.isChecked()){
                    mayout_plastie_abdominal.setVisibility(View.VISIBLE);
                }else{
                    mayout_plastie_abdominal.setVisibility(View.GONE);
                }
            }
        });
        if(Augmentation_mammaire.isChecked()){
            layout_prothese_mammaire.setVisibility(View.VISIBLE);
        }else{
            layout_prothese_mammaire.setVisibility(View.GONE);
        }
        Augmentation_mammaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Augmentation_mammaire.isChecked()){
                    layout_prothese_mammaire.setVisibility(View.VISIBLE);
                }else{
                    layout_prothese_mammaire.setVisibility(View.GONE);
                }
            }
        });
        if(Reduction_mammaire.isChecked()){
            layout_plastie_mamaire.setVisibility(View.VISIBLE);
        }else{
            layout_plastie_mamaire.setVisibility(View.GONE);
        }
        Reduction_mammaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Reduction_mammaire.isChecked()){
                    layout_plastie_mamaire.setVisibility(View.VISIBLE);
                }else{
                    layout_plastie_mamaire.setVisibility(View.GONE);
                }
            }
        });
        if(Lifting_des_cuisses.isChecked()){
            layout_lifting_cuisse1.setVisibility(View.VISIBLE);
            layout_lifting_cuisse2.setVisibility(View.VISIBLE);
        }else{
            layout_lifting_cuisse1.setVisibility(View.GONE);
            layout_lifting_cuisse2.setVisibility(View.GONE);
        }
        Lifting_des_cuisses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Lifting_des_cuisses.isChecked()){
                    layout_lifting_cuisse1.setVisibility(View.VISIBLE);
                    layout_lifting_cuisse2.setVisibility(View.VISIBLE);
                }else{
                    layout_lifting_cuisse1.setVisibility(View.GONE);
                    layout_lifting_cuisse2.setVisibility(View.GONE);
                }
            }
        });
        if(Blepharoplastie.isChecked()){
            layout_Blepharoplastie.setVisibility(View.VISIBLE);

        }else{
            layout_Blepharoplastie.setVisibility(View.GONE);

        }
        Blepharoplastie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Blepharoplastie.isChecked()){
                    layout_Blepharoplastie.setVisibility(View.VISIBLE);

                }else{
                    layout_Blepharoplastie.setVisibility(View.GONE);

                }
            }
        });
        if(Lifting_cervico_facial.isChecked()){
            layout_lifting_cervico_facial.setVisibility(View.VISIBLE);

        }else{
            layout_lifting_cervico_facial.setVisibility(View.GONE);

        }
        Lifting_cervico_facial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Lifting_cervico_facial.isChecked()) {
                    layout_lifting_cervico_facial.setVisibility(View.VISIBLE);

                } else {
                    layout_lifting_cervico_facial.setVisibility(View.GONE);

                }
            }
        });



        visage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (visage.isChecked()) {
                    Lifting_cervico_facial.setVisibility(View.VISIBLE);
                    Peeling.setVisibility(View.VISIBLE);
                    Blepharoplastie.setVisibility(View.VISIBLE);
                    Lifting_temporal.setVisibility(View.VISIBLE);
                    Lifting_complet.setVisibility(View.VISIBLE);
                    Genioplastie.setVisibility(View.VISIBLE);
                    Otoplastie.setVisibility(View.VISIBLE);
                    autrevisage.setVisibility(View.VISIBLE);
                    if(autrevisage.isChecked()) {
                        visageautre.setVisibility(View.VISIBLE);
                    }else{
                        visageautre.setVisibility(View.GONE);
                    }


                } else {
                    Lifting_cervico_facial.setVisibility(View.GONE);
                    Peeling.setVisibility(View.GONE);
                    Blepharoplastie.setVisibility(View.GONE);
                    Lifting_temporal.setVisibility(View.GONE);
                    Lifting_complet.setVisibility(View.GONE);
                    Genioplastie.setVisibility(View.GONE);
                    Otoplastie.setVisibility(View.GONE);
                    autrevisage.setVisibility(View.GONE);
                    visageautre.setVisibility(View.GONE);

                }

            }
        });

        seins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (seins.isChecked()) {
                    Augmentation_mammaire.setVisibility(View.VISIBLE);
                    Lipofilling_des_seins.setVisibility(View.VISIBLE);
                    Reduction_mammaire.setVisibility(View.VISIBLE);
                    Changement_de_protheses.setVisibility(View.VISIBLE);
                    Lifting_des_seins.setVisibility(View.VISIBLE);
                    Gynecomastie.setVisibility(View.VISIBLE);
                    autreseins.setVisibility(View.VISIBLE);
                    if(autreseins.isChecked()) {
                        seinsautre.setVisibility(View.VISIBLE);
                    }else{
                        seinsautre.setVisibility(View.GONE);
                    }



                } else {
                    Augmentation_mammaire.setVisibility(View.GONE);
                    Lipofilling_des_seins.setVisibility(View.GONE);
                    Reduction_mammaire.setVisibility(View.GONE);
                    Changement_de_protheses.setVisibility(View.GONE);
                    Lifting_des_seins.setVisibility(View.GONE);
                    Gynecomastie.setVisibility(View.GONE);
                    autreseins.setVisibility(View.GONE);
                    seinsautre.setVisibility(View.GONE);

                }

            }
        });

        corp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (corp.isChecked()) {

                    Liposuccion.setVisibility(View.VISIBLE);
                    Abdominoplastie.setVisibility(View.VISIBLE);
                    Lifting_des_cuisses.setVisibility(View.VISIBLE);
                    Augmentation_fesses.setVisibility(View.VISIBLE);
                    autrecorp.setVisibility(View.VISIBLE);
                    if(autrecorp.isChecked()) {
                        corpautre.setVisibility(View.VISIBLE);
                    }else{
                       corpautre.setVisibility(View.GONE);
                    }



                } else {

                    Liposuccion.setVisibility(View.GONE);
                    Abdominoplastie.setVisibility(View.GONE);
                    Lifting_des_cuisses.setVisibility(View.GONE);
                    Augmentation_fesses.setVisibility(View.GONE);
                    autrecorp.setVisibility(View.GONE);
                    corpautre.setVisibility(View.GONE);

                }

            }
        });
        autre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autre.isChecked()) {
                    autreoperation.setVisibility(View.VISIBLE);

                } else {
                    autreoperation.setText("");
                    autreoperation.setVisibility(View.GONE);
                }

            }
        });



        autrevisage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(autrevisage.isChecked()) {
                    visageautre.setVisibility(View.VISIBLE);

                }else{

                    visageautre.setVisibility(View.GONE);
                }

            }
        });

        autrecorp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(autrecorp.isChecked()) {
                    corpautre.setVisibility(View.VISIBLE);

                }else{

                    corpautre.setVisibility(View.GONE);
                }

            }
        });

        autreseins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(autreseins.isChecked()) {
                    seinsautre.setVisibility(View.VISIBLE);

                }else{

                    seinsautre.setVisibility(View.GONE);
                }

            }
        });

        if(radioequipeautre.isChecked()){
            autreequipe.setVisibility(View.VISIBLE);
        }
        if(radiooperateueautre.isChecked()){
            autreoperateur.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(modif) {
            getMenuInflater().inflate(R.menu.menu_detail_operation, menu);
            return true;
        }else{
            getMenuInflater().inflate(R.menu.menu_detail_operation2, menu);
            return true;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.modifoperation || id == R.id.modifoperation1){

            /*Intent intent = new Intent(detailOperation.this,
                    Authentification.class);

            startActivity(intent);*/



             Toast.makeText(detailOperation.this, "modifier", Toast.LENGTH_LONG).show();
            Intent inten=new Intent(detailOperation.this,detailOperation.class);
            inten.putExtra("modif",false);

            inten.putExtra("nom",editnom.getText().toString());
            inten.putExtra("prenom",editprenom.getText().toString());
            inten.putExtra("mail",editmail.getText().toString());
            inten.putExtra("tel", edittel.getText().toString());
            inten.putExtra("age", age.getText().toString());
            inten.putExtra("diag", editdiagnostic.getText().toString());
            inten.putExtra("antecedent", antecedent.getText().toString());
            inten.putExtra("compterendu", compterendu.getText().toString());
            inten.putExtra("dateoperation", dateoperation.getText().toString());
            inten.putExtra("admission",num_adminission.getText().toString());
            inten.putExtra("adresse",editadresse.getText().toString());
            if(femme.isChecked()){
                inten.putExtra("sexe","femme");

            }else{

                inten.putExtra("sexe","homme");
            }
            if(radiooperateurtaher.isChecked()){

                inten.putExtra("operateur", "drtaher");

            }else{

                inten.putExtra("operateur", autreoperateur.getText().toString());

            }
            if(radioequipetaher.isChecked()){

                inten.putExtra("equipe", "drtaher");

            }else{

                inten.putExtra("equipe", autreequipe.getText().toString());

            }
            if(!visage.isChecked()){

                inten.putExtra("visage",false);
                inten.putExtra("lifting_cervico_facial",false);
                inten.putExtra("peeling",false);
                inten.putExtra("blepharoplastie",false);
                inten.putExtra("lifting_temporal",false);
                inten.putExtra("lifting_complet",false);
                inten.putExtra("genioplastie",false);
                inten.putExtra("otoplastie",false);
                inten.putExtra("autrevisage","");

            }else{
                inten.putExtra("visage", true);

                if(Lifting_cervico_facial.isChecked()){
                    inten.putExtra("lifting_cervico_facial",true);
                }else{
                    inten.putExtra("lifting_cervico_facial",false);
                }
                if(Peeling.isChecked()){
                    inten.putExtra("peeling",true);
                }else{
                    inten.putExtra("peeling",false);
                }
                if(Blepharoplastie.isChecked()){
                    inten.putExtra("blepharoplastie",true);
                }else{
                    inten.putExtra("blepharoplastie",false);
                }
                if(Lifting_temporal.isChecked()){
                    inten.putExtra("lifting_temporal",true);
                }else{
                    inten.putExtra("lifting_temporal",false);
                }
                if(Lifting_complet.isChecked()){
                    inten.putExtra("lifting_complet",true);
                }else{
                    inten.putExtra("lifting_complet",false);
                }
                if(Genioplastie.isChecked()){
                    inten.putExtra("genioplastie",true);
                }else{
                    inten.putExtra("genioplastie",false);
                }
                if(Genioplastie.isChecked()){
                    inten.putExtra("otoplastie",true);
                }else{
                    inten.putExtra("otoplastie",false);
                }
                if(autrevisage.isChecked()){
                    inten.putExtra("autrevisage",visageautre.getText().toString());
                }else{
                    inten.putExtra("autrevisage","");
                }

            }
            if(!seins.isChecked()){
                inten.putExtra("seins",false);
                inten.putExtra("augmentation_mammaire",false);
                inten.putExtra("lipofilling_des_seins",false);
                inten.putExtra("reduction_mammaire(",false);
                inten.putExtra("changement_de_protheses",false);
                inten.putExtra("lifting_des_seins",false);
                inten.putExtra("gynecomastie",false);
                inten.putExtra("autreseins","");
            }else{
                inten.putExtra("seins",true);
                if(Augmentation_mammaire.isChecked()){
                    inten.putExtra("augmentation_mammaire",true);
                }else{
                    inten.putExtra("augmentation_mammaire",false);
                }
                if(Lipofilling_des_seins.isChecked()){
                    inten.putExtra("lipofilling_des_seins",true);
                }else{
                    inten.putExtra("lipofilling_des_seins",false);
                }
                if(Reduction_mammaire.isChecked()){
                    inten.putExtra("reduction_mammaire(",true);
                }else{
                    inten.putExtra("reduction_mammaire(",false);
                }
                if(Changement_de_protheses.isChecked()){
                    inten.putExtra("changement_de_protheses",true);
                }else{
                    inten.putExtra("changement_de_protheses",false);
                }
                if(Lifting_des_seins.isChecked()){
                    inten.putExtra("lifting_des_seins",true);
                }else{
                    inten.putExtra("lifting_des_seins",false);
                }
                if(Gynecomastie.isChecked()){
                    inten.putExtra("gynecomastie",true);
                }else{
                    inten.putExtra("gynecomastie",false);
                }
                if(autreseins.isChecked()){
                    inten.putExtra("autreseins",seinsautre.getText().toString());
                }else{
                    inten.putExtra("autreseins",seinsautre.getText().toString());
                }

            }
            if(!corp.isChecked()){
                inten.putExtra("corp",false);
                inten.putExtra("liposuccion",false);
                inten.putExtra("abdominoplastie",false);
                inten.putExtra("lifting_des_cuisses",false);
                inten.putExtra("Augmentation_fesse", false);
                inten.putExtra("autrecorp","");
            }else{
                inten.putExtra("corp", true);
                if(Liposuccion.isChecked()){
                    inten.putExtra("liposuccion",true);
                }else{
                    inten.putExtra("liposuccion",false);
                }
                if(Abdominoplastie.isChecked()){
                    inten.putExtra("abdominoplastie",true);
                }else{
                    inten.putExtra("abdominoplastie",false);
                }
                if(Lifting_des_cuisses.isChecked()){
                    inten.putExtra("lifting_des_cuisses",true);
                }else{
                    inten.putExtra("lifting_des_cuisses",false);
                }
                if(Augmentation_fesses.isChecked()){
                    inten.putExtra("Augmentation_fesse", true);
                }else{
                    inten.putExtra("Augmentation_fesse", false);
                }
                if(autrecorp.isChecked()){
                    inten.putExtra("autrecorp",corpautre.getText().toString());
                }else{
                    inten.putExtra("autrecorp","");

                }

            }
            if(!autre.isChecked()){
                inten.putExtra("autre", "");

            } else {
                inten.putExtra("autre", autreoperation.getText().toString());

            }
            inten.putExtra("id",ancienId);



                inten.putExtra("avant1", ancienavant1);
                inten.putExtra("descavant1", editnote1.getText().toString());


                inten.putExtra("avant2", ancienavant2);
                inten.putExtra("descavant2", editnote2.getText().toString());



                inten.putExtra("avant3", ancienavant3);
                inten.putExtra("descavant3", editnote3.getText().toString());


                inten.putExtra("avant4", ancienavant4);
                inten.putExtra("descavant4", editnote4.getText().toString());

                inten.putExtra("avant5", ancienavant5);
                inten.putExtra("descavant5", editnote5.getText().toString());


                inten.putExtra("apres1", ancienapres1);
                inten.putExtra("descapres1", editnote11.getText().toString());

                inten.putExtra("apres2", ancienapres2);
                inten.putExtra("descapres2", editnote21.getText().toString());

                inten.putExtra("apres3", ancienapres3);
                inten.putExtra("descapres3", editnote31.getText().toString());

                inten.putExtra("apres4", ancienapres4);
                inten.putExtra("descapres4", editnote41.getText().toString());

                inten.putExtra("apres5", ancienapres5);
                inten.putExtra("descapres5", editnote51.getText().toString());


            inten.putExtra("isavant1",isavant1);
            inten.putExtra("isavant2",isavant2);
            inten.putExtra("isavant3",isavant3);
            inten.putExtra("isavant4",isavant4);
            inten.putExtra("isavant5",isavant5);

            inten.putExtra("isapres1",isapres1);
            inten.putExtra("isapres2",isapres2);
            inten.putExtra("isapres3",isapres3);
            inten.putExtra("isapres4",isapres4);
            inten.putExtra("isapres5",isapres5);





            startActivity(inten);




        }
        if(id == R.id.confirmermodifoperation || id == R.id.confirmermodifoperation1){
            if(editnom.getText().toString().equals("")){
                errornom.setText("le nom est vide");
            }else{
                errornom.setText("");

            }
            if(editprenom.getText().toString().equals("")){
                errorprenom.setText("le prenom est vide");
            }else{
                errorprenom.setText("");

            }
            if(editmail.getText().toString().equals("")){
                errormail.setText("le mail est vide");
            }else{
                errormail.setText("");

            }
            if(edittel.getText().toString().equals("")){
                errortel.setText("le telephone est vide");
            }else{
                errortel.setText("");

            }
            if(age.getText().toString().equals("")){
                errorage.setText("le date de naissance  n'est pas saisie");
            }else{
                errorage.setText("");

            }
            if(editdiagnostic.getText().toString().equals("")){
                errordiagnostic.setText("le diagnostic est vide");
            }else{
                errordiagnostic.setText("");
            }

            if(antecedent.getText().toString().equals("")){
                errorantecedent.setText("l'antecedent est vide");
            }else{
                errorantecedent.setText("");
            }

            if(compterendu.getText().toString().equals("")){
                errorcompterendu.setText("le compte rendu est vide");
            }else{
                errorcompterendu.setText("");
            }
            if(dateoperation.getText().toString().equals("")){
                erordateoperation.setText("la date de l'operation est vide");
            }else{
                erordateoperation.setText("");
            }
            if(radioequipeautre.isChecked()&&autreequipe.getText().toString().equals("")){
                erorequipe.setText("l'autre equipe n'est pas saisie");
            }else{

                erorequipe.setText("");

            }
            if(radiooperateueautre.isChecked()&&autreoperateur.getText().toString().equals("")){
                eroroperateur.setText("l'autre operateur n'est pas saisie");
            }else{

                eroroperateur.setText("");

            }
            if(editadresse.getText().toString().equals("")){
                eroradresse.setText("l'adresse n'est pas saisie");
            }else{
                eroradresse.setText("");
            }
            if(num_adminission.getText().toString().equals("")){
                errorNum_admission.setText("le numero d'admission n'est pas saisie");
            }else{
                errorNum_admission.setText("");
            }
            if(!visage.isChecked()&&!corp.isChecked()&&!seins.isChecked()&&!autre.isChecked()){
                errordetailsoperationvisage.setVisibility(View.GONE);
                errordetailsoperationvisage.setText("");
                errordetailsoperationseins.setVisibility(View.GONE);
                errordetailsoperationseins.setText("");
                errordetailsoperationcorp.setVisibility(View.GONE);
                errordetailsoperationcorp.setText("");
                errordetailsoperationautre.setVisibility(View.GONE);
                errordetailsoperationautre.setText("");
                errordetailsoperation.setVisibility(View.VISIBLE);
                errordetailsoperation.setText("le detail operation n'est pas specifier");
            }else{
                errordetailsoperation.setVisibility(View.GONE);
                errordetailsoperation.setText("");
                errordetailsoperationvisage.setVisibility(View.GONE);
                errordetailsoperationvisage.setText("");
                errordetailsoperationseins.setVisibility(View.GONE);
                errordetailsoperationseins.setText("");
                errordetailsoperationcorp.setVisibility(View.GONE);
                errordetailsoperationcorp.setText("");
                errordetailsoperationautre.setVisibility(View.GONE);
                errordetailsoperationautre.setText("");
                if(visage.isChecked()){
                    if(!Lifting_cervico_facial.isChecked()&&!Peeling.isChecked()&&!Blepharoplastie.isChecked()&&!Lifting_temporal.isChecked()&&!Lifting_complet.isChecked()&&!Genioplastie.isChecked()&&!Otoplastie.isChecked()&&!autrevisage.isChecked()){
                        errordetailsoperationvisage.setText("le detail operation sur le visage n'est pas specifier");
                        errordetailsoperationvisage.setVisibility(View.VISIBLE);
                    }else{
                        errordetailsoperationvisage.setVisibility(View.GONE);
                        errordetailsoperationvisage.setText("");
                        if(autrevisage.isChecked()&&visageautre.getText().toString().equals("")){
                            errordetailsoperationvisage.setText("l'autre operation sur le visage n'est pas specifier");
                            errordetailsoperationvisage.setVisibility(View.VISIBLE);
                        }else{
                            errordetailsoperationvisage.setText("");
                            errordetailsoperationvisage.setVisibility(View.GONE);
                        }
                    }
                }else{
                    errordetailsoperationvisage.setText("");
                    errordetailsoperationvisage.setVisibility(View.GONE);
                }
                if(seins.isChecked()){
                    if(!Augmentation_mammaire.isChecked()&&!Lipofilling_des_seins.isChecked()&&!Reduction_mammaire.isChecked()&&!Changement_de_protheses.isChecked()&&!Lifting_des_seins.isChecked()&&!Gynecomastie.isChecked()&&!autreseins.isChecked()){
                        errordetailsoperationseins.setText("le detail operation sur les seins n'est pas specifier");
                        errordetailsoperationseins.setVisibility(View.VISIBLE);
                    }else{
                        errordetailsoperationseins.setText("");
                        errordetailsoperationseins.setVisibility(View.GONE);
                        if(autreseins.isChecked()&&seinsautre.getText().toString().equals("")){
                            errordetailsoperationseins.setText("l'autre operation sur les seins n'est pas specifier");
                            errordetailsoperationseins.setVisibility(View.VISIBLE);
                        }else{
                            errordetailsoperationseins.setText("");
                            errordetailsoperationseins.setVisibility(View.GONE);
                        }
                    }
                }else{
                    errordetailsoperationseins.setText("");
                    errordetailsoperationseins.setVisibility(View.GONE);
                }
                if(corp.isChecked()){
                    if(!Liposuccion.isChecked()&&!Abdominoplastie.isChecked()&&!Lifting_des_cuisses.isChecked()&&!Augmentation_fesses.isChecked()&&!autrecorp.isChecked()){
                        errordetailsoperationcorp.setText("le detail operation sur le corp n'est pas specifier");
                        errordetailsoperationcorp.setVisibility(View.VISIBLE);
                    }else{
                        errordetailsoperationcorp.setText("");
                        errordetailsoperationcorp.setVisibility(View.GONE);
                        if(autrecorp.isChecked()&&corpautre.getText().toString().equals("")){
                            errordetailsoperationcorp.setText("l'autre operation sur le corp n'est pas specifier");
                            errordetailsoperationcorp.setVisibility(View.VISIBLE);
                        }else{
                            errordetailsoperationcorp.setVisibility(View.GONE);
                            errordetailsoperationcorp.setText("");
                        }
                    }
                }else{
                    errordetailsoperationcorp.setVisibility(View.GONE);
                    errordetailsoperationcorp.setText("");
                }


            }
            if(autre.isChecked()&&autreoperation.getText().toString().equals("")){
                errordetailsoperationautre.setText("l'autre operation  n'est pas specifier");
                errordetailsoperationautre.setVisibility(View.VISIBLE);
            }else{
                errordetailsoperationautre.setText("");
                errordetailsoperationautre.setVisibility(View.GONE);
            }



            if(errornom.getText().toString().equals("")&&errorprenom.getText().toString().equals("")&&errormail.getText().toString().equals("")&&errortel.getText().toString().equals("")&&errorage.getText().toString().equals("")&&errordiagnostic.getText().toString().equals("")&&errorantecedent.getText().toString().equals("")&&errorcompterendu.getText().toString().equals("")&& erordateoperation.getText().toString().equals("")&& erorequipe.getText().toString().equals("")&& eroroperateur.getText().toString().equals("")&& eroradresse.getText().toString().equals("")&&errorNum_admission.getText().toString().equals("")&&errordetailsoperation.getText().toString().equals("")&&errordetailsoperationcorp.getText().toString().equals("")&&errordetailsoperationvisage.getText().toString().equals("")&&errordetailsoperationseins.getText().toString().equals("")&&errordetailsoperationautre.getText().toString().equals("")) {

                int lengtvisages=-1;
                String []visages= new String[10];;
                String autrevisages = "";
                int lengtseins=-1;
                String[]seinss= new String[10];;
                String autreseinss="";
                int lengtcorps=-1;
                String[]corps= new String[10];;
                String autrecorps="";
                int lengtinterventions=-1;
                String[]interventions = new String[10];
                String autreoperations="";
                if(visage.isChecked()){
                    lengtinterventions++;
                    interventions[lengtinterventions]="visages";
                    if(Lifting_cervico_facial.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Lifting cervico facial";
                    }
                    if(Peeling.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Peeling";
                    }
                    if(Blepharoplastie.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Blepharoplastie";
                    }
                    if(Lifting_temporal.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Lifting temporal";
                    }
                    if(Lifting_complet.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Lifting complet";
                    }
                    if(Genioplastie.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Genioplastie";
                    }
                    if(Otoplastie.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="Otoplastie";
                    }
                    if(autrevisage.isChecked()){
                        lengtvisages++;
                        visages[lengtvisages]="autre";
                        autrevisages=visageautre.getText().toString();
                    }



                }
                if(corp.isChecked()){
                    lengtinterventions++;
                    interventions[lengtinterventions]="corps";
                    if(Liposuccion.isChecked()){
                        lengtcorps++;
                        corps[lengtcorps]="Liposuccion";
                    }
                    if(Abdominoplastie.isChecked()){
                        lengtcorps++;
                        corps[lengtcorps]="Abdominoplastie";
                    }
                    if(Lifting_des_cuisses.isChecked()){
                        lengtcorps++;
                        corps[lengtcorps]="Lifting des cuisses";
                    }
                    if(Augmentation_fesses.isChecked()){
                        lengtcorps++;
                        corps[lengtcorps]="Augmentation fesses";
                    }
                    if(autrecorp.isChecked()){
                        lengtcorps++;
                        corps[lengtcorps]="autre";
                        autrecorps=corpautre.getText().toString();
                    }


                }
                if(seins.isChecked()){
                    lengtinterventions++;
                    interventions[lengtinterventions]="seins";
                    if(Augmentation_mammaire.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="Augmentation mammaire";
                    }
                    if(Lipofilling_des_seins.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="Lipofilling des seins";
                    }
                    if(Reduction_mammaire.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="Reduction mammaire";
                    }
                    if(Changement_de_protheses.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="Changement de protheses";
                    }
                    if(Lifting_des_seins.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="lifting des seins";
                    }
                    if(Gynecomastie.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="Gynecomastie";
                    }
                    if(autreseins.isChecked()){
                        lengtseins++;
                        seinss[lengtseins]="autre";
                        autreseinss=seinsautre.getText().toString();
                    }

                }
                if(autre.isChecked()){
                    lengtinterventions++;
                    interventions[lengtinterventions]="autre";
                    autreoperations=autreoperation.getText().toString();

                }
                SharedPreferences prefs = this.getSharedPreferences(
                        "com.example.app", Context.MODE_PRIVATE);

                String token=prefs.getString("token","h");
                String mailauth = prefs.getString("mail", "d");

                String ajousex="";
                if(femme.isChecked()){

                    ajousex="Femme";
                }else{
                    ajousex="Homme";
                }
                String operateur;
                String autroperateur="";
                if(radiooperateurtaher.isChecked()){
                    operateur="dr taher";
                    autroperateur="";

                }else{
                    operateur="autre";
                    autroperateur=autreoperateur.getText().toString();
                }
                String equipes;
                String autroequipes="";
                if(radioequipetaher.isChecked()){
                    equipes="dr taher";
                    autroequipes="";

                }else{
                    equipes="autre";
                    autroequipes=autreequipe.getText().toString();
                }


                threadmodification task=new threadmodification(ancienId,num_adminission.getText().toString(),token,mailauth,editnom.getText().toString(),editprenom.getText().toString(),editmail.getText().toString(),edittel.getText().toString(),age.getText().toString(),ajousex,editdiagnostic.getText().toString(),antecedent.getText().toString(),compterendu.getText().toString(),operateur,autroperateur,equipes,autroequipes,editadresse.getText().toString(),visages,autrevisages,seinss,autreseinss,corps,autrecorps,interventions,autreoperations,dateoperation.getText().toString());
                Thread thread = new Thread (task);
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(task.getAjouter()) {

                    threadgetimages task1 = new threadgetimages(ancienId);
                    Thread thread1 = new Thread(task1);
                    thread1.start();
                    try {
                        thread1.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    for(int i =0;i<task1.getImagesavant().length;i++){
                        if(!task1.getImagesavant()[i].getId().equals("")) {
                            threadsuppressionphoto task2 = new threadsuppressionphoto(task1.getImagesavant()[i].getId());
                            Thread thread2 = new Thread(task2);
                            thread2.start();
                            try {
                                thread2.join();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                    for(int i =0;i<task1.getImagesapres().length;i++){
                        if(!task1.getImagesapres()[i].getId().equals("")) {
                            threadsuppressionphoto task2 = new threadsuppressionphoto(task1.getImagesapres()[i].getId());
                            Thread thread2 = new Thread(task2);
                            thread2.start();
                            try {
                                thread2.join();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                    ancienavant1="";
                    ancienavant2="";
                    ancienavant3="";
                    ancienavant4="";
                    ancienavant5="";
                    ancienapres1="";
                    ancienapres2="";
                    ancienapres3="";
                    ancienapres4="";
                    ancienapres5="";

                   if(isavant1){
                       threadajouterphoto task5 = new threadajouterphoto(ancienId,"before",editnote1.getText().toString(),a1);
                       Thread thread5 = new Thread(task5);
                       thread5.start();
                       try {
                           thread5.join();
                       } catch (InterruptedException e) {
                           e.printStackTrace();
                       }
                       ancienavant1=task5.getLien();


                   }
                    if(isavant2){

                        threadajouterphoto task5 = new threadajouterphoto(ancienId,"before",editnote2.getText().toString(),a2);
                        Thread thread5 = new Thread(task5);
                        thread5.start();
                        try {
                            thread5.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ancienavant2=task5.getLien();

                    }
                    if(isavant3){
                        threadajouterphoto task5 = new threadajouterphoto(ancienId,"before",editnote3.getText().toString(),a3);
                        Thread thread5 = new Thread(task5);
                        thread5.start();
                        try {
                            thread5.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ancienavant3=task5.getLien();

                    }
                    if(isavant4){

                        threadajouterphoto task5 = new threadajouterphoto(ancienId,"before",editnote4.getText().toString(),a4);
                        Thread thread5 = new Thread(task5);
                        thread5.start();
                        try {
                            thread5.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ancienavant4=task5.getLien();

                    }
                    if(isavant5){

                        threadajouterphoto task5 = new threadajouterphoto(ancienId,"before",editnote5.getText().toString(),a5);
                        Thread thread5 = new Thread(task5);
                        thread5.start();
                        try {
                            thread5.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ancienavant5=task5.getLien();

                    }
                    if(isapres1){
                        threadajouterphoto task5 = new threadajouterphoto(ancienId,"before",editnote11.getText().toString(),ap1);
                        Thread thread5 = new Thread(task5);
                        thread5.start();
                        try {
                            thread5.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ancienapres1=task5.getLien();

                    }
                    if(isapres2){

                        threadajouterphoto task5 = new threadajouterphoto(ancienId,"before",editnote21.getText().toString(),ap2);
                        Thread thread5 = new Thread(task5);
                        thread5.start();
                        try {
                            thread5.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ancienapres2=task5.getLien();

                    }
                    if(isapres3){

                        threadajouterphoto task5 = new threadajouterphoto(ancienId,"before",editnote31.getText().toString(),ap3);
                        Thread thread5 = new Thread(task5);
                        thread5.start();
                        try {
                            thread5.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ancienapres3=task5.getLien();

                    }
                    if(isapres4){

                        threadajouterphoto task5 = new threadajouterphoto(ancienId,"before",editnote41.getText().toString(),ap4);
                        Thread thread5 = new Thread(task5);
                        thread5.start();
                        try {
                            thread5.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ancienapres4=task5.getLien();

                    }
                    if(isapres5){

                        threadajouterphoto task5 = new threadajouterphoto(ancienId,"before",editnote51.getText().toString(),ap5);
                        Thread thread5 = new Thread(task5);
                        thread5.start();
                        try {
                            thread5.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ancienapres5=task5.getLien();

                    }










                    Toast.makeText(detailOperation.this, "modification apporter", Toast.LENGTH_LONG).show();
                    anciensexe = sexe;

                    //modification operation
                    Intent inten = new Intent(detailOperation.this, detailOperation.class);
                    inten.putExtra("modif", true);
                    inten.putExtra("nom", editnom.getText().toString());
                    inten.putExtra("prenom", editprenom.getText().toString());
                    inten.putExtra("mail", editmail.getText().toString());
                    inten.putExtra("tel", edittel.getText().toString());
                    inten.putExtra("age", age.getText().toString());
                    inten.putExtra("diag", editdiagnostic.getText().toString());
                    inten.putExtra("antecedent", antecedent.getText().toString());
                    inten.putExtra("compterendu", compterendu.getText().toString());
                    inten.putExtra("dateoperation", dateoperation.getText().toString());
                    inten.putExtra("admission",num_adminission.getText().toString());
                    inten.putExtra("adresse",editadresse.getText().toString());

                    if (femme.isChecked()) {
                        inten.putExtra("sexe", "femme");

                    } else {

                        inten.putExtra("sexe", "homme");
                    }
                    if(radiooperateurtaher.isChecked()){

                        inten.putExtra("operateur", "drtaher");

                    }else{

                        inten.putExtra("operateur", autreoperateur.getText().toString());

                    }
                    if(radioequipetaher.isChecked()){

                        inten.putExtra("equipe", "drtaher");

                    }else{

                        inten.putExtra("equipe", autreequipe.getText().toString());

                    }
                    if(!visage.isChecked()){

                        inten.putExtra("visage",false);
                        inten.putExtra("lifting_cervico_facial",false);
                        inten.putExtra("peeling",false);
                        inten.putExtra("blepharoplastie",false);
                        inten.putExtra("lifting_temporal",false);
                        inten.putExtra("lifting_complet",false);
                        inten.putExtra("genioplastie",false);
                        inten.putExtra("otoplastie",false);
                        inten.putExtra("autrevisage","");

                    }else{
                        inten.putExtra("visage", true);

                        if(Lifting_cervico_facial.isChecked()){
                            inten.putExtra("lifting_cervico_facial",true);
                        }else{
                            inten.putExtra("lifting_cervico_facial",false);
                        }
                        if(Peeling.isChecked()){
                            inten.putExtra("peeling",true);
                        }else{
                            inten.putExtra("peeling",false);
                        }
                        if(Blepharoplastie.isChecked()){
                            inten.putExtra("blepharoplastie",true);
                        }else{
                            inten.putExtra("blepharoplastie",false);
                        }
                        if(Lifting_temporal.isChecked()){
                            inten.putExtra("lifting_temporal",true);
                        }else{
                            inten.putExtra("lifting_temporal",false);
                        }
                        if(Lifting_complet.isChecked()){
                            inten.putExtra("lifting_complet",true);
                        }else{
                            inten.putExtra("lifting_complet",false);
                        }
                        if(Genioplastie.isChecked()){
                            inten.putExtra("genioplastie",true);
                        }else{
                            inten.putExtra("genioplastie",false);
                        }
                        if(Genioplastie.isChecked()){
                            inten.putExtra("otoplastie",true);
                        }else{
                            inten.putExtra("otoplastie",false);
                        }
                        if(autrevisage.isChecked()){
                            inten.putExtra("autrevisage",visageautre.getText().toString());
                        }else{
                            inten.putExtra("autrevisage","");
                        }

                    }
                    if(!seins.isChecked()){
                        inten.putExtra("seins",false);
                        inten.putExtra("augmentation_mammaire",false);
                        inten.putExtra("lipofilling_des_seins",false);
                        inten.putExtra("reduction_mammaire(",false);
                        inten.putExtra("changement_de_protheses",false);
                        inten.putExtra("lifting_des_seins",false);
                        inten.putExtra("gynecomastie",false);
                        inten.putExtra("autreseins","");
                    }else{
                        inten.putExtra("seins",true);
                        if(Augmentation_mammaire.isChecked()){
                            inten.putExtra("augmentation_mammaire",true);
                        }else{
                            inten.putExtra("augmentation_mammaire",false);
                        }
                        if(Lipofilling_des_seins.isChecked()){
                            inten.putExtra("lipofilling_des_seins",true);
                        }else{
                            inten.putExtra("lipofilling_des_seins",false);
                        }
                        if(Reduction_mammaire.isChecked()){
                            inten.putExtra("reduction_mammaire(",true);
                        }else{
                            inten.putExtra("reduction_mammaire(",false);
                        }
                        if(Changement_de_protheses.isChecked()){
                            inten.putExtra("changement_de_protheses",true);
                        }else{
                            inten.putExtra("changement_de_protheses",false);
                        }
                        if(Lifting_des_seins.isChecked()){
                            inten.putExtra("lifting_des_seins",true);
                        }else{
                            inten.putExtra("lifting_des_seins",false);
                        }
                        if(Gynecomastie.isChecked()){
                            inten.putExtra("gynecomastie",true);
                        }else{
                            inten.putExtra("gynecomastie",false);
                        }
                        if(autreseins.isChecked()){
                            inten.putExtra("autreseins",seinsautre.getText().toString());
                        }else{
                            inten.putExtra("autreseins",seinsautre.getText().toString());
                        }

                    }
                    if(!corp.isChecked()){
                        inten.putExtra("corp",false);
                        inten.putExtra("liposuccion",false);
                        inten.putExtra("abdominoplastie",false);
                        inten.putExtra("lifting_des_cuisses",false);
                        inten.putExtra("Augmentation_fesse", false);
                        inten.putExtra("autrecorp","");
                    }else{
                        inten.putExtra("corp", true);
                        if(Liposuccion.isChecked()){
                            inten.putExtra("liposuccion",true);
                        }else{
                            inten.putExtra("liposuccion",false);
                        }
                        if(Abdominoplastie.isChecked()){
                            inten.putExtra("abdominoplastie",true);
                        }else{
                            inten.putExtra("abdominoplastie",false);
                        }
                        if(Lifting_des_cuisses.isChecked()){
                            inten.putExtra("lifting_des_cuisses",true);
                        }else{
                            inten.putExtra("lifting_des_cuisses",false);
                        }
                        if(Augmentation_fesses.isChecked()){
                            inten.putExtra("Augmentation_fesse", true);
                        }else{
                            inten.putExtra("Augmentation_fesse", false);
                        }
                        if(autrecorp.isChecked()){
                            inten.putExtra("autrecorp",corpautre.getText().toString());
                        }else{
                            inten.putExtra("autrecorp","");

                        }

                    }
                    if(!autre.isChecked()){
                        inten.putExtra("autre", "");

                    } else {
                        inten.putExtra("autre", autreoperation.getText().toString());

                    }
                    inten.putExtra("id",ancienId);


                    inten.putExtra("avant1", ancienavant1);
                    inten.putExtra("descavant1", editnote1.getText().toString());


                    inten.putExtra("avant2", ancienavant2);
                    inten.putExtra("descavant2", editnote2.getText().toString());



                    inten.putExtra("avant3", ancienavant3);
                    inten.putExtra("descavant3", editnote3.getText().toString());


                    inten.putExtra("avant4", ancienavant4);
                    inten.putExtra("descavant4", editnote4.getText().toString());

                    inten.putExtra("avant5", ancienavant5);
                    inten.putExtra("descavant5", editnote5.getText().toString());


                    inten.putExtra("apres1", ancienapres1);
                    inten.putExtra("descapres1", editnote11.getText().toString());

                    inten.putExtra("apres2", ancienapres2);
                    inten.putExtra("descapres2", editnote21.getText().toString());

                    inten.putExtra("apres3", ancienapres3);
                    inten.putExtra("descapres3", editnote31.getText().toString());

                    inten.putExtra("apres4", ancienapres4);
                    inten.putExtra("descapres4", editnote41.getText().toString());

                    inten.putExtra("apres5", ancienapres5);
                    inten.putExtra("descapres5", editnote51.getText().toString());

                    inten.putExtra("isavant1",isavant1);
                    inten.putExtra("isavant2",isavant2);
                    inten.putExtra("isavant3",isavant3);
                    inten.putExtra("isavant4",isavant4);
                    inten.putExtra("isavant5",isavant5);

                    inten.putExtra("isapres1",isapres1);
                    inten.putExtra("isapres2",isapres2);
                    inten.putExtra("isapres3",isapres3);
                    inten.putExtra("isapres4",isapres4);
                    inten.putExtra("isapres5",isapres5);




                    startActivity(inten);
                }else{


                    AlertDialog.Builder adb = new AlertDialog.Builder(detailOperation.this);

                    adb.setTitle("erreur");

                    adb.setMessage(task.getError());

                    adb.setPositiveButton("Ok", null);

                    adb.show();
                    num_adminission.setFocusable(true);

                }

            }else{
                Toast.makeText(detailOperation.this,"il ya des champs qui ne sont pas remplie . verifier que tous les champs sont bien remplie ",Toast.LENGTH_LONG).show();

            }
        }

        //noinspection SimplifiableIfStatement
        if(id == R.id.deconnexion || id == R.id.deconnexion1){

            Intent intent = new Intent(detailOperation.this,
                    Authentification.class);

            startActivity(intent);

            // Toast.makeText(Liste_operation.this,Personne.gettoken(),Toast.LENGTH_LONG).show();

        }
        if (id == R.id.quitter || id == R.id.quitter1) {
            Intent intent = new Intent(detailOperation.this,
                    Liste_operation.class);
            intent.putExtra("qr","");

            startActivity(intent);

        }
        if(id == R.id.rapportpdf || id == R.id.rapportpdf1){

            try {
                rapport();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }

        }
        return super.onOptionsItemSelected(item);
    }









    protected Dialog onCreateDialog(int id) {



        AlertDialog dialogDetails = null;



        switch (id) {

            case DIALOG_DATE_NAISSANCE:

                LayoutInflater inflater = LayoutInflater.from(this);

                View dialogview = inflater.inflate(R.layout.dialogdatenaissance, null);



                AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);

                dialogbuilder.setTitle("Date de naissance");



                dialogbuilder.setView(dialogview);

                dialogDetails = dialogbuilder.create();



                break;


            case DIALOG_DATE_OPERATION:

                LayoutInflater inflater1 = LayoutInflater.from(this);

                View dialogview1 = inflater1.inflate(R.layout.dialogdatenaissance, null);



                AlertDialog.Builder dialogbuilder1 = new AlertDialog.Builder(this);

                dialogbuilder1.setTitle("Date de l'operation");



                dialogbuilder1.setView(dialogview1);

                dialogDetails = dialogbuilder1.create();



                break;

        }



        return dialogDetails;

    }



    @Override

    protected void onPrepareDialog(int id, Dialog dialog) {



        switch (id) {




            case DIALOG_DATE_OPERATION:

                final AlertDialog alertDialog1 = (AlertDialog) dialog;

                Button ok1 = (Button) alertDialog1

                        .findViewById(R.id.ok);

                Button nn1 = (Button) alertDialog1

                        .findViewById(R.id.non);

                final DatePicker date1 = (DatePicker) alertDialog1

                        .findViewById(R.id.datenaissance);
                Date a1 =new Date(2015,0,1);
                Date b1 =new Date(2050,0,1);
                Calendar c1 = Calendar.getInstance();
                c1.set(2050, 0, 1);

                date1.setMaxDate(c1.getTimeInMillis());
                c1.set(2015, 0, 1);

                date1.setMinDate(c1.getTimeInMillis());







                ok1.setOnClickListener(new View.OnClickListener() {


                    @Override

                    public void onClick(View v) {

                        alertDialog1.dismiss();


                        Toast.makeText(

                                detailOperation.this,

                                "date de l'operation est  : " + (date1.getDayOfMonth()) + "/" + (date1.getMonth() + 1) + "/" + (date1.getYear())

                                ,

                                Toast.LENGTH_LONG).show();
                        dateOperation = "" + (date1.getDayOfMonth()) + "/" + (date1.getMonth() + 1) + "/" + (date1.getYear());
                        dateoperation.setText(dateOperation);


                    }

                });



                nn1.setOnClickListener(new View.OnClickListener() {



                    @Override

                    public void onClick(View v) {

                            alertDialog1.dismiss();

                    }

                });

                break;

        }

    }






    @Override
    public View makeView() {
        ImageView i = new ImageView(this);
        i.setBackgroundColor(0xFF000000);
        i.setScaleType(ImageView.ScaleType.FIT_CENTER);
        i.setLayoutParams(new ImageSwitcher.LayoutParams(ImageSwitcher.LayoutParams.MATCH_PARENT,
                ImageSwitcher.LayoutParams.MATCH_PARENT));
        return i;
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode== cam_request1){
            a1 =(Bitmap) data.getExtras().get("data");

             avant1.setImageBitmap(a1);
            isavant1=true;





        }else{
            if (requestCode== cam_request2){
                a2 =(Bitmap) data.getExtras().get("data");

                avant2.setImageBitmap(a2);
                isavant2=true;





            }else{
                if (requestCode== cam_request3){
                    a3 =(Bitmap) data.getExtras().get("data");

                    avant3.setImageBitmap(a3);
                    isavant3=true;




                }else{
                    if (requestCode== cam_request4){
                        a4 =(Bitmap) data.getExtras().get("data");

                        avant4.setImageBitmap(a4);
                        isavant4=true;





                    }else{
                        if (requestCode== cam_request5){
                            a5 =(Bitmap) data.getExtras().get("data");

                            avant5.setImageBitmap(a5);
                            isavant5=true;





                        }else{
                            if(requestCode == gallery_request1){
                                Uri selectedImage = data.getData();
                                String[] filePath = { MediaStore.Images.Media.DATA };
                                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                                c.moveToFirst();
                                int columnIndex = c.getColumnIndex(filePath[0]);
                                String picturePath = c.getString(columnIndex);
                                c.close();
                                a1 = (BitmapFactory.decodeFile(picturePath));
                                Log.w("path of image gallery", picturePath + "");
                                avant1.setImageBitmap(a1);
                                isavant1=true;


                            }else{
                                if(requestCode == gallery_request2){
                                    Uri selectedImage = data.getData();
                                    String[] filePath = { MediaStore.Images.Media.DATA };
                                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                                    c.moveToFirst();
                                    int columnIndex = c.getColumnIndex(filePath[0]);
                                    String picturePath = c.getString(columnIndex);
                                    c.close();
                                    a2 = (BitmapFactory.decodeFile(picturePath));
                                    Log.w("path of image gallery", picturePath + "");
                                    avant2.setImageBitmap(a2);
                                    isavant2=true;


                                }else{
                                    if(requestCode == gallery_request3){
                                        Uri selectedImage = data.getData();
                                        String[] filePath = { MediaStore.Images.Media.DATA };
                                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                                        c.moveToFirst();
                                        int columnIndex = c.getColumnIndex(filePath[0]);
                                        String picturePath = c.getString(columnIndex);
                                        c.close();
                                        a3 = (BitmapFactory.decodeFile(picturePath));
                                        Log.w("path of image gallery", picturePath + "");
                                        avant3.setImageBitmap(a3);
                                        isavant3=true;


                                    }else{
                                        if(requestCode == gallery_request4){
                                            Uri selectedImage = data.getData();
                                            String[] filePath = { MediaStore.Images.Media.DATA };
                                            Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                                            c.moveToFirst();
                                            int columnIndex = c.getColumnIndex(filePath[0]);
                                            String picturePath = c.getString(columnIndex);
                                            c.close();
                                            a4 = (BitmapFactory.decodeFile(picturePath));
                                            Log.w("path of image gallery", picturePath + "");
                                            avant4.setImageBitmap(a4);
                                            isavant4=true;


                                        }else{
                                            if(requestCode == gallery_request5){
                                                Uri selectedImage = data.getData();
                                                String[] filePath = { MediaStore.Images.Media.DATA };
                                                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                                                c.moveToFirst();
                                                int columnIndex = c.getColumnIndex(filePath[0]);
                                                String picturePath = c.getString(columnIndex);
                                                c.close();
                                                a5 = (BitmapFactory.decodeFile(picturePath));
                                                Log.w("path of image gallery", picturePath + "");
                                                avant5.setImageBitmap(a5);
                                                isavant5=true;


                                            }else {
                                                if (requestCode== cam_request11){
                                                    ap1 =(Bitmap) data.getExtras().get("data");

                                                    avant11.setImageBitmap(ap1);
                                                    isapres1=true;


                                                }else{
                                                    if (requestCode== cam_request21){
                                                        ap2 =(Bitmap) data.getExtras().get("data");

                                                        avant21.setImageBitmap(ap2);
                                                        isapres2=true;



                                                    }else{
                                                        if (requestCode== cam_request31){
                                                            ap3 =(Bitmap) data.getExtras().get("data");

                                                            avant31.setImageBitmap(ap3);
                                                            isapres3=true;




                                                        }else{
                                                            if (requestCode== cam_request41){
                                                                ap4 =(Bitmap) data.getExtras().get("data");

                                                                avant41.setImageBitmap(ap4);
                                                                isapres4=true;


                                                            }else{
                                                                if (requestCode== cam_request51){
                                                                    ap5 =(Bitmap) data.getExtras().get("data");

                                                                    avant51.setImageBitmap(ap5);
                                                                    isapres5=true;




                                                                }else{
                                                                    if(requestCode == gallery_request11){
                                                                        Uri selectedImage = data.getData();
                                                                        String[] filePath = { MediaStore.Images.Media.DATA };
                                                                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                                                                        c.moveToFirst();
                                                                        int columnIndex = c.getColumnIndex(filePath[0]);
                                                                        String picturePath = c.getString(columnIndex);
                                                                        c.close();
                                                                        ap1 = (BitmapFactory.decodeFile(picturePath));
                                                                        Log.w("path of image gallery", picturePath + "");
                                                                        avant11.setImageBitmap(ap1);
                                                                        isapres1=true;

                                                                    }else{
                                                                        if(requestCode == gallery_request21){
                                                                            Uri selectedImage = data.getData();
                                                                            String[] filePath = { MediaStore.Images.Media.DATA };
                                                                            Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                                                                            c.moveToFirst();
                                                                            int columnIndex = c.getColumnIndex(filePath[0]);
                                                                            String picturePath = c.getString(columnIndex);
                                                                            c.close();
                                                                            ap2 = (BitmapFactory.decodeFile(picturePath));
                                                                            Log.w("path of image gallery", picturePath + "");
                                                                            avant21.setImageBitmap(ap2);
                                                                            isapres2=true;

                                                                        }else{
                                                                            if(requestCode == gallery_request31){
                                                                                Uri selectedImage = data.getData();
                                                                                String[] filePath = { MediaStore.Images.Media.DATA };
                                                                                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                                                                                c.moveToFirst();
                                                                                int columnIndex = c.getColumnIndex(filePath[0]);
                                                                                String picturePath = c.getString(columnIndex);
                                                                                c.close();
                                                                                ap3 = (BitmapFactory.decodeFile(picturePath));
                                                                                Log.w("path of image gallery", picturePath + "");
                                                                                avant31.setImageBitmap(ap3);
                                                                                isapres3=true;


                                                                            }else{
                                                                                if(requestCode == gallery_request41){
                                                                                    Uri selectedImage = data.getData();
                                                                                    String[] filePath = { MediaStore.Images.Media.DATA };
                                                                                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                                                                                    c.moveToFirst();
                                                                                    int columnIndex = c.getColumnIndex(filePath[0]);
                                                                                    String picturePath = c.getString(columnIndex);
                                                                                    c.close();
                                                                                    ap4 = (BitmapFactory.decodeFile(picturePath));
                                                                                    Log.w("path of image gallery", picturePath + "");
                                                                                    avant41.setImageBitmap(ap4);
                                                                                    isapres4=true;


                                                                                }else{
                                                                                    if(requestCode == gallery_request51){
                                                                                        Uri selectedImage = data.getData();
                                                                                        String[] filePath = { MediaStore.Images.Media.DATA };
                                                                                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                                                                                        c.moveToFirst();
                                                                                        int columnIndex = c.getColumnIndex(filePath[0]);
                                                                                        String picturePath = c.getString(columnIndex);
                                                                                        c.close();
                                                                                        ap5 = (BitmapFactory.decodeFile(picturePath));
                                                                                        Log.w("path of image gallery", picturePath + "");
                                                                                        avant51.setImageBitmap(ap5);
                                                                                        isapres5=true;

                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



    }
    public void deletteallphoto(String id){



    }
    public Bitmap downloadImage(  String url) {
        Bitmap a = null;
        String h = "";
        if (!url.contains("http")) {
            h = "http://apps.smart-robox.com/drtaher/" + url;
        } else {
            h = url;
        }

        if (!url.equals("")) {

                threadreadfromurl taska1 = new threadreadfromurl(h);
                Thread threada1 = new Thread(taska1);
                threada1.start();
                try {
                    threada1.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                a = taska1.getImage();

            } else {
                threadreadfromurl taska2 = new threadreadfromurl("http://www.business-garden.com/img/google-chrome-navigateur-web.jpg");
                Thread threada2 = new Thread(taska2);
                threada2.start();
                try {
                    threada2.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                a = taska2.getImage();

            }
            return a;


    }
    public void rapport()throws FileNotFoundException, DocumentException {
        edit1= (EditText) findViewById(R.id.Edit1);
        edit2= (EditText) findViewById(R.id.Edit2);
        edit3= (EditText) findViewById(R.id.Edit3);
        edit4= (EditText) findViewById(R.id.Edit4);
        edit5= (EditText) findViewById(R.id.Edit5);
        edit7= (EditText) findViewById(R.id.Edit7);
        edit8= (EditText) findViewById(R.id.Edit8);
        edit9= (EditText) findViewById(R.id.Edit9);
        edit10= (EditText) findViewById(R.id.Edit10);
        edit11= (EditText) findViewById(R.id.Edit11);
        edit12= (EditText) findViewById(R.id.Edit12);
        edit13= (EditText) findViewById(R.id.Edit13);
        edit14= (EditText) findViewById(R.id.Edit14);
        edit15= (EditText) findViewById(R.id.Edit15);
        edit16= (EditText) findViewById(R.id.Edit16);
        edit17= (EditText) findViewById(R.id.Edit17);

        edit18= (CheckBox) findViewById(R.id.Edit18);
        edit19= (CheckBox) findViewById(R.id.Edit19);
        edit20= (CheckBox) findViewById(R.id.Edit20);
        edit21= (CheckBox) findViewById(R.id.Edit21);
        edit22= (CheckBox) findViewById(R.id.Edit22);
        edit23= (CheckBox) findViewById(R.id.Edit23);
        edit24= (CheckBox) findViewById(R.id.Edit24);
        edit25= (CheckBox) findViewById(R.id.Edit25);
        edit26= (CheckBox) findViewById(R.id.Edit26);
        edit27= (CheckBox) findViewById(R.id.Edit27);
        edit28= (CheckBox) findViewById(R.id.Edit28);
        edit29= (CheckBox) findViewById(R.id.Edit29);
        edit30= (CheckBox) findViewById(R.id.Edit30);
        edit31= (CheckBox) findViewById(R.id.Edit31);
        edit32= (CheckBox) findViewById(R.id.Edit32);
        edit33= (CheckBox) findViewById(R.id.Edit33);
        edit34= (CheckBox) findViewById(R.id.Edit34);
        edit35= (CheckBox) findViewById(R.id.Edit35);

        hama1= (RadioButton) findViewById(R.id.hama1);
        hama2= (RadioButton) findViewById(R.id.hama2);
        hama3= (RadioButton) findViewById(R.id.hama3);
        hama4= (RadioButton) findViewById(R.id.hama4);
        hama5= (RadioButton) findViewById(R.id.hama5);






            File pdfFolder = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS),editnom.getText().toString()+editprenom.getText().toString()+ num_adminission.getText().toString());
            if (!pdfFolder.exists()) {
                pdfFolder.mkdir();
                Log.i("dvcdddfd", "Pdf Directory created");
            }

            //Create time stamp
            Date date = new Date() ;
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);

            File myFile = new File(pdfFolder + timeStamp + ".pdf");

            OutputStream output = new FileOutputStream(myFile);

            //Step 1
            com.itextpdf.text.Document document=new com.itextpdf.text.Document();

            //Step 2
            PdfWriter.getInstance(document, output);

            //Step 3
            document.open();

            //Step 4 Add content

            document.add(new Phrase(30, new Chunk("1)details patient\n\n\n", FontFactory.getFont(FontFactory.HELVETICA, 30, Font.BOLD, new BaseColor(255, 0, 0)))));
            document.add(new Phrase(20, new Chunk("    - Numero admission : "+num_adminission.getText().toString()+"\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
            document.add(new Phrase(20, new Chunk("    - Nom  : "+editnom.getText().toString()+"\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
            document.add(new Phrase(20, new Chunk("    - Prenom  : "+editprenom.getText().toString()+"\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
        String sexe="";
        if(femme.isChecked()){

            sexe="Femme";
        }else{
            sexe="Homme";
        }
            document.add(new Phrase(20, new Chunk("    - Sexe  : "+sexe+"\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
            document.add(new Phrase(20, new Chunk("    - Age  : "+age.getText().toString()+"\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
            document.add(new Phrase(20, new Chunk("    - Mail  : "+editmail.getText().toString()+"\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
            document.add(new Phrase(20, new Chunk("    - Telephone  : "+edittel.getText().toString()+"\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
            document.add(new Phrase(20, new Chunk("    - Adresse  : "+editadresse.getText().toString()+"\n\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

        document.add(new Phrase(30, new Chunk("2)details operation\n\n\n", FontFactory.getFont(FontFactory.HELVETICA, 30, Font.BOLD, new BaseColor(255, 0, 0)))));
        document.add(new Phrase(20, new Chunk("    - Date operation : "+dateoperation.getText().toString()+"\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
        document.add(new Phrase(20, new Chunk("    - Diagnostic  : "+editdiagnostic.getText().toString()+"\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
        String opera ="";
        if(radiooperateurtaher.isChecked()){
            opera="Dr Taher Djemal";
        }else{
            opera=autreoperateur.getText().toString();
        }
        document.add(new Phrase(20, new Chunk("    - Operateur  : "+opera+"\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
        String equip="";
        if(radioequipetaher.isChecked()){
            equip="Dr Taher Djemal";

        }else{
            equip=autreequipe.getText().toString();
        }
        document.add(new Phrase(20, new Chunk("    - Equipe  : "+equip+"\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
        document.add(new Phrase(20, new Chunk("    - Antecedent  : "+antecedent.getText().toString()+"\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
        document.add(new Phrase(20, new Chunk("    - Compte rendu  : "+compterendu.getText().toString()+"\n\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
        document.add(new Phrase(30, new Chunk("3)Intervention \n\n\n", FontFactory.getFont(FontFactory.HELVETICA, 30, Font.BOLD, new BaseColor(255, 0, 0)))));
        document.add(new Phrase(20, new Chunk("    -  L'intervention va concerner  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
        if(visage.isChecked()){
            document.add(new Phrase(20, new Chunk("    -  Visage  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 255)))));
            if(Lifting_cervico_facial.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Lifting cervico-facial  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));
                document.add(new Phrase(20, new Chunk(" - Infiltration de xylocaine adrenalinee dilue dans du s�rum physiologique \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                if(edit20.isChecked()) {
                    document.add(new Phrase(20, new Chunk("        - Cov (oui)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("        - Cov (non)   \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(edit21.isChecked()) {
                    document.add(new Phrase(20, new Chunk("        - Region pre tragienne (oui)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("        - Region pre tragienne (non)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(edit22.isChecked()) {
                    document.add(new Phrase(20, new Chunk("        - Jugale Haute (oui)   \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("        - Jugale Haute (non)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(edit23.isChecked()) {
                    document.add(new Phrase(20, new Chunk("        - Jugale Basse (oui)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("        - Jugale Basse (non)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(edit24.isChecked()) {
                    document.add(new Phrase(20, new Chunk("        - Retroauriculaire (oui)   \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("        - Retroauriculaire (non)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(edit25.isChecked()) {
                    document.add(new Phrase(20, new Chunk(" - Incision sous mentale (oui)  \n\n      ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk(" - Incision sous mentale  (non) \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                document.add(new Phrase(20, new Chunk(" - Decollement sous cutane\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
               if(edit26.isChecked()) {
                   document.add(new Phrase(20, new Chunk(" - Anterieure (oui)\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
               }else{
                   document.add(new Phrase(20, new Chunk(" - Anterieure  (non)\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

               }
                if(edit27.isChecked()) {
                    document.add(new Phrase(20, new Chunk(" - Luteral (oui)\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk(" - Luteral  (non)\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(edit28.isChecked()) {
                    document.add(new Phrase(20, new Chunk(" - Hemostase (oui)    \n\n      ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk(" - Hemostase (non)   \n\n      ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                document.add(new Phrase(20, new Chunk(" - Levee d�un lambeau du SMAS \n\n" +
                        "- Plicature du SMAS \n\n" +
                        "- Au niveau du Cov :\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                if(edit29.isChecked()) {
                    document.add(new Phrase(20, new Chunk("        - Degraissage supraplaty smal (oui)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("        - Degraissage supraplaty smal (non) \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(edit30.isChecked()) {
                    document.add(new Phrase(20, new Chunk("        - Platysmotomie (oui)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("        - Platysmotomie (non)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(edit31.isChecked()) {
                    document.add(new Phrase(20, new Chunk("        - Degraissage du 3 C Triangles intudigrastrique (oui)   \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("        - Degraissage du 3 C Triangles intudigrastrique (non)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(edit32.isChecked()) {
                    document.add(new Phrase(20, new Chunk("        - 2 triangles sous muntonniers (oui)   \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("        - 2 triangles sous muntonniers (non)   \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(edit33.isChecked()) {
                    document.add(new Phrase(20, new Chunk("        - Resection des glandes submandi   sulaire  (oui)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("        - Resection des glandes submandi   sulaire (non)  \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(edit34.isChecked()) {
                    document.add(new Phrase(20, new Chunk("        - Plasty smoplastie (oui)  \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("        - Plasty smoplastie (non)  \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                document.add(new Phrase(20, new Chunk(" - Degraissage des  2 uborde   mandibulaires \n" +
                        "- Redrapage cutan�\n" +
                        "- Exerese du Tissu cutane en exces \n" +
                        "- Suture des voies d�abord au vicryl rapide 3/0 sur 2 drains de N�14 \n\n      ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
              if(edit35.isChecked()) {
                  document.add(new Phrase(20, new Chunk(" - Lipofilling (oui)    \n\n      ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
              }else{
                  document.add(new Phrase(20, new Chunk(" - Lipofilling (non)    \n\n      ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

              }
                document.add(new Phrase(20, new Chunk(" - Base de prelevim       \n\n      ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

            }
            if(Peeling.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Peeling  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));

            }
            if(Blepharoplastie.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Blepharoplastie  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));
                document.add(new Phrase(20, new Chunk(" Sous Anesthesie generale Frais palpebral superieur  \n\n (Infiltration sous cutanee � laxylocaine � 0.5 % adr�nalinee) \n\n\n - Exerese d�un fuseau de peau palpebrale superieure  \n\n- Excision d�une banale de muscle de l�ossiculaire  \n\n ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                document.add(new Phrase(20, new Chunk(" - Hernies graisseuses       ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                if(edit18.isChecked()) {
                    document.add(new Phrase(20, new Chunk(" Interne (oui)  ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }else{
                    document.add(new Phrase(20, new Chunk(" Interne (non) ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(edit19.isChecked()) {
                    document.add(new Phrase(20, new Chunk(" Moyenne (oui)  \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk(" Moyenne (non) \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                document.add(new Phrase(20, new Chunk(" - Exerese de la graisse de la loge interne  \n\n" +
                        "- Verification de  l�hemostase \n\n" +
                        "- Suture cutanee en un seul plan par du sujet \n\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

            }
            if(Lifting_complet.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Lifting complet  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));

            }
            if(Lifting_temporal.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Lifting temporal  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));

            }
            if(Genioplastie.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Genioplastie  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));

            }
            if(Otoplastie.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Otoplastie  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));

            }
            if(autrevisage.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  "+visageautre.getText().toString()+"  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));

            }
        }
        if(seins.isChecked()){
            document.add(new Phrase(20, new Chunk("    -  Les seins  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 255)))));
            if(Augmentation_mammaire.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Augmentation mammaire  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));
                document.add(new Phrase(20, new Chunk(" Sous anesthesie generale \n\n\n - Patiente en decubitus dorsal  \n\n - Installation sterile du thorax et du ventre  \n\n- Incision (peri ar�olaire, sous mammaire, axillaire)  \n\n - Dissection d�une loge (retro-glandulaire, retro-musculaire) \n\n- Hemostase rigoureuse\n\n - Mise en place d�une prothese volume "+edit14.getText().toString()+" profil "+edit15.getText().toString()+"\n\n - Suture sous cutanee au vicryl 3/0\n\n - Suture cutanee au vicryl 3/0 \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

            }
            if(Lipofilling_des_seins.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Lipofilling des seins  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));

            }
            if(Reduction_mammaire.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Reduction mammaire  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));
                document.add(new Phrase(20, new Chunk(" Sous anesthesie generale \n\n\n - Patiente en position demi-assise   \n\n - Installation st�rile du thorax et ventre  \n\n- Incision selon un sch�ma pr� �tabli (technique verticale avec  un lambeau � p�dicule  sup�rieur) du sein droit  \n\n - Dexpidermisation du lambeau porte-mamelon \n\n- Dissection du lambeau avec une �paisseur de 1 cm \n\n - R�section cutan�o-glandulaire en  bloc du r�glement  III  pesant "+edit16.getText().toString()+". g\n\n - Hemostase\n\n - Idem pour le sein gauche avec une r�section cutan�o-glandulaire en bloc du segment  III  pesant "+edit17.getText().toString()+" g \n\n - Suture sous cutan�e de la plaque ar�olo-mamlonnaire  au prol�ne 2/0 selon la technique du round block\n\n - Suture cutan�e de la PAM au Vicryl blanc 3/0 pansement ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

            }
            if(Changement_de_protheses.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Changement des protheses  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));

            }
            if(Lifting_des_seins.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Lifting des seins  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));

            }
            if(Gynecomastie.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Gynecomastie  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));


            }
            if(autreseins.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  "+seinsautre.getText().toString()+"  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));

            }

        }
        if(corp.isChecked()){
            document.add(new Phrase(20, new Chunk("    -  Corp  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 255)))));
            if(Liposuccion.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Liposuccion  : \n\n ", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));
                document.add(new Phrase(20, new Chunk(" Sous anesthesie generale \n\n\n - Patiente en decubitus (ventral, dorsal) \n\n -Installation sterile du (dos, flancs, bras, face interne des cuisses, face interne des genoux, mollets, ventre \n\n-Infiltration de"+edit1.getText().toString()+"L de serum physiologique avec la xylocaine adrenalinee. \n\n -Aspiration de"+edit2.getText().toString()+" L  de graisse avec une canule N "+edit3.getText().toString()+"\n\n-Suture  au vicryl blanc 3/0, pansement\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

            }
            if(Abdominoplastie.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Abdominoplastie  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));
                document.add(new Phrase(20, new Chunk(" - Incision  sus pubienne allant aux deux epines iliaques  antero � superieurs  \n\n Decollement : \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
               if(hama6.isChecked()) {
                   document.add(new Phrase(20, new Chunk("         Au ras du fascia superficialis (oui)\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
               }else{
                   document.add(new Phrase(20, new Chunk("         Au ras du fascia superficialis (non)\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

               }
                if(hama7.isChecked()) {
                    document.add(new Phrase(20, new Chunk("         Au ras de l�aponevrose a " + edit4.getText().toString() + " cm (oui)\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("         Au ras de l�aponevrose a " + edit4.getText().toString() + " cm (non)\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                document.add(new Phrase(20, new Chunk(" - Individualisation de l�ombilic avec une distance ombilic-pubienne  \n\n - D�collement du  tunnel en regard de la ligne m�diane sus ombilicale\n\n- Resection du fascia superficialis et du tissu adipeux au niveau de la ligne mediane \n\n - Reparation du diastasis par rapprochement des deux muscles grands droits au Nylon 2 \n\n-Fixation de l�ombilic � l�aponevrose au Nylon 2\n\n-Reparation d�une hernie "+edit5.getText().toString()+" Par un sujet au Nylon 2\n\n-Rapprochement des deux berges du fascia superficials au vicryl 1\n\n- Resection cutaneo-adipeuse \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                if(hama1.isChecked()) {
                    document.add(new Phrase(20, new Chunk("         Adipeuse au niveau du (oui)\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("         Adipeuse au niveau du  (non)\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(hama2.isChecked()) {
                    document.add(new Phrase(20, new Chunk("         Bord superieur de l�ombilic (oui)\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("         Bord superieur de l�ombilic (non)\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(hama3.isChecked()) {
                    document.add(new Phrase(20, new Chunk("         Bord inferieur de l�ombilic (oui) \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("         Bord inferieur de l�ombilic (non)\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                 document.add(new Phrase(20, new Chunk(" - Capitonnage par des points separes au Nylon 2  \n\n - Exteriorisation de l�ombilic par une incision en quartier d�orange \n\n- Nouvelle distance ombilico-pubienne "+edit7.getText().toString()+" cm \n\n - Suture sous cutan�e au vicryl 1 et 3/0 \n\n- Suture cutan�e au vicryl blanc 3/0 sur 3 drains de reden  aspiratif N : 14\n\n- Etage sus ombilical\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                if(hama4.isChecked()) {
                    document.add(new Phrase(20, new Chunk("         Relache (oui) \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("         Relache (non) \n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
                if(hama5.isChecked()) {
                    document.add(new Phrase(20, new Chunk("         Sous tension (oui)\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));
                }else{
                    document.add(new Phrase(20, new Chunk("         Sous tension (non)\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                }
            }
            if(Lifting_des_cuisses.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Lifting des cuisses (1)  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));
                document.add(new Phrase(20, new Chunk(" Sous Anesthesie generale \n\n - Patiente en D D\n\n" +
                        "- Installation st�rile des cuisses �cart�es \n\n" +
                        "- Incision selon un sch�ma pr� �tabli allant de la face interne du genou jusqu�au pli genito �crural\n\n" +
                        "- D�collement sous cutan� au bistouri �lectrique \n\n" +
                        "-  R�section de l�exc�s cutan�o-graisseux en quatio d�orange h�mostase\n\n" +
                        "- Suture sous cutan�e au vicyl 1 et  2/0\n\n" +
                        "- Suture cutan�e au vicyl blanc 3/0\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

                document.add(new Phrase(20, new Chunk("        *  Lifting des cuisses (2)  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));
                document.add(new Phrase(20, new Chunk(" Sous Anesthesie generale \n\n - Patiente en D D  positon gyn�cologique\n\n" +
                        "- Installation st�rile des cuisses \n\n" +
                        "- Incision au niveau du pli de l�aine\n\n" +
                        "- D�collement sous cutan�e\n\n" +
                        "- R�section de l�exc�s cutan�o � graisseux \n\n" +
                        "- Fixation du lambeau inferieur sur le ligament arqu� pubien au Nylon 2\n" +
                        "- Suture sous cutan�e au vicyl 1 et 2/0\n\n" +
                        "- Suture cutan�e au vicyl blanc 3/0\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

            }
            if(Augmentation_fesses.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  Augmentation fesses  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));

            }
            if(autrecorp.isChecked()){
                document.add(new Phrase(20, new Chunk("        *  "+corpautre.getText().toString()+"  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 255, 0)))));

            }

        }
        if(autre.isChecked()){
            document.add(new Phrase(20, new Chunk("    -  "+autreoperation.getText().toString()+"  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 255)))));

        }

        document.add(new Phrase(20, new Chunk("    -  Injection de graisse au niveau "+edit8.getText().toString()+"  : \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 255)))));
        document.add(new Phrase(20, new Chunk("        *  Aspiration de "+edit9.getText().toString()+" l de graisse avec une canule N :"+edit10.getText().toString()+" dans un bocal sterile  : \n\n        * Tanisage de la graisse aspiree \n\n        * Injection de "+edit11.getText().toString()+" ml de graisse par une serinage  de "+edit12.getText().toString()+" avec une canule N :"+edit13.getText().toString()+" \n\n        * Suture cutanee au vicryl  blanc 3/0\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));

        document.add(new Phrase(20, new Chunk("    -  Lifting des bras \n\n Sous Anesthesie generale \n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 255)))));
        document.add(new Phrase(20, new Chunk("        *  Patiente en decubitus dorsal  \n\n        * Installation sterile des deux bras\n\n        * Incision au niveau du bras se prolongeant jusqu'a la region axillaire brisee au niveau de pli axillaire interne \n\n        * Decollement sous cutane au bistouri electrique\n\n        * Resection de l�exces cutaneo � graisseux en quartier d�orange \n\n        * Hemostase\n\n        * Suture sous cutanee au vicyl 3/0\n\n        * Suture sous cutanee au vicyl blanc  3/0\n\n", FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new BaseColor(0, 0, 0)))));


        //Step 5: Close the document
            document.close();

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(myFile), "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);



    }
}
