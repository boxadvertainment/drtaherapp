package com.example.lenovo.ddr_taher_app;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

/**
 * Created by lenovo on 14/06/2015.
 */
public class imageAdaptater extends BaseAdapter {
    public static Integer[] mThumbIds = {
            R.mipmap.tof1,R.mipmap.tof2,R.mipmap.tof3,
            R.mipmap.tof4,R.mipmap.tof5


    };

    public static Integer[] mImageIds = {
            R.mipmap.tof1,R.mipmap.tof2,R.mipmap.tof3,
            R.mipmap.tof4,R.mipmap.tof5
    };
    public imageAdaptater(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView i = new ImageView(mContext);

        i.setImageResource(mThumbIds[position]);
        i.setAdjustViewBounds(true);
        i.setLayoutParams(new Gallery.LayoutParams(
                Gallery.LayoutParams.MATCH_PARENT, Gallery.LayoutParams.MATCH_PARENT));
        i.setBackgroundResource(R.color.material_deep_teal_200);
        return i;
    }

    private Context mContext;

}

