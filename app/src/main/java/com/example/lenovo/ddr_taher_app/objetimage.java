package com.example.lenovo.ddr_taher_app;

/**
 * Created by lenovo on 10/07/2015.
 */
public class objetimage {
    private String id;
    private String data;
    private boolean existe;
    private String desc;

    public objetimage(String id,String data, boolean existe, String desc) {
        this.data = data;
        this.existe = existe;
        this.desc = desc;
        this.id=id;
    }
    public objetimage() {

    }


    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean getType() {
        return existe;
    }

    public void setType(boolean type) {
        this.existe = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
