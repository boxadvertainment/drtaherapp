package com.example.lenovo.ddr_taher_app;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lenovo on 01/07/2015.
 */
public class threadAjoutOperationBrouillant implements Runnable {
    private String num_admission;

    private String Token ;
    private String mailauth;
    private String nom;
    private String prenom;
    private String mail;
    private String tel;
    private String age;
    private String sexe;
    private String diagnostic;
    private String antecedent;
    private String compterendu;
    private String operateur;
    private String autreoperateur;
    private String equipe;
    private String aureequipe;
    private String adresse;

    private String[] visage;
    private String autrevisage;

    private String[] seins;
    private String autreseins;

    private String [] corp ;
    private String autrecorp;

    private String[]intervention;
    private String autre;



    private String date_operation;

    public boolean ajouter;
    public String error;

    public threadAjoutOperationBrouillant(String num_admission, String token, String mailauth, String nom, String prenom, String mail, String tel, String age, String sexe, String diagnostic, String antecedent, String compterendu, String operateur, String autreoperateur, String equipe, String aureequipe, String adresse, String[] visage, String autrevisage, String[] seins, String autreseins, String[] corp, String autrecorp, String[] intervention, String autre, String date_operation) {
        this.num_admission = num_admission;
        this.Token = token;
        this.mailauth = mailauth;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.tel = tel;
        this.age = age;
        this.sexe = sexe;
        this.diagnostic = diagnostic;
        this.antecedent = antecedent;
        this.compterendu = compterendu;
        this.operateur = operateur;
        this.autreoperateur = autreoperateur;
        this.equipe = equipe;
        this.aureequipe = aureequipe;
        this.adresse = adresse;
        this.visage = visage;
        this.autrevisage = autrevisage;
        this.seins = seins;
        this.autreseins = autreseins;
        this.corp = corp;
        this.autrecorp = autrecorp;
        this.intervention = intervention;
        this.autre = autre;
        this.date_operation = date_operation;
    }


    public String getError(){
        return error;
    }
    public Boolean getAjouter(){
        return ajouter;
    }

    @Override
    public void run() {

        Personne p = new Personne("hama", "grine");
        String add = "";
        add=p.addbrouillant(this.mailauth,this.Token,this.num_admission,this.nom,this.prenom,this.mail,this.tel,this.age,this.sexe,1,"hhh",this.date_operation,this.diagnostic,this.antecedent,this.compterendu,this.operateur,this.autreoperateur,this.equipe,this.aureequipe,this.adresse,this.visage,this.autrevisage,this.seins,this.autreseins,this.corp,this.autrecorp,this.intervention,this.autre);
        try {
            JSONObject hama=new JSONObject(add);
            ajouter=hama.getBoolean("success");
            if(ajouter==false){
                error=hama.getString("message");

            }else{
                error="";
            }
        } catch (JSONException e) {
            error="le champs numero d'admission est obligatoire meme dans le brouillant";


            e.printStackTrace();
        }
    }


}
