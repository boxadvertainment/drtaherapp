package com.example.lenovo.ddr_taher_app;

import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lenovo on 11/07/2015.
 */
public class threadajouterphoto implements Runnable {
    private String type;
    private String desc;
    private Bitmap image;
    private String lien ;
    private String id;
    private String idoper;

    public threadajouterphoto(String idoper ,String type, String desc, Bitmap image) {
        this.idoper=idoper;
        this.type = type;
        this.desc = desc;
        this.image = image;
    }

    @Override
    public void run() {
        Personne p = new Personne("hama","grine");
        String h = p.ajouter(this.idoper , this.type,this.desc,this.image);
        try {
            JSONObject a =new JSONObject(h);
            this.lien=a.getString("link");
            this.id=a.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public String getLien() {
        return lien;
    }

    public String getId() {
        return id;
    }
}
