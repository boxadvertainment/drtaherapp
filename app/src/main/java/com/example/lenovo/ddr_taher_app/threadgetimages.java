package com.example.lenovo.ddr_taher_app;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lenovo on 10/07/2015.
 */
public class threadgetimages implements  Runnable {
    private objetimage [] imagesavant;
    private objetimage [] imagesapres;
    private String id;

    public threadgetimages(String id) {
        this.id = id;
    }

    @Override
    public void run() {

        Personne p = new Personne("hama","grine");
        String h=p.getImage(this.id);

        try {
            JSONObject req = new JSONObject(h);
            JSONArray avant=req.getJSONArray("before");
            this.imagesavant=new objetimage[5];
            if(avant.length()==0){
                for (int j = 0; j < 5; j++) {
                    objetimage image = new objetimage("","", false, "");
                    this.imagesavant[j] = image;

                }
            }else
            {
                if(avant.length()<5) {


                    for (int i = 0; i < avant.length(); i++) {
                        JSONObject o = avant.getJSONObject(i);
                        objetimage image = new objetimage(o.getString("id"),o.getString("link"), true, o.getString("description"));
                        this.imagesavant[i] = image;
                    }
                    for (int j = avant.length(); j < 5; j++) {
                        objetimage image = new objetimage("","", false, "");
                        this.imagesavant[j] = image;

                    }
                }else{
                    for (int i = 0; i < 5; i++) {
                        JSONObject o = avant.getJSONObject(i);
                        objetimage image = new objetimage(o.getString("id"),o.getString("link"), true, o.getString("description"));
                        this.imagesavant[i] = image;
                    }
                }
            }


            JSONArray apres=req.getJSONArray("after");
            this.imagesapres=new objetimage[5];
            if(apres.length()==0){
                for (int j = 0; j < 5; j++) {
                    objetimage image = new objetimage("","", false, "");
                    this.imagesapres[j] = image;

                }

            }else {
                if(apres.length()<5) {

                    for (int i = 0; i < apres.length(); i++) {
                        JSONObject o = apres.getJSONObject(i);
                        objetimage image = new objetimage(o.getString("id"),o.getString("link"), true, o.getString("description"));
                        this.imagesapres[i] = image;
                    }
                    for (int j = apres.length(); j < 5; j++) {
                        objetimage image = new objetimage("","", false, "");
                        this.imagesapres[j] = image;

                    }
                }else{
                    for (int i = 0; i < 5; i++) {
                        JSONObject o = apres.getJSONObject(i);
                        objetimage image = new objetimage(o.getString("id"),o.getString("link"), true, o.getString("description"));
                        this.imagesapres[i] = image;
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public objetimage[] getImagesapres() {
        return imagesapres;
    }

    public objetimage[] getImagesavant() {
        return imagesavant;
    }
}
