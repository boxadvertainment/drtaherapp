package com.example.lenovo.ddr_taher_app;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by lenovo on 09/07/2015.
 */
public class threadreadfromurl implements Runnable  {
    Bitmap image;
    String url;

    public threadreadfromurl(String url) {
        this.url = url;
    }

    @Override
    public void run() {
        try {
            /*
            URL urlImage = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlImage.openConnection();
            InputStream inputStream = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(inputStream);
            */
            this.image = BitmapFactory.decodeStream(new URL(this.url).openConnection().getInputStream());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
