package com.example.lenovo.ddr_taher_app;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by lenovo on 09/07/2015.
 */
public class threadreadfromurlImage implements Runnable  {
    Image image;
    String url;
    Document d;

    public threadreadfromurlImage(String url , Document d) {
        this.url = url;
        this.d=d;
    }

    @Override
    public void run() {
        try {
            /*
            URL urlImage = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlImage.openConnection();
            InputStream inputStream = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(inputStream);
            */
            try {
                this.image = Image.getInstance(new URL("http://apps.smart-robox.com/drtaher/"+url));
                d.add(this.image);
            } catch (BadElementException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
